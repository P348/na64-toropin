      
      char histoname[100];
      char title[100];

      std::vector<TH1F*> histo1F;
      std::vector<TH2F*> histo2F;
      std::vector<TProfile*> profile;

      // -------- TRG ----------------------
      TH1F *histTRG[2];
      char hid1[8] = "hTRG_0";
      char title1[8] = "TRG x0";
      
      for (int i = 0; i < 2; i++) {
        sprintf(hid1,"hTRG%d",i);
        sprintf(title1,"TRG x%d",i);
        histTRG[i] = new TH1F(hid1, title1, 200, -100, 100);
	histo1F.push_back(histTRG[i]);
      }
      TH1F *timet2 = new TH1F("timet2", "Time in T2", 400, 0, 400.);
      histo1F.push_back(timet2);
      TH1F *hTimeSelected = new TH1F("hTimeSelected", "Time of selected events", 200, -100, 100.);
      histo1F.push_back(hTimeSelected);

      TH1F *histSRD[3];
      TH1F *histSRDcal[3];
//      TH1F *histSRDped0[3];
//      TH1F *histSRDped1[3];
      TH1F *histSRDtime[3];
      
      // -------- SRD ----------------------      
      for (int i = 0; i < 3; i++) {
        sprintf(histoname,"hSRD%d",i);
        sprintf(title,"SRD x%d",i);
	histSRD[i] = new TH1F(histoname, title, 4000, 0, 4000);
        histo1F.push_back(histSRD[i]);
      }
      for (int i = 0; i < 3; i++) {
	sprintf(histoname,"hSRDcal_%d",i);
	sprintf(title,"SRDcal_%d",i);
	histSRDcal[i] = new TH1F(histoname, title, 500, 0, 500);
	histo1F.push_back(histSRDcal[i]);
      }
      for (int i = 0; i < 3; i++) {
        sprintf(histoname,"hSRDtime_%d",i);
	sprintf(title,"SRD time ch %d - time T2",i);
	histSRDtime[i] = new TH1F(histoname, title, 200, -100, 100);
        histo1F.push_back(histSRDtime[i]);
      }
      TH1F *hSumSRDcal = new TH1F("hSumSRDcal", "Sum of 3 SRD cells, MeV", 500, 0, 500);
      histo1F.push_back(hSumSRDcal);

      TH1F *hNumberEventsInSpill = new TH1F("hNumberEventsInSpill", "Number of Events in Spill",
				      1000, 0, 40000);
      histo1F.push_back(hNumberEventsInSpill);

      // -------- VETO ---------------------- 
      TH1F *histVETO[6];
      TH1F *histVETOcal[6];
      TH1F *histVETOtime[6];
//    TH1F *histVETOped0[6];
//    TH1F *histVETOped1[6];

      for (int i = 0; i < 6; i++) {
	sprintf(histoname,"hVETO%d",i);
	sprintf(title,"VETO x%d",i);
	histVETO[i] = new TH1F(histoname, title, 1000, 0, 4000);
        histo1F.push_back(histVETO[i]);
      }
      for (int i = 0; i < 6; i++) {
	sprintf(histoname,"hVETOcal%d",i);
	sprintf(title,"VETOcal x%d (calibrated)",i);
	histVETOcal[i] = new TH1F(histoname, title, 100, 0, 10);
        histo1F.push_back(histVETOcal[i]);
      }
//    for (int i = 0; i < 6; i++) {
//	sprintf(histoname,"hVETOped0_%d",i);
//	sprintf(title,"VETO Ped0 - <Ped0> ch %d",i);
//	histVETOped0[i] = new TH1F(histoname, title, 200, -100, 100);
//      histo1F.push_back(histVETOped0[i]);
//    }
//    for (int i = 0; i < 6; i++) {
//	sprintf(histoname,"hVETOped1_%d",i);
//	sprintf(title,"VETO Ped1 - <Ped1> ch %d",i);
//	histVETOped1[i] = new TH1F(histoname, title, 200, -100, 100);
//      histo1F.push_back(histVETOped1[i]);
//    }
      for (int i = 0; i < 6; i++) {
	sprintf(histoname,"hVETOtime_%d",i);
	sprintf(title,"VETO time ch %d - time T2",i);
	histVETOtime[i] = new TH1F(histoname, title, 200, -100, 100);	
        histo1F.push_back(histVETOtime[i]);
      }

     TH1F *hSumVETO = new TH1F("hSumVETO", "Sum of VETO counters, Mips", 200, 0, 20.);
     histo1F.push_back(hSumVETO);
     TH1F *hSumVETO01 = new TH1F("hSumVETO01", "Sum of VETO counters 0+1, Mips", 100, 0, 10.);
     histo1F.push_back(hSumVETO01);
     TH1F *hSumVETO23 = new TH1F("hSumVETO23", "Sum of VETO counters 2+3, Mips", 100, 0, 10.);
     histo1F.push_back(hSumVETO23);
     TH1F *hSumVETO45 = new TH1F("hSumVETO45", "Sum of VETO counters 4+5, Mips", 100, 0, 10.);
     histo1F.push_back(hSumVETO45);

//    TH1F *hSumVETO = new TH1F("hSumVETO", "Sum of VETO counters, MeV", 200, 0, 50.);
//    histo1F.push_back(hSumVETO);
//    TH1F *hSumVETO01 = new TH1F("hSumVETO01", "VETO counters 0+1, MeV", 100, 0, 20.);
//    histo1F.push_back(hSumVETO01);
//    TH1F *hSumVETO23 = new TH1F("hSumVETO23", "VETO counters 2+3, MeV", 100, 0, 20.);
//    histo1F.push_back(hSumVETO23);
//    TH1F *hSumVETO45 = new TH1F("hSumVETO45", "VETO counters 4+5, MeV", 100, 0, 20.);
//    histo1F.push_back(hSumVETO45);

      TH1F *hSumVETO01sel = new TH1F("hSumVETO01sel", "VETO counters 0+1 (sel), Mips", 100, 0, 10.);
      histo1F.push_back(hSumVETO01sel);
      TH1F *hSumVETO23sel = new TH1F("hSumVETO23sel", "VETO counters 2+3 (sel), Mips", 100, 0, 10.);
      histo1F.push_back(hSumVETO23sel);
      TH1F *hSumVETO45sel = new TH1F("hSumVETO45sel", "VETO counters 4+5 (sel), Mips", 100, 0, 10.);
      histo1F.push_back(hSumVETO45sel);

      TH1F *histVeto12[3];
      TH1F *histVeto12cal[3];
      TH1F *histVeto12time[3];
//    TH1F *histVeto12ped0[3];
//    TH1F *histVeto12ped1[3];

#if 0
      for (int i = 0; i < 3; i++) {
        sprintf(histoname,"hVeto12_%d",i);
        sprintf(title,"Veto12 x%d",i);
        histVeto12[i] = new TH1F(histoname, title, 1000, 0, 4000);
        histo1F.push_back(histVeto12[i]);
      }
      for (int i = 0; i < 3; i++) {
        sprintf(histoname,"hVeto12cal_%d",i);
        sprintf(title,"Veto12 (calibrated) x%d",i);
        histVeto12cal[i] = new TH1F(histoname, title, 100, 0, 10.);
        histo1F.push_back(histVeto12cal[i]);
      }
#endif
//      for (int i = 0; i < 3; i++) {
//        sprintf(histoname,"hVeto12ped0_%d",i);
//        sprintf(title,"Veto12 Ped0 - <Ped0> ch %d",i);
//        histVeto12ped0[i] = new TH1F(histoname, title, 200, -100, 100);
//        histo1F.push_back(histVeto12ped0[i]);
//      }
//      for (int i = 0; i < 3; i++) {
//        sprintf(histoname,"hVeto12ped1_%d",i);
//        sprintf(title,"Veto12 Ped1 - <Ped1> ch %d",i);
//        histVeto12ped1[i] = new TH1F(histoname, title, 200, -100, 100);
//        histo1F.push_back(histVeto12ped1[i]);
//      }
      for (int i = 0; i < 3; i++) {
        sprintf(histoname,"hVeto12time_%d",i);
        sprintf(title,"Veto12 time ch %d - time T2",i);
        histVeto12time[i] = new TH1F(histoname, title, 200, -100, 100);
        histo1F.push_back(histVeto12time[i]);
      }

#if WCATCHER
      // -------- WCAL ---------------------- 
      TH1F *histWC[3];
      TH1F *histWCcal[3];
      TH1F *histWCcalInTime[3];
      float xmax[3] = {100, 10, 2500};
      int nbins[3] = {100, 100, 2500};

      for (int i = 0; i < 3; i++) {
	sprintf(histoname,"hWC%d",i);
	sprintf(title,"WCAL x%d",i);
	histWC[i] = new TH1F(histoname, title, nbins[i], 0, xmax[i]);
        histo1F.push_back(histWC[i]);
      }
      for (int i = 0; i < 3; i++) {
	sprintf(histoname,"hWCcal%d",i);
	sprintf(title,"WCcal x%d (calibrated)",i);
	histWCcal[i] = new TH1F(histoname, title, nbins[i], 0, xmax[i]);
        histo1F.push_back(histWCcal[i]);
      }
      for (int i = 0; i < 3; i++) {
	sprintf(histoname,"hWCcalInTime%d",i);
	sprintf(title,"WCcal(InTime) x%d (calibrated)",i);
	histWCcalInTime[i] = new TH1F(histoname, title, nbins[i], 0, xmax[i]);
        histo1F.push_back(histWCcalInTime[i]);
      }
#endif
      // -------- ECP0 - ECP1 --------------
      TH1F *histECP[2][6][6];
      
      TH1F *histECPcal[2];
      TH1F *histECPnocal36[2];
      TH1F *histECPcal36[2];
      TH1F *histECPnocal[2];
      
      TH1F *histECtmax[2][6][6];
      TH1F *histECtime[2][6][6];
     
      float MaxEvents(20000.);
      int NBins(1000);
      
      float range[2] = {20., 200.};
      for (int ipl = 0; ipl < 2; ipl++) {
	for (int i = 0; i < 6; i++) {
	  for (int j = 0; j < 6; j++) {
	    sprintf(histoname,"hECP%d_%d%d", ipl, i, j);
	    sprintf(title,"ECP%d (%d,%d)", ipl, i, j);
	    histECP[ipl][i][j] = new TH1F(histoname, title, 1000, 0, 5000);
	    histo1F.push_back(histECP[ipl][i][j]);
	  }
	}
	for (int i = 0; i < 6; i++) {
	  for (int j = 0; j < 6; j++) {
	    sprintf(histoname,"hEC%d_time_%d%d",ipl,i,j);
	    sprintf(title,"EC(%d) time ch %d%d - time T2",ipl,i,j);
	    histECtime[ipl][i][j] = new TH1F(histoname, title, 200, -100, 100);
	    histo1F.push_back(histECtime[ipl][i][j]);
	  }
	}
      }

      for (int ipl = 0; ipl < 2; ipl++) {
	sprintf(histoname,"hECP%d_nocal", ipl);
	sprintf(title, "Ecal_%d energy 3x3 no cal.",ipl);
	histECPnocal[ipl] = new TH1F(histoname, title, 1000, 0, 5000);
        histo1F.push_back(histECPnocal[ipl]);
	sprintf(histoname,"hECP%d_cal", ipl);
	sprintf(title, "Ecal_%d energy (calibrated) 3x3",ipl);
	histECPcal[ipl] = new TH1F(histoname, title, 2000, 0, range[ipl]);
        histo1F.push_back(histECPcal[ipl]);
	sprintf(histoname,"hECP%d_nocal36", ipl);
	sprintf(title, "Ecal_%d energy 6x6 no cal.",ipl);
	histECPnocal36[ipl] = new TH1F(histoname, title, 5000, 0, 5000);
        histo1F.push_back(histECPnocal36[ipl]);
	sprintf(histoname,"hECP%d_cal36", ipl);
	sprintf(title, "Ecal_%d energy (calibrated) 6x6",ipl);
	histECPcal36[ipl] = new TH1F(histoname, title, 500, 0, range[ipl]);
        histo1F.push_back(histECPcal36[ipl]);
      }

      TH1F *histECtotal = new TH1F("histECtotal", "Ecal energy (Ecal+Preshower)",
			     1000, 0, 200);
      histo1F.push_back(histECtotal);

      TH1F *histECtotal36 = new TH1F("histECtotal36", "Ecal energy (Ecal+Preshower) (total)",
			       1000, 0, 200);
      histo1F.push_back(histECtotal36);

      TH1F *histECtotal36afterCuts = new TH1F("histECtotal36afterCuts", "Ecal energy (EC+PS) (total)",
			       1000, 0, 200);
      histo1F.push_back(histECtotal36afterCuts);

      TH1F *histECtotalBad = new TH1F("histECtotalBad", "(Ecal+Preshower), bad time energy",
			       1000, 0, 200);
      histo1F.push_back(histECtotalBad);


      TH2F *hHCvsEC_EC33cuts = new TH2F("hHCvsEC_EC33cuts", "Hcal energy VS Ecal energy (3,3) cuts",
			    200, 0, 200, 200, 0, 200);
      histo2F.push_back(hHCvsEC_EC33cuts);

      TH2F *hHCvsEC_SRDcuts = new TH2F("hHCvsEC_SRDcuts", "Hcal energy VS Ecal energy (SRD cuts)",
			    200, 0, 200, 200, 0, 200);
      histo2F.push_back(hHCvsEC_SRDcuts);

      TH2F *hHCvsEC_VETOcuts = new TH2F("hHCvsEC_VETOcuts", "Hcal energy VS Ecal energy (VETO cuts)",
			    200, 0, 200, 200, 0, 200);
      histo2F.push_back(hHCvsEC_VETOcuts);

      TH2F *hHCvsEC_ECcuts = new TH2F("hHCvsEC_ECcuts",
				      "Hcal energy VS Ecal energy (bad EC energy cuts)",
				      200, 0, 200, 200, 0, 200);
      histo2F.push_back(hHCvsEC_ECcuts);

      TH2F *hHCvsEC_HCbadEnergyCuts = new TH2F("hHCvsEC_HCbadEnergyCuts",
						 "Hcal energy VS Ecal energy (bad HC energy cuts)",
						 200, 0, 200, 200, 0, 200);
      histo2F.push_back(hHCvsEC_HCbadEnergyCuts);

      TH2F *hHCvsEC_AllCutsAndV2cut = new TH2F("hHCvsEC_AllCutsAndV2cut",
					       "Hcal energy VS Ecal energy (bad HC energy cuts & V2 cut)",
					       200, 0, 200, 200, 0, 200);
      histo2F.push_back(hHCvsEC_AllCutsAndV2cut);


      TH2F *hHCvsEC_GoodTrack = new TH2F("hHCvsEC_GoodTrack", "Hcal energy VS Ecal energy (MM track)",
			    200, 0, 200, 200, 0, 200);
      histo2F.push_back(hHCvsEC_GoodTrack);

      TH2F *hHCvsEC_ChiSQcuts = new TH2F("hHCvsEC_ChiSQcuts", "Hcal energy VS Ecal energy (ShiSQ cuts)",
			    200, 0, 200, 200, 0, 200);
      histo2F.push_back(hHCvsEC_ChiSQcuts);

      TH2F *hHCvsEC_DimuonsCut = new TH2F("hHCvsEC_DimuonsCut", "Hcal energy VS Ecal energy (Dimuons cut)",
			    200, 0, 200, 200, 0, 200);
      histo2F.push_back(hHCvsEC_DimuonsCut);
//TH2F *hHCvsEC_ECratiocuts = new TH2F("hHCvsEC_ECratiocuts", "Hcal energy VS Ecal energy (ECratio cuts)",
//			    200, 0, 200, 200, 0, 200);
//    histo2F.push_back(hHCvsEC_ECratiocuts);

      TH2F *hHCvsEC_HCratioCuts = new TH2F("hHCvsEC_HCratioCuts", "Hcal energy VS Ecal energy (HCratio cuts)",
			    200, 0, 200, 200, 0, 200);
      histo2F.push_back(hHCvsEC_HCratioCuts);

      TH2F *hHCvsEC_HC12ratioCut = new TH2F("hHCvsEC_HC12ratioCut", "Hcal energy VS Ecal energy, ratio HC2/(HC1+HC2) cut",
			    200, 0, 200, 200, 0, 200);
      histo2F.push_back(hHCvsEC_HC12ratioCut);

      TH2F *hHCvsEC_MOMcuts = new TH2F("hHCvsEC_MOMcuts", "Hcal energy VS Ecal energy (HCmuon cuts)",
			    200, 0, 200, 200, 0, 200);
      histo2F.push_back(hHCvsEC_MOMcuts);

      TH2F *hHCvsEC_HCmuonCut = new TH2F("hHCvsEC_HCmuonCut", "Hcal energy VS Ecal energy, HC time cut",
					   200, 0, 200, 200, 0, 200);
      histo2F.push_back(hHCvsEC_HCmuonCut);

      TH2F *hHCvsEC_HCpassCut = new TH2F("hHCvsEC_HCpassCut", "Hcal energy VS Ecal energy (HC pass cuts)",
			    200, 0, 200, 200, 0, 200);
      histo2F.push_back(hHCvsEC_HCpassCut);

      TH2F *hHCvsEC_SRD1cut = new TH2F("hHCvsEC_SRD1cut", "Hcal energy VS Ecal energy (SRD1 cut)",
			    200, 0, 200, 200, 0, 200);
      histo2F.push_back(hHCvsEC_SRD1cut);


      TH2F *hHCvsEC_SRDsum = new TH2F("hHCvsEC_SRDsum", "Hcal energy VS Ecal energy (SRDsum cut)",
     			    200, 0, 200, 200, 0, 200);
      histo2F.push_back(hHCvsEC_SRDsum);


      TH2F *hHCvsEC_Angle12cut = new TH2F("hHCvsEC_Angle12cut", "Hcal energy VS Ecal energy (Angle12 cut)",
   			    200, 0, 200, 200, 0, 200);
      histo2F.push_back(hHCvsEC_Angle12cut);


      TH2F *hHCvsEC_HC3cut = new TH2F("hHCvsEC_HC3cut", "Hcal energy VS Ecal energy (HC3 cut)",
   			    200, 0, 200, 200, 0, 200);
      histo2F.push_back(hHCvsEC_HC3cut);

      TH2F *hHCvsEC_R0cut = new TH2F("hHCvsEC_R0cut", "Hcal energy VS Ecal energy (R0 cut)",
   			    200, 0, 200, 200, 0, 200);
      histo2F.push_back(hHCvsEC_R0cut);

      TH2F *hHCvsEC_OOTratioCut = new TH2F("hHCvsEC_OOTratioCut", "Hcal energy VS Ecal energy (OOTratio cut)",
   			    200, 0, 200, 200, 0, 200);
      histo2F.push_back(hHCvsEC_OOTratioCut);

      TH2F *hHCvsEC_ECnondiagCut = new TH2F("hHCvsEC_ECnondiagCut", "Hcal energy VS Ecal energy (ECnondiag cut)",
   			    200, 0, 200, 200, 0, 200);
      histo2F.push_back(hHCvsEC_ECnondiagCut);

      TH2F *hHCvsDelta = new TH2F("hHCvsDelta", "Hcal energy VS Ecal-100GeV", 200, -100, 100, 200, 0, 200);
      histo2F.push_back(hHCvsDelta);

      TProfile *profile_EC33cuts = new TProfile("profile_EC33cuts", "HC vs EC profile after EC33 cuts",
			    200, 0., 200., 0., 200.);
      profile.push_back(profile_EC33cuts);


#if 0
      // begin --- new staff from 20-Jan-2019 ----------------------------------------------------
      TH2F *hHCvsEC_ECratiocuts_1 = new TH2F("hHCvsEC_ECratiocuts_1", "Hcal energy VS Ecal energy (ECratio cuts)",
			    200, 0, 200, 200, 0, 200);
      histo2F.push_back(hHCvsEC_ECratiocuts_1);
      TH2F *hHCvsEC_OOTratioCut_2 = new TH2F("hHCvsEC_OOTratioCut_2", "Hcal energy VS Ecal energy (OOTratio cut)",
   			    200, 0, 200, 200, 0, 200);
      histo2F.push_back(hHCvsEC_OOTratioCut_2);



      TH1F *hECplot_60_1 = new TH1F("hECplot_60_1", "EC energy (dimuons selection)", 120, 0., 60.);
      histo1F.push_back(hECplot_60_1);
      TH1F *hECplot_80_1 = new TH1F("hECplot_80_1", "EC energy (dimuons selection)", 160, 0., 80.);
      histo1F.push_back(hECplot_80_1);

      TH1F *hECplot_60_2 = new TH1F("hECplot_60_2", "EC energy (dimuons selection)", 120, 0., 60.);
      histo1F.push_back(hECplot_60_2);
      TH1F *hECplot_80_2 = new TH1F("hECplot_80_2", "EC energy (dimuons selection)", 160, 0., 80.);
      histo1F.push_back(hECplot_80_2);
      // end --- new staff from 20-Jan-2019 -------------------------------------------------------

      // veto plots 
      TH2F *hVETO1vsVETO0 = new TH2F("hVETO1vsVETO0", "VETO1 VS VETO0", 200, 0, 10, 200, 0, 10);
      histo2F.push_back(hVETO1vsVETO0);

      TH2F *hVETO3vsVETO2 = new TH2F("hVETO3vsVETO2", "VETO3 VS VETO2", 200, 0, 10, 200, 0, 10);
      histo2F.push_back(hVETO3vsVETO2);

      TH2F *hVETO5vsVETO4 = new TH2F("hVETO5vsVETO4", "VETO5 VS VETO4", 200, 0, 10, 200, 0, 10);
      histo2F.push_back(hVETO5vsVETO4);

      TH2F *hVETO1vsVETO0cuts = new TH2F("hVETO1vsVETO0cuts", "VETO1 VS VETO0", 200, 0, 10, 200, 0, 10);
      histo2F.push_back(hVETO1vsVETO0cuts);

      TH2F *hVETO3vsVETO2cuts = new TH2F("hVETO3vsVETO2cuts", "VETO3 VS VETO2", 200, 0, 10, 200, 0, 10);
      histo2F.push_back(hVETO3vsVETO2cuts);

      TH2F *hVETO5vsVETO4cuts = new TH2F("hVETO5vsVETO4cuts", "VETO5 VS VETO4", 200, 0, 10, 200, 0, 10);
      histo2F.push_back(hVETO5vsVETO4cuts);
#endif

      TH2F *hRatioVsEnergy = new TH2F("hRatioVsEnergy", "EC: Ratio(EC(6x6)-EC(3x3))/EC(6x6) VS EC(6x6)",
				      500, 10., 110., 150, 0., 0.15);
      histo2F.push_back(hRatioVsEnergy);


//    for (int i = 0; i < 6; i++) {
//	sprintf(histoname,"hVETOcal%d",i);
//	sprintf(title,"VETOcal x%d (calibrated)",i);
//	histVETOcal[i] = new TH1F(histoname, title, 100, 0, 10);
//      histo1F.push_back(histVETOcal[i]);
//    }

//      TH2F *hHCvsEC9b = new TH2F("hHCvsEC9b", "Hcal energy VS Ecal energy in matrix (3,3)",
//			   200, 0, 200, 200, 0, 200);
//      histo2F.push_back(hHCvsEC9b);

      TH1F *shchisq = new TH1F("shchisq", "ChiSq: shower - (shower-predicted)", 100, 0., 20.);
      histo1F.push_back(shchisq);

      TH1F *histRatio = new TH1F("histRatio", "Ecal: Ratio = (EC(6x6)-EC3x3)/EC(6x6)", 200, 0, 0.5);
      histo1F.push_back(histRatio);

      TH1F *hECdiffOverECintime = new TH1F("hECdiffOverECintime", "Ecal: Ratio = (ECtotal-EC(6x6))/EC(6x6)", 100, 0, 1.0);
      histo1F.push_back(hECdiffOverECintime);

// OOT ratios in different pads
      TH1F *histOOTratio[8];
      for (int i = 0; i < 8; i++) {
	sprintf(histoname,"hOOTratio_%d", i);
	sprintf(title," Pad %d:  Ratio = (ECtotal-EC(6x6))/EC(6x6)", i );
	histOOTratio[i] = new TH1F(histoname, title, 200, 0., 1.);
	histo1F.push_back(histOOTratio[i]);
      }

      TH1F *hAsymmX0 = new TH1F("hAsymmX0", "Shower Asymmetry X[0]", 100, -0.5, 0.95);
      histo1F.push_back(hAsymmX0);
      TH1F *hAsymmX1 = new TH1F("hAsymmX1", "Shower Asymmetry X[1]", 100, -0.2, 0.2);
      histo1F.push_back(hAsymmX1);

      TH1F *hAsymmY0 = new TH1F("hAsymmY0", "Shower Asymmetry Y[0]", 100, -0.5, 0.95);
      histo1F.push_back(hAsymmY0);
      TH1F *hAsymmY1 = new TH1F("hAsymmY1", "Shower Asymmetry Y[1]", 100, -0.2, 0.2);
      histo1F.push_back(hAsymmY1);

// -----------------------------------------
#if 0
      TH1F *hRHC0outOverHC0total_1 = new TH1F("hRHC0outOverHC0total_1", "1) HC0: R = (HC0-HC0_11)/HC0", 100, 0, 1.0);
      histo1F.push_back(hRHC0outOverHC0total_1);
      TH1F *hRHC0outOverHC0total_2 = new TH1F("hRHC0outOverHC0total_2", "2) HC0: R = (HC0-HC0_11)/HC0", 100, 0, 1.0);
      histo1F.push_back(hRHC0outOverHC0total_2);
      TH1F *hRHC0outOverHC0total_3 = new TH1F("hRHC0outOverHC0total_3", "2) HC0: R = (HC0-HC0_11)/HC0", 100, 0, 1.0);
      histo1F.push_back(hRHC0outOverHC0total_3);
      TH1F *hRHC0outOverHC0total_4 = new TH1F("hRHC0outOverHC0total_4", "4) HC0: R = (HC0-HC0_11)/HC0", 100, 0, 1.0);
      histo1F.push_back(hRHC0outOverHC0total_4);
      TH1F *hRHC0outOverHC0total_5 = new TH1F("hRHC0outOverHC0total_5", "5) HC0: R = (HC0-HC0_11)/HC0", 100, 0, 1.0);
      histo1F.push_back(hRHC0outOverHC0total_5);
      TH1F *hRHC0outOverHC0total_6 = new TH1F("hRHC0outOverHC0total_6", "6) HC0: R = (HC0-HC0_11)/HC0", 100, 0, 1.0);
      histo1F.push_back(hRHC0outOverHC0total_6);

      TH1F *hRHC0outOverHC0total_1_a = new TH1F("hRHC0outOverHC0total_1_a", "1_a) HC0: R = (HC0-HC0_11)/HC0", 100, 0, 1.0);
      histo1F.push_back(hRHC0outOverHC0total_1_a);
      TH1F *hRHC0outOverHC0total_2_a = new TH1F("hRHC0outOverHC0total_2_a", "2_a) HC0: R = (HC0-HC0_11)/HC0", 100, 0, 1.0);
      histo1F.push_back(hRHC0outOverHC0total_2_a);
      TH1F *hRHC0outOverHC0total_3_a = new TH1F("hRHC0outOverHC0total_3_a", "2_a) HC0: R = (HC0-HC0_11)/HC0", 100, 0, 1.0);
      histo1F.push_back(hRHC0outOverHC0total_3_a);
      TH1F *hRHC0outOverHC0total_4_a = new TH1F("hRHC0outOverHC0total_4_a", "4_a) HC0: R = (HC0-HC0_11)/HC0", 100, 0, 1.0);
      histo1F.push_back(hRHC0outOverHC0total_4_a);
      TH1F *hRHC0outOverHC0total_5_a = new TH1F("hRHC0outOverHC0total_5_a", "5_a) HC0: R = (HC0-HC0_11)/HC0", 100, 0, 1.0);
      histo1F.push_back(hRHC0outOverHC0total_5_a);
      TH1F *hRHC0outOverHC0total_6_a = new TH1F("hRHC0outOverHC0total_6_a", "6_a) HC0: R = (HC0-HC0_11)/HC0", 100, 0, 1.0);
      histo1F.push_back(hRHC0outOverHC0total_6_a);
#endif

      TH1F *hRHC0outOverHC0[10];
        for (int i = 0; i < 10; i++) {
	sprintf(histoname,"hRHC0outOverHC0total_%d",i+1);
	sprintf(title,"%d) HC0: R = (HC0-HC0_11)/HC0",i+1);
	hRHC0outOverHC0[i] = new TH1F(histoname, title, 100, 0., 1.0);
        histo1F.push_back(hRHC0outOverHC0[i]);
      }
      TH1F *hRHC0outOverHC0_a[10];
        for (int i = 0; i < 10; i++) {
	sprintf(histoname,"hRHC0outOverHC0total_%d_a",i+1);
	sprintf(title,"%d_a) HC0: R = (HC0-HC0_11)/HC0",i+1);
	hRHC0outOverHC0_a[i] = new TH1F(histoname, title, 100, 0., 1.0);
        histo1F.push_back(hRHC0outOverHC0_a[i]);
      }
      TH1F *hRHC0outOverHC0total_all = new TH1F("hRHC0outOverHC0total_all", "all) HC0: R = (HC0-HC0_11)/HC0", 100, 0, 1.0);
      histo1F.push_back(hRHC0outOverHC0total_all);
      TH1F *hRHC0outOverHC0total_all_up = new TH1F("hRHC0outOverHC0total_all_up", "all_up) HC0: R = (HC0-HC0_11)/HC0", 100, 0, 1.0);
      histo1F.push_back(hRHC0outOverHC0total_all_up);

      TH1F *hRHC0outOverHC0total_all_a = new TH1F("hRHC0outOverHC0total_all_a", "all_a) HC0: R = (HC0-HC0_11)/HC0", 100, 0, 1.0);
      histo1F.push_back(hRHC0outOverHC0total_all_a);
      TH1F *hRHC0outOverHC0total_all_a_up = new TH1F("hRHC0outOverHC0total_all_a_up", "all_a_up) HC0: R = (HC0-HC0_11)/HC0", 100, 0, 1.0);
      histo1F.push_back(hRHC0outOverHC0total_all_a_up);
      
//----------------------------------------------------------------------------------------------------------------------

      TH1F *hEnergyHC3 = new TH1F("hEnergyHC3", "HC3 energy", 200, 0., 200.);
      histo1F.push_back(hEnergyHC3);

      // -------- HCP0 - HCP3 -------------- 
      TH1F *histHCP[4][3][3];
      TH1F *histHCPcal[4][3][3];
      
      TH1F *histHCPsum[4];
      TH1F *histHCPsumcal[4];

      TH1F *histHCtime[4][3][3];
      
      for (int ipl = 0; ipl < 4; ipl++) {	
	for (int i = 0; i < 3; i++) {
	  for (int j = 0; j < 3; j++) {
	    sprintf(histoname,"hHCP%d_%d%d", ipl, i, j);
	    sprintf(title,"HCP%d (%d,%d)", ipl, i, j);
	    histHCP[ipl][i][j] = new TH1F(histoname, title, 1000, 0, 4000);
	    histo1F.push_back(histHCP[ipl][i][j]);
	  }
	}  
	for (int i = 0; i < 3; i++) {
	  for (int j = 0; j < 3; j++) {
	    sprintf(histoname,"hHCPcal%d_%d%d", ipl, i, j);
	    sprintf(title,"HCPcal%d (%d,%d)", ipl, i, j);
	    histHCPcal[ipl][i][j] = new TH1F(histoname, title, 400, 0, 200);
	    histo1F.push_back(histHCPcal[ipl][i][j]);
	  }
	}  
	for (int i = 0; i < 3; i++) {
	  for (int j = 0; j < 3; j++) {	    
	    sprintf(histoname,"hHC%d_time_%d%d",ipl,i,j);
	    sprintf(title,"HC(%d) time %d%d - time T2",ipl,i,j);
	    histHCtime[ipl][i][j] = new TH1F(histoname, title, 300, -200, 100);
	    histo1F.push_back(histHCtime[ipl][i][j]);
	  }
	}  
      }

      for (int ipl = 0; ipl < 4; ipl++) {
	sprintf(histoname,"hHCP%d_sum", ipl);
	sprintf(title, "Hcal_%d sum (3x3)",ipl);
	histHCPsum[ipl] = new TH1F(histoname, title, 500, 0, 5000);
        histo1F.push_back(histHCPsum[ipl]);
	sprintf(histoname,"hHCP%d_sumcal", ipl);
	sprintf(title, "Hcal_%d sum (3x3), calibrated",ipl);
	histHCPsumcal[ipl] = new TH1F(histoname, title, 1000, 0, 200);
        histo1F.push_back(histHCPsumcal[ipl]);
      }

       TH1F *histHCtotal = new TH1F("histHCtotal", "Hcal energy (0+1+2+3), calib.", 1000, 0, 200);
       histo1F.push_back(histHCtotal);
       TH1F *histHCtotal2 = new TH1F("histHCtotal2", "Hcal energy (0+1), calib.", 1000, 0, 200);
       histo1F.push_back(histHCtotal2);
       TH1F *histHCtotalBad = new TH1F("histHCtotalBad", "Hcal energy (0+1+2+3), bad time energy",
				       1000, 0, 200);
       histo1F.push_back(histHCtotalBad);
       TH1F *histHCtotalBad2 = new TH1F("histHCtotalBad2", "Hcal energy (0+1), calib. bad time",
					1000, 0, 200);
       histo1F.push_back(histHCtotalBad2);
       TH1F *hHCratioGoodOverTotal = new TH1F("hHCratioGoodOverTotal",
				   "HC Ratio Good over Total energy", 100, 0., 2.);
       histo1F.push_back(hHCratioGoodOverTotal);

       TH1F *histHC0 = new TH1F("histHC0", "Hcal energy HC0, calib.", 1000, 0, 200);
       histo1F.push_back(histHC0);
       TH1F *histHC1 = new TH1F("histHC1", "Hcal energy HC1, calib.", 1000, 0, 200);
       histo1F.push_back(histHC1);
       TH1F *histHC2 = new TH1F("histHC2", "Hcal energy HC2, calib.", 1000, 0, 200);
       histo1F.push_back(histHC2);
       TH1F *histHC3 = new TH1F("histHC3", "Hcal energy HC3, calib.", 1000, 0, 200);
       histo1F.push_back(histHC3);

       TH1F *hHC0selected = new TH1F("hHC0selected", "Hcal0 energy (selected)", 1000, 0, 200);
       histo1F.push_back(hHC0selected);
       TH1F *hHC1selected = new TH1F("hHC1selected", "Hcal1 energy (selected)", 1000, 0, 200);
       histo1F.push_back(hHC1selected);
       TH1F *hHC2selected = new TH1F("hHC2selected", "Hcal2 energy (selected)", 1000, 0, 200);
       histo1F.push_back(hHC2selected);
       TH1F *hHC3selected = new TH1F("hHC3selected", "Hcal3 energy (selected)", 1000, 0, 200);
       histo1F.push_back(hHC3selected);

//#if MICROMEGAS
      TH1F *mm1X = new TH1F("mm1X", "X position of beam on MM1, mm", 500, -100, 100);
      histo1F.push_back(mm1X);
      TH1F *mm1Y = new TH1F("mm1Y", "Y position of beam on MM1, mm", 500, -100, 100);
      histo1F.push_back(mm1Y);
      TH1F *mm2X = new TH1F("mm2X", "X position of beam on MM2, mm", 500, -100, 100);
      histo1F.push_back(mm2X);
      TH1F *mm2Y = new TH1F("mm2Y", "Y position of beam on MM2, mm", 500, -100, 100);
      histo1F.push_back(mm2Y);
      TH1F *mm3X = new TH1F("mm3X", "X position of beam on MM3, mm", 500, -100, 100);
      histo1F.push_back(mm3X);
      TH1F *mm3Y = new TH1F("mm3Y", "Y position of beam on MM3, mm", 500, -100, 100);
      histo1F.push_back(mm3Y);
      TH1F *mm4X = new TH1F("mm4X", "X position of beam on MM4, mm", 500, -100, 100);
      histo1F.push_back(mm4X);
      TH1F *mm4Y = new TH1F("mm4Y", "Y position of beam on MM4, mm", 500, -100, 100);
      histo1F.push_back(mm4Y);
      TH1F *mm5X = new TH1F("mm5X", "X position of beam on MM5, mm", 500, -100, 100);
      histo1F.push_back(mm5X);
      TH1F *mm5Y = new TH1F("mm5Y", "Y position of beam on MM5, mm", 500, -100, 100);
      histo1F.push_back(mm5Y);
      TH1F *mm6X = new TH1F("mm6X", "X position of beam on MM6, mm", 500, -100, 100);
      histo1F.push_back(mm6X);
      TH1F *mm6Y = new TH1F("mm6Y", "Y position of beam on MM6, mm", 500, -100, 100);
      histo1F.push_back(mm6Y);

	TH1F *mm3_hit_x = new TH1F("mm3_hit_x", "real X position of beam on MM3, mm", 500, -500, 0);
        histo1F.push_back(mm3_hit_x);
	TH1F *mm3_hit_y = new TH1F("mm3_hit_y", "real Y position of beam on MM3, mm", 500, -100, 100);
        histo1F.push_back(mm3_hit_y);
	TH1F *mm4_hit_x = new TH1F("mm4_hit_x", "real X position of beam on MM4, mm", 500, -500, 0);
        histo1F.push_back(mm4_hit_x);
	TH1F *mm4_hit_y = new TH1F("mm4_hit_y", "real Y position of beam on MM4, mm", 500, -100, 100);
        histo1F.push_back(mm4_hit_y);


      TH2F *mm1 = new TH2F("mm1", "Beam Profile on MM1, x(mm), y(mm)", 500, -100, 100,
			   500, -100, 100);
      histo2F.push_back(mm1);
      TH2F *mm2 = new TH2F("mm2", "Beam Profile on MM2, x(mm), y(mm)", 500, -100, 100,
			   500, -100, 100);
      histo2F.push_back(mm2);
      TH2F *mm3 = new TH2F("mm3", "Beam Profile on MM3, x(mm), y(mm)", 500, -100, 100,
			   500, -100, 100);
      histo2F.push_back(mm3);
      TH2F *mm4 = new TH2F("mm4", "Beam Profile on MM4, x(mm), y(mm)", 500, -100, 100,
			   500, -100, 100);
      histo2F.push_back(mm4);
      TH2F *mm5 = new TH2F("mm5", "Beam Profile on MM5, x(mm), y(mm)", 500, -100, 100,
			   500, -100, 100);
      histo2F.push_back(mm5);
      TH2F *mm6 = new TH2F("mm6", "Beam Profile on MM6, x(mm), y(mm)", 500, -100, 100,
			   500, -100, 100);
      histo2F.push_back(mm6);

    TH1F *mm1X_time = new TH1F("mm1X_time", "MM1X best hit time", 1000, 0, 500);
    histo1F.push_back(mm1X_time);
    TH1F *mm1Y_time = new TH1F("mm1Y_time", "MM1Y best hit time", 1000, 0, 500);
    histo1F.push_back(mm1Y_time);
    TH1F *mm2X_time = new TH1F("mm2X_time", "MM2X best hit time", 1000, 0, 500);
    histo1F.push_back(mm2X_time);
    TH1F *mm2Y_time = new TH1F("mm2Y_time", "MM2Y best hit time", 1000, 0, 500);
    histo1F.push_back(mm2Y_time);
    TH1F *mm3X_time = new TH1F("mm3X_time", "MM3X best hit time", 1000, 0, 500);
    histo1F.push_back(mm3X_time);
    TH1F *mm3Y_time = new TH1F("mm3Y_time", "MM3Y best hit time", 1000, 0, 500);
    histo1F.push_back(mm3Y_time);
    TH1F *mm4X_time = new TH1F("mm4X_time", "MM4X best hit time", 1000, 0, 500);
    histo1F.push_back(mm4X_time);
    TH1F *mm4Y_time = new TH1F("mm4Y_time", "MM4Y best hit time", 1000, 0, 500);
    histo1F.push_back(mm4Y_time);
    TH1F *mm5X_time = new TH1F("mm5X_time", "MM5X best hit time", 1000, 0, 500);
    histo1F.push_back(mm5X_time);
    TH1F *mm5Y_time = new TH1F("mm5Y_time", "MM5Y best hit time", 1000, 0, 500);
    histo1F.push_back(mm5Y_time);
    TH1F *mm6X_time = new TH1F("mm6X_time", "MM6X best hit time", 1000, 0, 500);
    histo1F.push_back(mm6X_time);
    TH1F *mm6Y_time = new TH1F("mm6Y_time", "MM6Y best hit time", 1000, 0, 500);
    histo1F.push_back(mm6Y_time);

      TH1F *momentum = new TH1F("momentum", "Reconstructed momentum of electron, GeV",
				200, 0, 200);
      histo1F.push_back(momentum);
      TH1F *momentum1 = new TH1F("momentum1", "momentum of e, GeV (after coord. and angles cuts)",
				1000, 0, 1000);
      histo1F.push_back(momentum1);
      TH1F *mom_chi2 = new TH1F("mom_chi2", "chi2 of momentum of e", 2000, 0, 500);
      histo1F.push_back(mom_chi2);

      TH2F *momVsEC = new TH2F("momVsEC", "Momentum of electron vs EC, GeV", 200, 0, 200.,
			       200, 0, 200);
      histo2F.push_back(momVsEC);

      TH2F *ECAL_pos_mom = new TH2F("ECAL_pos_mom", "Beam Profile on ECAL for all momentums (mm)",
				    1000, -500, 0, 1000, -250, 150);
      histo2F.push_back(ECAL_pos_mom);
      TH2F *ECAL_pos_mom_1 = new TH2F("ECAL_pos_mom_1", "Beam Profile on ECAL (mm)",
				    1000, -500, 0, 1000, -250, 150);
      histo2F.push_back(ECAL_pos_mom_1);

      TH2F *HCAL_pos_mom = new TH2F("HCAL_pos_mom", "Beam Profile on HCAL for all momentums (mm)",
				    1000, -500, 0, 1000, -250, 150);
      histo2F.push_back(HCAL_pos_mom);


      TH1F *beamAngle12 = new TH1F("beamAngle12", "Angle MM1_2 & beam, mrad", 1000, 0., 100.);
      histo1F.push_back(beamAngle12);


//TH1F *beamAngle34 = new TH1F("beamAngle34", "Angle MM3_4 & beam, mrad (after MM cut)", 1000, 0., 100.);
//histo1F.push_back(beamAngle34);
      TH1F *hAngle12 = new TH1F("hAngle12", "Beam Angle MM1_2, mrad", 1000, 0., 100.);
      histo1F.push_back(hAngle12);
//TH1F *hAngle34 = new TH1F("hAngle34", "Beam Angle MM3_4, mrad", 1000, 0., 100.);
//histo1F.push_back(hAngle34);
//TH1F *hAngle34_1 = new TH1F("hAngle34_1", "Beam Angle MM3_4, mrad (after MM cut)", 1000, 0., 100.);
//histo1F.push_back(hAngle34_1);
//TH1F *hAngle34_2 = new TH1F("hAngle34_2", "Beam Angle MM3_4, mrad (after all cuts)", 1000, 0., 100.);
//histo1F.push_back(hAngle34_2);
//TH1F *hInOutDist = new TH1F("hInOutDist", "InOutDistance (after all cuts)", 1000, 0., 100.);
//histo1F.push_back(hInOutDist);
     
      TH1F *Ecal_pos_X = new TH1F("Ecal_pos_X", "X position of beam on EC, mm", 500, -500, 0);
      histo1F.push_back(Ecal_pos_X);
      TH1F *Ecal_pos_Y = new TH1F("Ecal_pos_Y", "Y position of beam on EC, mm", 500, -100, 100);
      histo1F.push_back(Ecal_pos_Y);

      TH1F *Hcal_pos_X = new TH1F("Hcal_pos_X", "X position of beam on HC, mm", 500, -500, 0);
      histo1F.push_back(Hcal_pos_X);
      TH1F *Hcal_pos_Y = new TH1F("Hcal_pos_Y", "Y position of beam on HC, mm", 500, -100, 100);
      histo1F.push_back(Hcal_pos_Y);

      TH1F *EcalX0MM = new TH1F("EcalX0MM", "X0 position of beam on EC, mm", 100, -25, 25);
      histo1F.push_back(EcalX0MM);
      TH1F *EcalY0MM = new TH1F("EcalY0MM", "Y0 position of beam on EC, mm", 100, -25, 25);
      histo1F.push_back(EcalY0MM);

      TH1F *EcalX0 = new TH1F("EcalX0", "X0 position of shower in EC, mm", 100, -25, 25);
      histo1F.push_back(EcalX0);
      TH1F *EcalY0 = new TH1F("EcalY0", "Y0 position of shower in EC, mm", 100, -25, 25);
      histo1F.push_back(EcalY0);

      TH1F *EcalX33 = new TH1F("EcalX33", "X0 position of shower in EC (SVD), mm", 100, -25, 25);
      histo1F.push_back(EcalX33);
      TH1F *EcalY33 = new TH1F("EcalY33", "Y0 position of shower in EC (SVD), mm", 100, -25, 25);
      histo1F.push_back(EcalY33);

      TH1F *Xcenter = new TH1F("Xcenter", "X0 position in EC, mm", 1000, -500, -300);
      histo1F.push_back(Xcenter);
      TH1F *Ycenter = new TH1F("Ycenter", "Y0 position in EC, mm", 1000, -100, 100);
      histo1F.push_back(Ycenter);

      TH1F *HCtotal = new TH1F("HCtotal", "Total energy in HC0 (pions)", 200, 0, 200);
      histo1F.push_back(HCtotal);
      TH1F *HCcenter = new TH1F("HCcenter", "Energy in central cell of HC0 (pions)", 200, 0, 200);
      histo1F.push_back(HCcenter);
      TH1F *HCratio = new TH1F("HCratio", "Ratio = (HCtotal - HCcenter) / HCcenter in HC0 (pions)", 100, 0, 4);
      histo1F.push_back(HCratio);

      TH1F *HCratio1 = new TH1F("HCratio1", "Ratio1 = (HCtotal - HCcenter) / HCtotal in HC0", 100, 0, 1);
      histo1F.push_back(HCratio1);
      TH1F *HCratio2 = new TH1F("HCratio2", "Ratio2 = (HCtotal - HCcenter) / HCtotal in HC1+HC2", 100, 0, 1);
      histo1F.push_back(HCratio2);
      TH1F *HC12energyRatio = new TH1F("HC12energyRatio", "HC12EnergyRatio = HCtotal2 /(HCtotal1 + HCtotal2) ", 100, 0, 1);
      histo1F.push_back(HC12energyRatio);

      TH2F *HCratioVsHCtotal = new TH2F("HCratioVsHCtotal", "HC: HCratio VS HCtotal", 200, 0, 200, 100, 0, 1);
      histo2F.push_back(HCratioVsHCtotal);

//#endif // MICROMEGAS

      //TH1F *EcalX = new TH1F("EcalX","", 80, -19.5, 19.5);
      TH1F *EcalX = new TH1F("EcalX","", 200, -200., 200.);
      histo1F.push_back(EcalX);

      //TH1F *EcalY = new TH1F("EcalY","", 80, -19.5, 19.5);
      TH1F *EcalY = new TH1F("EcalY","", 200, -200., 200.);
      histo1F.push_back(EcalY);

      //TH2F *EcalXY = new TH2F("EcalXY"," ", 80, -19.5, 19.5, 80, -19.5, 19.5);
      TH2F *EcalXY = new TH2F("EcalXY"," ", 200, -200., 200., 200, -200., 200.);
      histo2F.push_back(EcalXY);

      TH1F *EcalXbad = new TH1F("EcalXbad","X position of beam on EC, mm", 200, -50., 50.);
      histo1F.push_back(EcalXbad);

      TH1F *EcalYbad = new TH1F("EcalYbad","Y position of beam on EC, mm", 200, -50., 50.);
      histo1F.push_back(EcalYbad);

      TH2F *EcalXYbad = new TH2F("EcalXYbad"," ", 200, -50., 50., 200, -50., 50.); 
      histo2F.push_back(EcalXYbad);

//TH2F *hECledcorrVSspill = new TH2F("hECledcorrVSspill","EC ledcorr VS spill", 200, 0., 200., 200, 0., 2.); 
//histo2F.push_back(hECledcorrVSspill);

//TH2F *hHCledcorrVSspill = new TH2F("hHCledcorrVSspill","HC ledcorr VS spill", 200, 0., 200., 200, 0., 2.); 
//histo2F.push_back(hHCledcorrVSspill);



