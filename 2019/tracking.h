#pragma once

#include "TVector3.h"
#include "TMath.h"


// extrapolate the line defined by two points p1 and p2
// and find the cross-section point with the surface defined by surfZ coordinate

//const TVector3 ecal_hit = extrapolate_line(e.MM3.pos() + MM3_pos, e.MM4.pos() + 
//                          MM4_pos, ECAL_pos.Z())

TVector3 extrapolate_line(
			  const TVector3 p1,
			  const TVector3 p2,
			  const double surfZ)
{
  const TVector3 diff = p2 - p1;
  const double t = (surfZ - p2.Z()) / diff.Z();
  const TVector3 crossp = p2 + t * diff;
  return crossp;
}

////by VV
double simple_tracking3(
			const TVector3 _p1,
			const TVector3 _p2,
			const TVector3 _p3,
			double _magb_z,
			const double _mage_z,
			const double mag_field)
{
  TVector3 p1;
  TVector3 p2;
  TVector3 p3;
  double magb_z;
  double mage_z;
  
  if (1)
    //  if (_p2.Z() < (_magb_z + _mage_z)/2.0)
    {
      p1 = _p1;
      p2 = _p2;
      p3 = _p3;
      magb_z = _magb_z;
      mage_z = _mage_z;
    }
  else
    {
      p1 = _p3;
      p2 = _p2;
      p3 = _p1;
      magb_z = _mage_z;
      mage_z = _magb_z;
    }
  
  
  const TVector3 del_12 = p2 - p1;
  const TVector3 del_23 = p3 - p2;
  
  // extrapolate p1 -> p2 line on p3 surface
  const double t1 = del_23.Z() / del_12.Z();
  const TVector3 P3 = p2 + t1 * del_12;
  
  
  const TVector3 del_23_new = p3 - P3;
  
  double mag_len = fabs(mage_z - magb_z);
  
  TVector3 del_23_correct = del_23_new * ((mage_z - magb_z)/2.0 /
					  (p3.Z() - mage_z + (mage_z - magb_z)/2.0 ) ); 
  double rad = 0.5 * mag_len * sqrt(1 + pow(mag_len / del_23_correct.Perp(), 2));
  
  rad *= 0.001;
  
  const double q = 1.6E-19; // particle charge, C
  const double SI_GeV = 5.34E-19; // SI to GeV conversion = 1e9 * e * 1V / c , 
                                  // e - electron charge, c - speed of light
  const double C = mag_field * q / SI_GeV;
  
  const double mom = C * rad * cos(atan(del_12.Y()/del_12.Z()));
  
  return mom;
}
////


// simple track reconstruction
// p1, p2 - coordinates of two hit points before magnet
// p3, p4 - coordinates of two hit points after magnet
// mag_len - length of magnetic field region along Z
// mag_field - magnetic field, Tesla
// length and position units: mm

double simple_tracking(
		       const TVector3 p1,
		       const TVector3 p2,
		       const TVector3 p3,
		       const TVector3 p4,
		       const double mag_len,
		       const double mag_field)
{
  const TVector3 del_12 = p2 - p1;
  const TVector3 del_23 = p3 - p2;
  const TVector3 del_34 = p4 - p3;
  const TVector3 del_24 = p4 - p2;
  
  // extrapolate p1 -> p2 line on p3 surface
  const double t1 = del_23.Z() / del_12.Z();
  const TVector3 P3 = p2 + t1 * del_12;
  
  // extrapolate p1 -> p2 line on p4 surface
  const double t2 = del_24.Z() / del_12.Z();
  const TVector3 P4 = p2 + t2 * del_12;
  
  const TVector3 del_23_new = p3 - P3;
  const TVector3 del_24_new = p4 - P4;
  
  const double def_1 = sqrt(pow(del_12.Y(),2));
  const double def_2 = del_23_new.Perp();
  const double def_3 = del_24_new.Perp();
  
  const double q = 1.6E-19; // particle charge, C
  const double SI_GeV = 5.34E-19; // SI to GeV conversion = 1e9 * e * 1V / c , 
                                  // e - electron charge, c - speed of light
  const double C = mag_field * q / SI_GeV;
  
  const double rad = mag_len*1.E-3 * sqrt(1 + pow(del_34.Z() / (def_2-def_3), 2));
  const double mom = C * rad * cos(atan(def_1/del_12.Z()));
  
  return mom;
}

struct SimpleTrack {
  bool isDefined;
  double momentum;
  double in_theta;
  double in_theta_x;
  double in_theta_y;
  double out_theta;
  double out_theta_x;
  double out_theta_y;
  double in_out_angle;
  double in_out_distance;
  
  double chi2;
  double chi2all;
  int ndf;
  int ndfall;
  
  double mm1dx, mm1dy;
  double mm2dx, mm2dy;
  double mm3dx, mm3dy;
  double mm4dx, mm4dy;
  double mm5dx, mm5dy;
  double mm6dx, mm6dy;
  
  double x[8];
  double dy[8];
  
  int mm1hit_size, mm1hit_charge;
  int mm2hit_size, mm2hit_charge;
  int mm3hit_size, mm3hit_charge;
  int mm4hit_size, mm4hit_charge;
  int mm5hit_size, mm5hit_charge;
  int mm6hit_size, mm6hit_charge;
  
  int sum_mm;
  
  int sum_mm1;
  int sum_mm2;
  int sum_mm3;
  int sum_mm4;
  int sum_mm5;
  int sum_mm6;
  
  double momentumold;
  
  //double momentum123;
  //double momentum124;
  //double momentum431;
  //double momentum432;
  
  TVector3 ecalhit; 
  TVector3 hcalhit; 
  
SimpleTrack(): isDefined(false) {}
  
  operator bool() const { return isDefined; }
};

ostream& operator<<(ostream& os, const SimpleTrack& t)
{
  os << "SimpleTrack:\n" << "  isDefined = " << t.isDefined << "\n";
  
  if (!t) return os;
  
  const double mrad = 1e-3; // radians to milli-radians conversion
  
  os << "  momentum = " << t.momentum << " GeV/c\n"
    
     << "  in_theta = " << t.in_theta/mrad << " mrad\n"
     << "  in_theta_x = " << t.in_theta_x/mrad << " mrad\n"
     << "  in_theta_y = " << t.in_theta_y/mrad << " mrad\n"
    
     << "  out_theta = " << t.out_theta/mrad << " mrad\n"
     << "  out_theta_x = " << t.out_theta_x/mrad << " mrad\n"
     << "  out_theta_y = " << t.out_theta_y/mrad << " mrad\n"
    
     << "  in_out_angle = " << t.in_out_angle/mrad << " mrad\n"
     << "  in_out_distance = " << t.in_out_distance << " mm\n";
  
  return os;
}

// convenience function assuming hits in four MM chambers
SimpleTrack simple_tracking_4MM_2016(const RecoEvent& e)
{
  SimpleTrack st;
  
  // check enough coordinate points
  const bool mmHas4 = (e.MM1.hasHit() && e.MM2.hasHit() && e.MM3.hasHit() && e.MM4.hasHit());
  if (!mmHas4) return st;
  
  // apply aligmnent
  const TVector3 mm1 = e.MM1.abspos();
  const TVector3 mm2 = e.MM2.abspos();
  const TVector3 mm3 = e.MM3.abspos();
  const TVector3 mm4 = e.MM4.abspos();
  
  const TVector3 v12 = mm1 - mm2;
  const TVector3 v34 = mm3 - mm4;
  
  const double mag_len = fabs(MAGU_pos.pos.Z() - MAGD_pos.pos.Z());
  
  st.isDefined = true;
  st.momentum = simple_tracking(mm1, mm2, mm3, mm4, mag_len, MAG_field);
  st.in_theta = v12.Theta();
  st.in_theta_x = TMath::ATan2(v12.x(), v12.z());
  st.in_theta_y = TMath::ATan2(v12.y(), v12.z());
  st.out_theta = v34.Theta();
  st.out_theta_x = TMath::ATan2(v34.x(), v34.z());
  st.out_theta_y = TMath::ATan2(v34.y(), v34.z());
  st.in_out_angle = v12.Angle(v34);
  
  // distance between skew lines
  // https://en.wikipedia.org/wiki/Skew_lines#Distance
  const TVector3 n = v12.Cross(v34).Unit();
  st.in_out_distance = fabs(n.Dot(mm2 - mm4));
  
  return st;
}



double getApprox(double *x, double *y, double *a, double *b, int n) {
  if (n == 0) return 0.0;
  double sumx = 0;
  double sumy = 0;
  double sumx2 = 0;
  double sumxy = 0;
  for (int i = 0; i<n; i++) {
    sumx += x[i];
    sumy += y[i];
    sumx2 += x[i] * x[i];
    sumxy += x[i] * y[i];
  }
  *a = (n*sumxy - (sumx*sumy)) / (n*sumx2 - sumx*sumx);
  *b = (sumy - *a*sumx) / n;
  
  double chi2 = 0.0;
  for (int i = 0; i<n; i++) 
    {
      double err = 0.18;
      chi2 += ((*a)*x[i] + *b - y[i])*((*a)*x[i] + *b - y[i]) / (err*err);
    }
  
  return  chi2;
}

void sumMMstrip(const RecoEvent& e, SimpleTrack& st)
{
  
  st.sum_mm = 0;
  for (int iStrip = 0; iStrip < MM_size; iStrip++) 
    {
      st.sum_mm += (int)e.MM1.xplane.strips[iStrip];
      st.sum_mm += (int)e.MM1.yplane.strips[iStrip];
      st.sum_mm += (int)e.MM2.xplane.strips[iStrip];
      st.sum_mm += (int)e.MM2.yplane.strips[iStrip];
      st.sum_mm += (int)e.MM3.xplane.strips[iStrip];
      st.sum_mm += (int)e.MM3.yplane.strips[iStrip];
      st.sum_mm += (int)e.MM4.xplane.strips[iStrip];
      st.sum_mm += (int)e.MM4.yplane.strips[iStrip];
      st.sum_mm += (int)e.MM5.xplane.strips[iStrip];
      st.sum_mm += (int)e.MM5.yplane.strips[iStrip];
      st.sum_mm += (int)e.MM6.xplane.strips[iStrip];
      st.sum_mm += (int)e.MM6.yplane.strips[iStrip];
    }
  
  st.sum_mm1 = 0;
  for (int iStrip = 0; iStrip < MM_size; iStrip++) 
    {
      st.sum_mm1 += (int)e.MM1.xplane.strips[iStrip];
      st.sum_mm1 += (int)e.MM1.yplane.strips[iStrip];
    }
  
  st.sum_mm2 = 0;
  for (int iStrip = 0; iStrip < MM_size; iStrip++) 
    {
      st.sum_mm2 += (int)e.MM2.xplane.strips[iStrip];
      st.sum_mm2 += (int)e.MM2.yplane.strips[iStrip];
    }
  
  st.sum_mm3 = 0;
  for (int iStrip = 0; iStrip < MM_size; iStrip++) 
    {
      st.sum_mm3 += (int)e.MM3.xplane.strips[iStrip];
      st.sum_mm3 += (int)e.MM3.yplane.strips[iStrip];
    }
  
  st.sum_mm4 = 0;
  for (int iStrip = 0; iStrip < MM_size; iStrip++) 
    {
      st.sum_mm4 += (int)e.MM4.xplane.strips[iStrip];
      st.sum_mm4 += (int)e.MM4.yplane.strips[iStrip];
    }
  
  st.sum_mm5 = 0;
  for (int iStrip = 0; iStrip < MM_size; iStrip++) 
    {
      st.sum_mm5 += (int)e.MM5.xplane.strips[iStrip];
      st.sum_mm5 += (int)e.MM5.yplane.strips[iStrip];
    }
  
  st.sum_mm6 = 0;
  for (int iStrip = 0; iStrip < MM_size; iStrip++) 
    {
      st.sum_mm6 += (int)e.MM6.xplane.strips[iStrip];
      st.sum_mm6 += (int)e.MM6.yplane.strips[iStrip];
    }
}




SimpleTrack simple_tracking_combi(
				  vector<Hit> hits1,
				  vector<Hit> hits2,
				  vector<Hit> hits3,
				  vector<Hit> hits4,
				  vector<Hit> hits5,
				  vector<Hit> hits6
				  )
  
{
  SimpleTrack st;
  
  TVector3 mm1;
  TVector3 mm2;
  TVector3 mm3;
  TVector3 mm4;
  TVector3 mm5;
  TVector3 mm6;
  
  if (hits1.size() > 0) mm1 = hits1[0].abspos();
  if (hits2.size() > 0) mm2 = hits2[0].abspos();
  if (hits3.size() > 0) mm3 = hits3[0].abspos();
  if (hits4.size() > 0) mm4 = hits4[0].abspos();
  if (hits5.size() > 0) mm5 = hits5[0].abspos();
  if (hits6.size() > 0) mm6 = hits6[0].abspos();
  
  st.isDefined = true;
  
  //printf("====================================\n");
  
  
  const double mag_len = fabs(MAGU_pos.pos.Z() - MAGD_pos.pos.Z());
  
  if (hits1.size() > 0 && hits2.size() > 0 && hits3.size() > 0 && hits4.size() > 0)
    st.momentumold = simple_tracking(mm1, mm2, mm3, mm4, mag_len, MAG_field);
  
  double chi2_min = 999999999.9;
  int ndf = 0;
  
  for (size_t ih1 = 0; ih1 < (hits1.size() > 0 ? hits1.size() : 1); ih1++)
    for (size_t ih2 = 0; ih2 < (hits2.size() > 0 ? hits2.size() : 1); ih2++)
      for (size_t ih3 = 0; ih3 < (hits3.size() > 0 ? hits3.size() : 1); ih3++)
	for (size_t ih4 = 0; ih4 < (hits4.size() > 0 ? hits4.size() : 1); ih4++)
	  for (size_t ih5 = 0; ih5 < (hits5.size() > 0 ? hits5.size() : 1); ih5++)
	    for (size_t ih6 = 0; ih6 < (hits6.size() > 0 ? hits6.size() : 1); ih6++)
	      {
		
		if (hits1.size() > 0) mm1 = hits1[ih1].abspos();
		if (hits2.size() > 0) mm2 = hits2[ih2].abspos();
		if (hits3.size() > 0) mm3 = hits3[ih3].abspos();
		if (hits4.size() > 0) mm4 = hits4[ih4].abspos();
		if (hits5.size() > 0) mm5 = hits5[ih5].abspos();
		if (hits6.size() > 0) mm6 = hits6[ih6].abspos();
		
		
		double x[8], y[8], a, b;
		
		int n = 0;
		
		TVector3 mm[6];
		TVector3 field_hit;
		
		if (hits1.size() > 0) {x[n] = mm1.Z();  y[n] = mm1.Y(); mm[n] = mm1; n++;}
		if (hits2.size() > 0) {x[n] = mm2.Z();  y[n] = mm2.Y(); mm[n] = mm2; n++;}
		if (hits3.size() > 0) {x[n] = mm3.Z();  y[n] = mm3.Y(); mm[n] = mm3; n++;}
		if (hits4.size() > 0) {x[n] = mm4.Z();  y[n] = mm4.Y(); mm[n] = mm4; n++;}
		if (hits5.size() > 0) {x[n] = mm5.Z();  y[n] = mm5.Y(); mm[n] = mm5; n++;}
		if (hits6.size() > 0) {x[n] = mm6.Z();  y[n] = mm6.Y(); mm[n] = mm6; n++;}
		
		double chi2 = getApprox(x, y, &a, &b, n);
		
		if (chi2 < chi2_min)
		  {
		    
		    st.mm1dy = -99999999.9;  st.mm1hit_size = -9999999; st.mm1hit_charge = -9999999;
		    st.mm2dy = -99999999.9;  st.mm2hit_size = -9999999; st.mm2hit_charge = -9999999;
		    st.mm3dy = -99999999.9;  st.mm3hit_size = -9999999; st.mm3hit_charge = -9999999;
		    st.mm4dy = -99999999.9;  st.mm4hit_size = -9999999; st.mm4hit_charge = -9999999;
		    st.mm5dy = -99999999.9;  st.mm5hit_size = -9999999; st.mm5hit_charge = -9999999;
		    st.mm6dy = -99999999.9;  st.mm6hit_size = -9999999; st.mm6hit_charge = -9999999;
		    
		    
		    int iiin = 0;
		    if (hits1.size() > 0) st.mm1dy = a*x[iiin++] + b - mm1.Y();
		    if (hits2.size() > 0) st.mm2dy = a*x[iiin++] + b - mm2.Y();
		    if (hits3.size() > 0) st.mm3dy = a*x[iiin++] + b - mm3.Y();
		    if (hits4.size() > 0) st.mm4dy = a*x[iiin++] + b - mm4.Y();
		    if (hits5.size() > 0) st.mm5dy = a*x[iiin++] + b - mm5.Y();
		    if (hits6.size() > 0) st.mm6dy = a*x[iiin++] + b - mm6.Y();
		    
		    
		    for (int iX = 0; iX < 8; iX++) st.x[iX] = x[iX];
		    
		    st.mm1hit_size = (hits1.size() > 0) ? hits1[ih1].xcluster->size  + 
		      hits1[ih1].ycluster->size: 9999999;
		    st.mm2hit_size = (hits2.size() > 0) ? hits2[ih2].xcluster->size  + 
		      hits2[ih2].ycluster->size: 9999999;
		    st.mm3hit_size = (hits3.size() > 0) ? hits3[ih3].xcluster->size  + 
		      hits3[ih3].ycluster->size: 9999999;
		    st.mm4hit_size = (hits4.size() > 0) ? hits4[ih4].xcluster->size  + 
		      hits4[ih4].ycluster->size: 9999999;
		    st.mm5hit_size = (hits5.size() > 0) ? hits5[ih5].xcluster->size  + 
		      hits5[ih5].ycluster->size: 9999999;
		    st.mm6hit_size = (hits6.size() > 0) ? hits6[ih6].xcluster->size  + 
		      hits6[ih6].ycluster->size: 9999999;
		    
		    st.mm1hit_charge = (hits1.size() > 0) ? hits1[ih1].xcluster->charge_total  + 
		      hits1[ih1].ycluster->charge_total: 9999999;
		    st.mm2hit_charge = (hits2.size() > 0) ? hits2[ih2].xcluster->charge_total  + 
		      hits2[ih2].ycluster->charge_total: 9999999;
		    st.mm3hit_charge = (hits3.size() > 0) ? hits3[ih3].xcluster->charge_total  + 
		      hits3[ih3].ycluster->charge_total: 9999999;
		    st.mm4hit_charge = (hits4.size() > 0) ? hits4[ih4].xcluster->charge_total  + 
		      hits4[ih4].ycluster->charge_total: 9999999;
		    st.mm5hit_charge = (hits5.size() > 0) ? hits5[ih5].xcluster->charge_total  + 
		      hits5[ih5].ycluster->charge_total: 9999999;
		    st.mm6hit_charge = (hits6.size() > 0) ? hits6[ih6].xcluster->charge_total  + 
		      hits6[ih6].ycluster->charge_total: 9999999;
		    
		    //printf("  %10.2f  %10.2f  %10.2f  %10.2f  %10.2f  %10.2f\n", st.mm1dy,
		    //          st.mm2dy, st.mm3dy, st.mm4dy, st.mm5dy, st.mm6dy);
		    //printf("%10d   %10d\n", st.mm1hit_size, st.mm1hit_charge);
		    
		    chi2_min = chi2;
		    
		    st.isDefined = true;
		    
		    double sum = 0.0;
		    
		    sum = 0.0;
		    st.ecalhit = {0.0, 0.0, 0.0};
		    field_hit = extrapolate_line(mm1, mm2, (MAGU_pos.pos.Z() + MAGD_pos.pos.Z())/2.0);
		    for (int iMM = 2; iMM < n; iMM++) 
		      {
			sum += simple_tracking3(mm1, mm2, mm[iMM], MAGU_pos.pos.Z(), MAGD_pos.pos.Z(), 
						MAG_field);
			st.ecalhit = st.ecalhit + extrapolate_line(field_hit, mm[iMM], ECAL_pos.pos.Z());
		      }
		    st.momentum = sum/(n - 2);
		    st.ecalhit = st.ecalhit*(1.0/(n - 2));

		    //std::cout << "\n 1: ecalhit x =" << st.ecalhit.X() << "  y = " << st.ecalhit.Y() << std::endl;
		    
		    //    st.chi2all = getApprox(&x[2], &y[2], &a, &b, n - 2);
		    //    st.ndfall = n - 4;
		    st.chi2all = chi2;
		    st.ndfall = n - 2;
		    
		    //printf("%10.2f  %10.2f %10d\n", st.momentum, st.chi2, st.ndf);
		    
		    
		    int N = n;
		    n = 0;
		    if (hits1.size() > 0) {x[n] = mm1.Z();  y[n] = mm1.Y(); mm[n] = mm1; n++;}
		    if (hits2.size() > 0) {x[n] = mm2.Z();  y[n] = mm2.Y(); mm[n] = mm2; n++;}
		    
		    double mindy = 0.0;
		    double maxdy = 999999999.9;
		    ////    if (N > 2) n = 2;
		    //    if (N > 2) n = 3;
		    for (int in = 2; in < N; in++)
		      {
			double dy = a*x[in] + b - y[in];
			if (fabs(dy) < maxdy && fabs(dy) > mindy) 
			  {
			    n = 3; x[n - 1] = x[in];  y[n - 1] = y[in]; mm[n - 1] = mm[in]; maxdy = fabs(dy);
			    //          printf("%1d  %10.2f\n", in + 1, dy);
			  }
		      }
		    //printf("------\n") ;
		    mindy = maxdy;
		    maxdy = 99999999999.9;
		    ////    if (N > 3) n = 3;
		    //    if (N > 3) n = 4;
		    for (int in = 3; in < N; in++)
		      {
			double dy = a*x[in] + b - y[in];
			if (fabs(dy) < maxdy && fabs(dy) > mindy)
			  {
			    n = 4; x[n -1] = x[in];  y[n - 1] = y[in]; mm[n - 1] = mm[in]; maxdy = fabs(dy); 
			    //          printf("%1d  %10.2f\n", in + 1, dy);
			  }
		      }
		    //printf("------\n") ;
		    
		    chi2 = getApprox(x, y, &a, &b, n);
		    
		    
		    //    if (chi2/(n - 2) < st.chi2/st.ndf)
		    {
		      st.chi2 = chi2;
		      st.ndf = n - 2;
		      
		      
		      st.mm1dy = hits1.size() > 0 ? a*mm1.Z() + b - mm1.Y() : -999999999.9;
		      st.mm2dy = hits2.size() > 0 ? a*mm2.Z() + b - mm2.Y() : -999999999.9;
		      st.mm3dy = hits3.size() > 0 ? a*mm3.Z() + b - mm3.Y() : -999999999.9;
		      st.mm4dy = hits4.size() > 0 ? a*mm4.Z() + b - mm4.Y() : -999999999.9;
		      st.mm5dy = hits5.size() > 0 ? a*mm5.Z() + b - mm5.Y() : -999999999.9;
		      st.mm6dy = hits6.size() > 0 ? a*mm6.Z() + b - mm6.Y() : -999999999.9;
		      
		      if (false)
			//if (fabs(st.mm3dy) < 0.01 && fabs(st.mm4dy) < 0.01 && fabs(st.mm5dy) < 0.01)
			{
			  printf("  %10.2f  %10.2f  %10.2f  %10.2f  %10.2f  %10.2f\n", st.mm1dy, st.mm2dy, st.mm3dy, 
				 st.mm4dy, st.mm5dy, st.mm6dy);
			  printf("%10.2f  %10.2f %10d\n", st.momentum, st.chi2, st.ndf);
			}
		      
		      sum = 0.0;
		      st.ecalhit = {0.0, 0.0, 0.0};
		      st.hcalhit = {0.0, 0.0, 0.0};
		      field_hit = extrapolate_line(mm1, mm2, (MAGU_pos.pos.Z() + MAGD_pos.pos.Z())/2.0);
		      for (int iMM = 2; iMM < n; iMM++) 
			{
			  sum += simple_tracking3(mm1, mm2, mm[iMM], MAGU_pos.pos.Z(), MAGD_pos.pos.Z(), 
						  MAG_field);
			  st.ecalhit = st.ecalhit + extrapolate_line(field_hit, mm[iMM], ECAL_pos.pos.Z());
			  st.hcalhit = st.hcalhit + extrapolate_line(field_hit, mm[iMM], HCAL0_pos.pos.Z());
			  //               st.ecalhit = st.ecalhit + extrapolate_line(field_hit, mm[iMM], -1866.0);
			  //               st.ecalhit = st.ecalhit + extrapolate_line(field_hit, mm[iMM], -3344.0);
			}
		      
		      st.ecalhit = st.ecalhit*(1.0/(n - 2));
		      st.hcalhit = st.hcalhit*(1.0/(n - 2));
		      st.momentum = sum/(n - 2);

		      //std::cout << " 2: ecalhit x =" << st.ecalhit.X() << "  y = " << st.ecalhit.Y() << std::endl;
		      //std::cout << " 2: hcalhit x =" << st.hcalhit.X() << "  y = " << st.hcalhit.Y() << std::endl;

		      
		      //if (hits5.size() > 0 && hits6.size() > 0)
		      //{
		      //    st.momentum123 = simple_tracking3(mm1, mm2, mm5, MAGU_pos.pos.Z(), MAGD_pos.pos.Z(), 
		      //                                      MAG_field);
		      //    st.momentum124 = simple_tracking3(mm1, mm2, mm6, MAGU_pos.pos.Z(), MAGD_pos.pos.Z(), 
		      //                                      MAG_field);
		      
		      //    st.momentum431 = simple_tracking3(mm6, mm5, mm1, MAGD_pos.pos.Z(), MAGU_pos.pos.Z(), 
		      //                                      MAG_field);
		      //    st.momentum432 = simple_tracking3(mm6, mm5, mm2, MAGD_pos.pos.Z(), MAGU_pos.pos.Z(), 
		      //                                      MAG_field);
		      //}
		      
		    }
		    
		    //    const TVector3 v12 = mm1 - mm2;
		    //    const TVector3 v34 = mm3 - mm4;  
		    const TVector3 v12 = (mm[0].Z() > mm[1].Z()) ? mm[0] - mm[1] : mm[1] - mm[0];
		    const TVector3 v34 = (mm[2].Z() > field_hit.Z()) ? mm[2] - field_hit : field_hit - mm[2];
		    
		    st.in_theta = v12.Theta();
		    st.in_theta_x = TMath::ATan2(v12.x(), v12.z());
		    st.in_theta_y = TMath::ATan2(v12.y(), v12.z());
		    st.out_theta = v34.Theta();
		    st.out_theta_x = TMath::ATan2(v34.x(), v34.z());
		    st.out_theta_y = TMath::ATan2(v34.y(), v34.z());
		    st.in_out_angle = v12.Angle(v34);
		    
		    
		    // distance between skew lines
		    // https://en.wikipedia.org/wiki/Skew_lines#Distance
		    const TVector3 nnn = v12.Cross(v34).Unit();
		    //    st.in_out_distance = fabs(nnn.Dot(mm2 - mm3));
		    st.in_out_distance = fabs(nnn.Dot(mm[1] - mm[2]));
		    
		    
		  }
		
	      }
  
  st.chi2all = chi2_min;
  
  return st; 
}



// convenience function assuming hits in four MM chambers
SimpleTrack simple_tracking_4MM_2017(const RecoEvent& e)
{
  SimpleTrack st;
  
  vector<Hit> hits1;
  vector<Hit> hits2;
  vector<Hit> hits3;
  vector<Hit> hits4;
  vector<Hit> hits5;
  vector<Hit> hits6;
  
  // check enough coordinate points
  const bool mmHasDownStream = (e.MM5.hasHit() && e.MM7.hasHit());
  if (!mmHasDownStream) return st;
  const bool mmHasUpStream = ((e.MM1.hasHit() && e.MM3.hasHit()) || 
		              (e.MM1.hasHit() && e.MM4.hasHit()) || 
			      (e.MM2.hasHit() && e.MM3.hasHit()) || 
		              (e.MM2.hasHit() && e.MM4.hasHit()));
  if (!mmHasUpStream) return st;
  
  // apply aligmnent
  hits3 = e.MM5.hits;
  hits4 = e.MM7.hits;
  
  if((e.MM1.hasHit() && e.MM3.hasHit()))
    {
      hits1 = e.MM1.hits;
      hits2 = e.MM3.hits;
    }
  else if (e.MM2.hasHit() && e.MM3.hasHit())
    {
      hits1 = e.MM2.hits;
      hits2 = e.MM3.hits;
    }
  else if (e.MM1.hasHit() && e.MM4.hasHit())
    {
      hits1 = e.MM1.hits;
      hits2 = e.MM4.hits;
    }
  else if (e.MM2.hasHit() && e.MM4.hasHit())
    {
      hits1 = e.MM2.hits;
      hits2 = e.MM4.hits;
    }
  
  st = simple_tracking_combi(hits1, hits2, hits3, hits4, hits5, hits6);
  
  
  return st;
}

// convenience function assuming hits in four MM chambers
SimpleTrack simple_tracking_4MM_2018(const RecoEvent& e)
{
  SimpleTrack st;
  
  // check enough coordinate points
  //  const bool mmHasDownStream = (e.MM3.hasHit() && e.MM4.hasHit());
  //  const bool mmHasDownStream = (e.MM3.hasHit() && e.MM4.hasHit() && e.MM5.hasHit() && 
  //                                e.MM6.hasHit());
  const bool mmHasDownStream = (e.MM3.hasHit() || e.MM4.hasHit() || e.MM5.hasHit() || 
		                e.MM6.hasHit());
  if (!mmHasDownStream) return st;
  const bool mmHasUpStream = (e.MM1.hasHit() && e.MM2.hasHit());
  if (!mmHasUpStream) return st;
  
  vector<Hit> hits1 = e.MM1.hits;
  vector<Hit> hits2 = e.MM2.hits;
  vector<Hit> hits3 = e.MM3.hits;
  vector<Hit> hits4 = e.MM4.hits;
  vector<Hit> hits5 = e.MM5.hits;
  vector<Hit> hits6 = e.MM6.hits;
  
  st = simple_tracking_combi(hits1, hits2, hits3, hits4, hits5, hits6);
  
  sumMMstrip(e, st);
  
  
  return st;
}

// convenience function assuming hits in four MM chambers
SimpleTrack simple_tracking_4MM(const RecoEvent& e)
{
  // 2016B
  if (1776 <= e.run && e.run <= 2551) return simple_tracking_4MM_2016(e);
  
  // 2017
  if (2817 <= e.run && e.run <= 3573) return simple_tracking_4MM_2017(e);
  
  // 2018 -
  if (3855 <= e.run && e.run <= 9999) return simple_tracking_4MM_2018(e);
  
  return SimpleTrack();
}


