
//=================================================================================
// shower profile obtained from data using MM coordinates
class Shower {

public:
  Shower();
  ~Shower() { };
  void getShower(double x0, double y0, double sum9cal, double *a);
  
private:
  std::fstream inFile;
  char filename[80], grname[4];
  std::vector<TGraph2D*> g0;
  TGraph2D *g1;
  
  int na;
  double xa[9][2704];
  double ya[9][2704];
  double za[9][2704];
  double x, y, z;
};

Shower::Shower () {
  
  for (int index = 0; index < 9; index++) {
    na = 0;
    //sprintf(filename, "/afs/cern.ch/work/t/toropin/NA64/p348-daq/p348reco/2018-1/a%1dxyVar1.dat", index);
    sprintf(filename, "../../a%1dxyVar1.dat", index);
    std::fstream inFile(filename);	
    //inFile.open(filename);
    
    while(1) 
      {
	inFile >> x >> y >> z;
	xa[index][na] = x;
	ya[index][na] = y;
	za[index][na] = z;
	na += 1;
	if (inFile.eof()) break;
      }
    //inFile.close();
   
    sprintf(grname, "g0%d", index);
    g1 = new TGraph2D(grname, "", na, xa[index], ya[index], za[index]);
    g0.push_back(g1);
  }
}    
  
void Shower::getShower(double x, double y, double sum9cal, double *a) {

  // coordinate system of Shower Profile (rotated around horizontal axis):
  // (center of the local coordinate system is in the element of matrix a11).
  // ------------------------------------------------------------------------
  //  Y ^  in reality             in memory    
  //    |  a02  a12  a22          00  01  02     0  1  2       
  //    |  a01  a11  a21     ==>  10  11  12     3  4  5  
  //    |  a00  a10  a20          20  21  22     6  7  8       
  //    |----------------> x 
  
  int index = -1;
  
  for (int i = 0; i < 9; i++) a[i] = 0;
  // check if x0 or y0 is out of central cell of the shower
  if (std::abs(x) > 19 || std::abs(y) > 19) return;


  int ind0[4][9] = {{0,1,2, 3,4,5, 6,7,8}, {2,1,0, 5,4,3, 8,7,6},
                    {8,7,6, 5,4,3, 2,1,0}, {6,7,8, 3,4,5, 0,1,2}};
  
  if (x >= 0 && y >= 0) index = 0;
  if (x <= 0 && y >= 0) index = 1;
  if (x <= 0 && y <= 0) index = 2;
  if (x >= 0 && y <= 0) index = 3;
  if (index < 0) return;
  
  if (x < 0) x = -x;
  if (y < 0) y = -y;

  // normalized to the beam energy in calibration run for 3x3 cells
  for (int i = 0; i < 9; i++) {
    int ind = ind0[index][i];
    // normalized to e- momentum in run 2363 for 3x3 matrix
    a[i] = g0[ind]->Interpolate(x,y) * sum9cal/98.3;
  }
  return;
}
