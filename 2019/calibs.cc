#define LOCAL 0

#if LOCAL
#include <iostream>
#include <iomanip>

#include <stdlib.h>
#include <string>
#include <cmath>
#include <algorithm>
#include <vector>
#include <string>

#include <cstdio>
#include <exception>
#include <string>
#include <set>
#include <fstream>

#endif
using namespace std;

//=================================================================================

struct Calibs {

public:
  Calibs(): ecalCalibratedRun(0), hcalCalibratedRun(0), vetoCalibratedRun(0),
	    srdCalibratedRun(0), countersCalibratedRun(0), wcalCalibratedRun(0) { };
  ~Calibs() { };

  
public:
  float calibECAL[2][6][6];
  float timeECAL[2][6][6];
  float rmsECAL[2][6][6];
  float ledECAL[2][6][6];
  int   ecalCalibratedRun;
  
  float calibHCAL[4][3][3];
  float timeHCAL[4][3][3];
  float rmsHCAL[4][3][3];
  float ledHCAL[4][3][3];
  int   hcalCalibratedRun;
  
  float calibVETO[6];
  float timeVETO[6];
  float rmsVETO[6];
  int   vetoCalibratedRun;

  float calibSRD[3];
  float timeSRD[3];
  float rmsSRD[3];
  int   srdCalibratedRun;

  float calibS[5];
  float timeS[5];
  float rmsS[5];
  int   countersCalibratedRun;

  float calibV[3];
  float timeV[3];
  float rmsV[3];

  float calibT[3];
  float timeT[3];
  float rmsT[3];

  float calibWCAL[3];
  float timeWCAL[3];
  float rmsWCAL[3];
  float ledWCAL[3];
  int   wcalCalibratedRun;

  float calibHODO[3][2][15];
  float timeHODO[3][2][15];
  float rmsHODO[3][2][15];
};

void readCalibs(struct Calibs &calibs, int runNumber) {

#if LOCAL  
  bool caldebug = true;
#else
  bool caldebug = false;
#endif
  
  int ix, iy;
  float mean, time, rms, led;
  std::string name, runs, blank;
  
#if LOCAL
  std::fstream inFile("2018.xml/ECAL.xml");  
#else
  std::fstream inFile("../2018.xml/ECAL.xml");
#endif
  
  bool skip = false;
  bool pass = false;
  bool setFlag = true;
  int num1 = 0, num2 = 0;
  
  //if (caldebug) std::cout << " ======== Run: " << runNumber << " ========= " << std::endl;

  if (runNumber != calibs.ecalCalibratedRun) { 
    while(1) 
      {
	inFile >> name;     
	// skip comment lines
	if (!name.compare(0, 1, "<")) { // first character of comment line
	  if (!name.compare(1, 3, "!--")) {
	    skip = true;
	    continue;
	  }
	  if (!name.compare(1, 4, "runs")) { // compare run numbers
	    inFile >> blank >> runs;
	    num1 = std::stoi(&runs[1]);
	    num2 = std::stoi(&runs[8]);
	    if (runNumber >= num1 && runNumber < num2) {
	      pass = false;
	    } else {
	      pass = true;
	    }
	  } // end compare run numbers
	  skip = true;
	}
	if (!name.compare(name.size()-1, 1, ">")) { // last character of comment line
	  skip = false;
	}
	if (skip) continue;
	// end of skipping part	  
	if (name.compare(0, 4,"ECAL") == 0) {
	  if (!pass) { //continue;
	    int ind = std::stoi(&name[4]);
	    inFile >> ix >> iy >> mean >> time >> rms >> led;
	    calibs.calibECAL[ind][ix][iy] = mean > 0 ? 1./mean : 0;
	    calibs.timeECAL[ind][ix][iy] = time;// + CorrTimeECAL[ind][ix][iy];
	    calibs.rmsECAL[ind][ix][iy] = rms*3;
	    calibs.ledECAL[ind][ix][iy] = led;
	    if (setFlag) {
	      calibs.ecalCalibratedRun = runNumber;
	      setFlag = false;
	    }
	  }
	}
	if (inFile.eof()) break;
      }
    inFile.clear();
    inFile.seekg(0, std::ios_base::beg);
    
    if (caldebug) {
      std::cout << "\n  ECAL calibration for run = " << runNumber << std::endl;
      for (int k = 0; k < 2; k++) {
	std::cout << "\n calibration of ECAL" << k << ":" << std::endl;
	for(int i = 0; i < 6; i++)
	  for (int j = 0; j < 6; j++)
	    std::cout << "   " << k << "  " << i << "  " << j << "  " << calibs.calibECAL[k][i][j]
		      << "  " << calibs.timeECAL[k][i][j] << "  " << calibs.rmsECAL[k][i][j]
		      << "  " << calibs.ledECAL[k][i][j] << std::endl;
      }
    }
  }
#if LOCAL    
  std::fstream inFile1("2018.xml/HCAL.xml");
#else
  std::fstream inFile1("../2018.xml/HCAL.xml");
#endif

  num1 = 0;
  num2 = 0;
  pass = false;
  setFlag = true;
  
  if (runNumber != calibs.hcalCalibratedRun) {
    while(1) 
      {
	inFile1 >> name;     
	// skip comment lines
	if (!name.compare(0, 1, "<")) { // first character of comment line
	  if (!name.compare(1, 3, "!--")) {
	    skip = true;
	    continue;
	  }
	  if (!name.compare(1, 4, "runs")) { // compare run numbers
	    inFile1 >> blank >> runs;
	    num1 = std::stoi(&runs[1]);
	    num2 = std::stoi(&runs[8]);
	    if (runNumber >= num1 && runNumber < num2) {	      
	      pass = false;
	    } else {
	      pass = true;
	    }
	  } // end compare run numbers
	  skip = true;
	}
	if (!name.compare(name.size()-1, 1, ">")) { // last character of comment line
	  skip = false;
	}
	if (skip) continue;
	// end of skipping part
	if (name.compare(0, 4, "HCAL") == 0) {
	  if (!pass) { //continue;
	    int ind = std::stoi(&name[4]);
	    inFile1 >> ix >> iy >> mean >> time >> rms >> led;
	    calibs.calibHCAL[ind][ix][iy] = mean > 0 ? 1./mean : 0;
	    calibs.timeHCAL[ind][ix][iy] = time;// + CorrTimeHCAL[ind][ix][iy] ;
	    calibs.rmsHCAL[ind][ix][iy] = rms*3;
	    calibs.ledHCAL[ind][ix][iy] = led;
	    if (setFlag) {
	      calibs.hcalCalibratedRun = runNumber;
	      setFlag = false;
	    }
	  }
	}
	if (inFile1.eof()) break;
      }
    inFile1.clear();
    inFile1.seekg(0, std::ios_base::beg);
    
    if (caldebug) {
      std::cout << "\n  HCAL calibration for run = " << runNumber << std::endl;
      for (int k = 0; k < 4; k++) {
	std::cout << "\n calibration of HCAL" << k << ":" << std::endl;
	for(int i = 0; i < 3; i++)
	  for (int j = 0; j < 3; j++)
	    std::cout << "   " << k << "  " << i << "  " << j << "  " << calibs.calibHCAL[k][i][j]
		      << "  " << calibs.timeHCAL[k][i][j]  << "  " << calibs.rmsHCAL[k][i][j]
		      << "  " << calibs.ledHCAL[k][i][j] << std::endl;
      }
    }
  }
  
#if LOCAL    
  std::fstream inFile2("2018.xml/VETO.xml");
#else  
  std::fstream inFile2("../2018.xml/VETO.xml");
#endif

  num1 = 0;
  num2 = 0;
  pass = false;
  setFlag = true;
  
  if (runNumber != calibs.vetoCalibratedRun) {
    while(1) 
      {
	inFile2 >> name;     
	// skip comment lines
	if (!name.compare(0, 1, "<")) { // first character of comment line
	  if (!name.compare(1, 3, "!--")) {
	    skip = true;
	    continue;
	  }
	  if (!name.compare(1, 4, "runs")) { // compare run numbers
	    inFile2 >> blank >> runs;
	    num1 = std::stoi(&runs[1]);
	    num2 = std::stoi(&runs[8]);
	    if (runNumber >= num1 && runNumber < num2) {
	      pass = false;
	    } else {
	      pass = true;
	    }
	  } // end compare run numbers
	  skip = true;
	}
	if (!name.compare(name.size()-1, 1, ">")) { // last character of comment line
	  skip = false;
	}
	
	if (skip) continue;
	if (name.compare(0, 4,"VETO") == 0) {
	  if (!pass) { //continue;
	    int ind = std::stoi(&name[4]);
	    inFile2  >> mean >> time >> rms;
	    calibs.calibVETO[ind] = mean > 0 ? 1./mean : 0;
	    calibs.timeVETO[ind] = time;// + CorrTimeVETO[ind];
	    calibs.rmsVETO[ind] = rms*3;
	    if (setFlag) {
	      calibs.vetoCalibratedRun = runNumber;
	    setFlag = false;
	    }
	  }
	}
	if (inFile2.eof()) break;
      }
    inFile2.clear();
    inFile2.seekg(0, std::ios_base::beg);
    
    if (caldebug) {
      std::cout << std::endl;
      std::cout << "\n  VETO calibration for run = " << runNumber << std::endl << std::endl;
      for (int k = 0; k < 6; k++) {
	std::cout << " calibration of VETO" << k << ":";
	std::cout << "   " << k << "  " << calibs.calibVETO[k]  << "  " << calibs.timeVETO[k]
		  << "  " << calibs.rmsVETO[k] << std::endl;
      }
    }
  }
    
#if LOCAL    
  std::fstream inFile3("2018.xml/SRD.xml");
#else  
  std::fstream inFile3("../2018.xml/SRD.xml");
#endif

  num1 = 0;
  num2 = 0;
  pass = false;
  setFlag = true;
  
  if (runNumber != calibs.srdCalibratedRun) {
    while(1) 
      {
	inFile3 >> name;
	// skip comment lines
	if (!name.compare(0, 1, "<")) { // first character of comment line
	  if (!name.compare(1, 3, "!--")) {
	    skip = true;
	    continue;
	  }
	  if (!name.compare(1, 4, "runs")) { // compare run numbers
	    inFile3 >> blank >> runs;
	    num1 = std::stoi(&runs[1]);
	    num2 = std::stoi(&runs[8]);
	    if (runNumber >= num1 && runNumber < num2) {
	      pass = false;
	    } else {
	      pass = true;
	    }
	  } // end compare run numbers
	  skip = true;
	}
	if (!name.compare(name.size()-1, 1, ">")) { // last character of comment line
	  skip = false;
	}
	
	if (skip) continue;
	if (name.compare(0, 3,"SRD") == 0) {
	  if (!pass) { //continue;
	    int ind = std::stoi(&name[3]);
	    inFile3  >> mean >> time >> rms;
	    calibs.calibSRD[ind] = mean > 0 ? 1./mean : 0;
	    calibs.timeSRD[ind] = time;// + CorrTimeSRD[ind];
	    calibs.rmsSRD[ind] = rms*3;
	    if (setFlag) {
	      calibs.srdCalibratedRun = runNumber;
	      setFlag = false;
	    }
	  }
	}
	if (inFile3.eof()) break;
      }
    inFile3.clear();
    inFile3.seekg(0, std::ios_base::beg);
    
    if (caldebug) {
      std::cout << "\n  SRD calibration for run = " << runNumber << std::endl;
      std::cout << std::endl;
      for (int k = 0; k < 3; k++) {
	std::cout << " calibration of SRD" << k << ":";
	std::cout << "   " << k << "  " << calibs.calibSRD[k] << "  " << calibs.timeSRD[k]
		  << "  " << calibs.rmsSRD[k] << std::endl;
      }
    }
  }
  
#if 1
#if LOCAL    
  std::fstream inFile4("2018.xml/BEAMCOUNTERS.xml");
#else
  std::fstream inFile4("../2018.xml/BEAMCOUNTERS.xml");
#endif
    
  num1 = 0;
  num2 = 0;
  pass = false;
  setFlag = true;

  if (runNumber != calibs.countersCalibratedRun) {
    for (int i = 0; i < 5; i++) {
      calibs.calibS[i] = 0;
      calibs.timeS[i] = 0;
      calibs.rmsS[i] = 0;
    }
    for (int i = 0; i < 3; i++) {
      calibs.calibV[i] = 0;
      calibs.timeV[i] = 0;
      calibs.rmsV[i] = 0;
    }
    for (int i = 0; i < 3; i++) {
      calibs.calibT[i] = 0;
      calibs.timeT[i] = 0;
      calibs.rmsT[i] = 0;
    }
    
    while(1) 
      {
	inFile4 >> name;
	// skip comment lines
	if (!name.compare(0, 1, "<")) { // first character of comment line
	  if (!name.compare(1, 3, "!--")) {
	    skip = true;
	    continue;
	  }
	  if (!name.compare(1, 4, "runs")) { // compare run numbers
	    inFile4 >> blank >> runs;
	    num1 = std::stoi(&runs[1]);
	    num2 = std::stoi(&runs[8]);
	    if (runNumber >= num1 && runNumber < num2) {
	      pass = false;
	    } else {
	      pass = true;
	    }
	  } // end compare run numbers
	  skip = true;
	}
	if (!name.compare(name.size()-1, 1, ">")) { // last character of comment line
	  skip = false;
	}
	if (skip) continue;
	if (name.compare(0, 1,"S") == 0) {
	  if (!pass) { //continue;
	    int ind = std::stoi(&name[1]);
	    inFile4  >> mean >> time >> rms;
	    calibs.calibS[ind] = mean > 0 ? 1./mean : 0;
	    calibs.timeS[ind] = time;
	    calibs.rmsS[ind] = rms*3;
	  }
	} else if (name.compare(0, 1,"V") == 0) {
	  if (!pass) { //continue;
	    int ind = std::stoi(&name[1]);
	    inFile4  >> mean >> time >> rms;
	    calibs.calibV[ind] = mean > 0 ? 1./mean : 0;
	    calibs.timeV[ind] = time;// + CorrTimeVETO1a2[ind];
	    calibs.rmsV[ind] = rms*3;
	  }
	} else if (name.compare(0, 1,"T") == 0) {
	  if (!pass) { //continue;
	    int ind = std::stoi(&name[1]);
	    inFile4  >> mean >> time >> rms;
	    calibs.calibT[ind] = mean > 0 ? 1./mean : 0;
	    calibs.timeT[ind] = time;
	    calibs.rmsT[ind] = rms*3;
	    //} else if (name.compare(0, 4,"WCAT") == 0) {
	    //inFile4  >> mean >> time >> rms;
	    //calibs.calibWCAT[0] = mean > 0 ? 1./mean : 0;
	    //calibs.timeWCAT[0] = time;
	    //calibs.rmsWCAT[0] = rms*3;
	    if (setFlag) {
	      calibs.countersCalibratedRun = runNumber;
	      setFlag = false;
	    }
	  }
	}	
	if (inFile4.eof()) break;
      }
    inFile4.clear();
    inFile4.seekg(0, std::ios_base::beg);  
    
    if (caldebug) {
      std::cout << "\n BEAM COUNTERS calibration for run = " << runNumber << std::endl;
      std::cout << std::endl;
      for (int k = 0; k < 5; k++) {
	std::cout << " calibration of S" << k << ":";
	std::cout << "   " << k << "  " << calibs.calibS[k] << "  " << calibs.timeS[k]
		  << "  " << calibs.rmsS[k] << std::endl;
      }
      for (int k = 0; k < 3; k++) {
	std::cout << " calibration of V" << k << ":";
	std::cout << "   " << k << "  " << calibs.calibV[k] << "  " << calibs.timeV[k]
		  << "  " << calibs.rmsV[k] << std::endl;
      }
      for (int k = 0; k < 3; k++) {
	std::cout << " calibration of T" << k << ":";
	std::cout << "   " << k << "  " << calibs.calibT[k] << "  " << calibs.timeT[k]
		  << "  " << calibs.rmsT[k] << std::endl;
      }
      //std::cout << " calibration of WCAT:";
      //std::cout << "   " << calibs.calibWCAT[0] << "  " << calibs.timeWCAT[0]
      //	      << "  " << calibs.rmsWCAT[0] << std::endl;
    }
  }
#endif
    
#if LOCAL    
  std::fstream inFile5("2018.xml/WCAL.xml");
#else  
  std::fstream inFile5("../2018.xml/WCAL.xml");
#endif

  num1 = 0;
  num2 = 0;
  pass = false;
  setFlag = true;
  
  if (runNumber != calibs.wcalCalibratedRun) {
    while(1) 
      {
	inFile5 >> name;
	if (!name.compare(0, 1, "<")) { // first character of comment line
	  if (!name.compare(1, 3, "!--")) {
	    skip = true;
	    continue;
	  }
	  if (!name.compare(1, 4, "runs")) { // compare run numbers
	    inFile5 >> blank >> runs;
	    num1 = std::stoi(&runs[1]);
	    num2 = std::stoi(&runs[8]);
	    if (runNumber >= num1 && runNumber < num2) {
	      pass = false;
	    } else {
	      pass = true;
	    }
	  } // end compare run numbers
	  skip = true;
	}
	if (!name.compare(name.size()-1, 1, ">")) { // last character of comment line
	  skip = false;
	}
	if (skip) continue;
	if (name.compare(0, 4,"WCAL") == 0) {
	  if (!pass) { //continue;
	    int ind = std::stoi(&name[4]);
	    inFile5  >> mean >> time >> rms >> led;
	    calibs.calibWCAL[ind] = mean > 0 ? 1./mean : 0;
	    calibs.timeWCAL[ind] = time;
	    calibs.rmsWCAL[ind] = rms*3;
	    calibs.ledWCAL[ind] = led;
	    if (setFlag) {
	      calibs.wcalCalibratedRun = runNumber;
	      setFlag = false;
	    }
	  }
	}
	if (inFile5.eof()) break;
      }
    inFile5.clear();
    inFile5.seekg(0, std::ios_base::beg);
    
    if (caldebug) {
      std::cout << "\n  WCAL calibrated for run = " << runNumber << std::endl;
      std::cout << std::endl;
      for (int k = 0; k < 3; k++) {
	std::cout << " calibration of WCAL" << k << ":";
	std::cout << "   " << k << "  " << calibs.calibWCAL[k] << "  " << calibs.timeWCAL[k]
		  << "  " << calibs.rmsWCAL[k] << "  " << calibs.ledWCAL[k] << std::endl;
      }
    }
  }
    
#if 0
  //std::fstream inFile5("2018.xml/HODOT.xml");  
  std::fstream inFile5("../2018.xml/HODOT.xml");  
  
  //float calibHODO[1][2][15];
  // HOD1X   0  0  37.  -3.0 4. 
  
  while(1) 
    {
      inFile5 >> name;
      
      // skip comment lines
      if (!name.compare(0, 1, "<")) { // first character of comment line
	skip = true;
      }
      if (!name.compare(name.size()-1, 1, ">")) { // last character of comment line
	skip = false;
      }
      if (skip) continue;
      // end of skipping part

      int indXY(-1);
      if (name.compare(0, 3,"HOD") == 0) {
	int ind = std::stoi(&name[3]);
	//std::cout << "  ind = " << ind << std::endl;
	if (name.compare(4, 4, "X") == 0) indXY = 0;
	else if (name.compare(4, 4, "Y") == 0) indXY = 1;
	//std::cout << "  indXY = " << indXY << std::endl;
	if (indXY < 0) continue;
	std::cout << "  ind = " << ind << " indXY = " << indXY << std::endl;
	inFile5 >> ix >> iy >> mean >> time >> rms;
	std::cout << "  ix = " << ix << "  iy = " << iy << "  mean = " << mean
		  << "  time = " << time << "  rms = " << rms << std::endl;
	calibs.calibHODO[ind][indXY][iy] = mean;// > 0 ? 1./mean : 0;
	calibs.timeHODO[ind][indXY][iy] = time;
	calibs.rmsHODO[ind][indXY][iy] = rms*3;
      }
      if (inFile5.eof()) break;
    }
  inFile5.close();
  
  if (caldebug) {
    for (int k = 0; k < 3; k++) {
      std::cout << "\n calibration of HODO" << k << ":" << std::endl;
      for(int i = 0; i < 2; i++)
	for (int j = 0; j < 15; j++)
	  std::cout << "   " << k << "  " << i << "  " << j << "  " << calibs.calibHODO[k][i][j]
		    << "  " << calibs.timeHODO[k][i][j] << "  " << calibs.rmsHODO[k][i][j]
		    << std::endl;
    }
  }
#endif    
}

#if LOCAL
int main() {
// read calibrations for .xml files for run October-November 2016
       //---------------------------------------------------------------
       Calibs calibs;
       
       int run = 4009;
       readCalibs(calibs, run);

       run = 4009;
       readCalibs(calibs, run);

       //run = 4050;
       //readCalibs(calibs, run);

       //run = 4100;
       //readCalibs(calibs, run);

       run = 4300;
       readCalibs(calibs, run);
}
#endif
//======================================================================
