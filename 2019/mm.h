#pragma once

struct Cluster {
  int bin[MM_size];       // strips index
  double charge[MM_size]; // strips charge
  double hit_time[MM_size]; // strip hit time
  double hit_t02[MM_size];  // strip t02
  double hit_t12[MM_size];  // strip t12
  double hit_sigma02[MM_size]; // strip sigma02
  double hit_sigma12[MM_size]; // strip sigma12
  double charge_total;    // total charge
  size_t size;               // total strips
  
  Cluster() : charge_total(0), size(0) {}
  
  void add_strip(const int bin_, const double charge_, const double time_, const double t02_, const double t12_, const double sigma02_, const double sigma12_)
  {
    bin[size] = bin_;
    charge[size] = charge_;
    hit_time[size] = time_;
    hit_t02[size] = t02_;
    hit_t12[size] = t12_;
    hit_sigma02[size] = sigma02_;
    hit_sigma12[size] = sigma12_;
    charge_total += charge_;
    size++;
  }
  
  // return cluster center-of-mass
  double position() const
  {
    double num = 0;
    double den = 0;
    
    for (size_t i = 0; i < size; i++) {
      num += bin[i] * charge[i];
      den += charge[i];
    }
    
    return (den != 0) ? num/den : 0;
  }
  
  double cluster_time() const
  {
    double cl_t =0;
    double tim_num=0;
    double tim_den=0;

    if(MM_time_ratio_sigma)
      {
    for(size_t i = 0; i < size ; i++) {
      if(hit_t02[i]!=0 && hit_t12[i]!=0)
        {
          tim_num+=(hit_t02[i]/pow(hit_sigma02[i],2))+(hit_t12[i]/pow(hit_sigma12[i],2));
          tim_den+=(1/pow(hit_sigma02[i],2))+(1/pow(hit_sigma12[i],2));
        }
    }
        
      }
    if(MM_time_ratio_simple)
      {
        for (size_t i = 0; i < size; i++) {
        if(hit_t02[i]!=0 && hit_t12[i]!=0)
        {
         
          tim_num += ((hit_t02[i]+hit_t12[i])/2) * charge[i];
          tim_den += charge[i];
            
          
        }
      }
      }
      
    cl_t = tim_num/tim_den;
      return (cl_t);
  }

  double cluster_time_error() const
  {
    double t_error =0;
    double t_den=0;

    for(size_t i = 0; i < size ; i++) {
      if(hit_sigma02[i]!=0 && hit_sigma12[i]!=0)t_den += ((1/pow(hit_sigma02[i],2))+(1/pow(hit_sigma12[i],2)));
        }
        
    t_error = 1/sqrt(t_den);
    return t_error;
  }

};

bool cmpByTotalCharge(const Cluster& a, const Cluster& b)
{
  return (a.charge_total < b.charge_total);
}

bool cmpBySize(const Cluster& a, const Cluster& b)
{
  return (a.size < b.size);
}

bool isClusterDeleted(const Cluster& a)
{
  return (a.size == 0);
}

struct MicromegaPlane {
  vector<CS::uint32> strips; // charge per each strip
  vector<double> strip_time; // time per each strip
  vector<double> strip_t02;
  vector<double> strip_t12;
  vector<double> strip_sigma02;
  vector<double> strip_sigma12;
  vector<Cluster> clusters;
  
  int nclusters;

  MicromegaPlane(): strips(MM_size), strip_time(MM_size), strip_t02(MM_size), strip_t12(MM_size), strip_sigma02(MM_size), strip_sigma12(MM_size) {}
  
  bool hasHit() const { return (clusters.size() > 0); }
  
  size_t bestClusterIndex() const;
  double bestHitStrip() const { return hasHit() ? clusters[bestClusterIndex()].position() : -1; };
  double bestHitTime() const { return hasHit() ? clusters[bestClusterIndex()].cluster_time() : -1;}
};

 
size_t MicromegaPlane::bestClusterIndex() const
{
  // search max cluster
  auto it = std::max_element(clusters.begin(), clusters.end(),
    MM_maxcluster_bycharge ? cmpByTotalCharge : cmpBySize);
  
  return distance(clusters.begin(), it);
}

//Hit struct

struct Hit {
  const Cluster* xcluster;
  const Cluster* ycluster;
  const DetGeo* geom;
  
  double xpos;
  double ypos;
  
  Hit(): xcluster(0), ycluster(0), geom(0), xpos(0), ypos(0) {}

  void CalculatePos()
  {
    double strip_x = xcluster->position();
    double strip_y = ycluster->position();
    
    // translate into center of MM surface
    const double center = MM_size/2.0 - 0.5;
    strip_x -= center;
    strip_y -= center;
    
    // scale to real size
    strip_x *= MM_strip_step;
    strip_y *= MM_strip_step;
    
    // rotate by 45 degree
    const double angle = geom->Angle;
    const double Kx = cos(angle);
    const double Ky = sin(angle);
    xpos = -Kx*strip_x + Ky*strip_y;
    ypos = -Ky*strip_x - Kx*strip_y;
  }
  
  TVector3 pos() const { return TVector3(xpos, ypos, 0); }
  TVector3 abspos() const {return (pos() + geom->pos); }
};

bool hitByTotalCharge(const Hit& a, const Hit& b)
{
 const double charge_a = a.xcluster->charge_total + a.ycluster->charge_total;
 const double charge_b = b.xcluster->charge_total + b.ycluster->charge_total;
 return (charge_a < charge_b);
}

bool hitBySize(const Hit& a, const Hit& b)
{
 const double size_a = a.xcluster->size + a.ycluster->size;
 const double size_b = b.xcluster->size + b.ycluster->size;
 return (size_a < size_b);
}
  

struct Micromega {
  MicromegaPlane xplane;
  MicromegaPlane yplane;
  vector<Hit> hits;
  double xpos;
  double ypos;
  const MM_calib* calib;
  const DetGeo* geom;
  
  Micromega(): xpos(0), ypos(0), calib(0), geom(0) {}
  
  bool hasHit() const { return (xplane.hasHit() && yplane.hasHit()); }
  
  TVector3 pos() const { return TVector3(xpos, ypos, 0); }

  TVector3 abspos() const {return (pos() + geom->pos); }
  
};

void DoHitAccumulation(const CS::uint16 chan, const CS::uint32* raw_q, MicromegaPlane& plane, const MM_plane_calib& calib)
{
  CS::uint32 max_q = 0;

  double Q_Wi=0;
  double Q_W=0;
  
  for (int i = 0; i < MM_apv_time_samples; i++) {
    const CS::uint32 q = raw_q[i];
    Q_Wi += q*i;
    Q_W += q;
    if (max_q < q) max_q = q;
  }

  const double A0 = raw_q[0];
  const double A1 = raw_q[1];
  const double A2 = raw_q[2];
  double t02=0;
  double t12=0;
  double sigma_t02=0;
  double sigma_t12=0;

  if(A2>0)
    {
      const double A02 = A0/A2;
      const double A12 = A1/A2;
     
      if(calib.timing.a02.parameters.r0>A02 && A02!=0) 
        {
          t02=calib.timing.a02.time(A02);     
          sigma_t02=calib.timing.a02.error(A0,A2,calib.sigma[chan]);
        }
      if(calib.timing.a12.parameters.r0>A12 && A12!=0) 
        {
           t12=calib.timing.a12.time(A12);     
           sigma_t12=calib.timing.a12.error(A1,A2,calib.sigma[chan]);
        }
    }
  
  
  const double t_avg = Q_Wi / Q_W;
  
  for(size_t i = 0; i < MM_map[chan].size(); i++) {
      const int strip = MM_map[chan][i];
      plane.strips[strip] = max_q;
      plane.strip_time[strip]=t_avg;
      plane.strip_t02[strip]=t02;
      plane.strip_t12[strip]=t12;
      plane.strip_sigma02[strip]=sigma_t02;
      plane.strip_sigma12[strip]=sigma_t12;
  }
}

void DoMMClusClear1(MicromegaPlane& plane)
{
  double clear_ratio = 5.0;

  vector<Cluster> clusters = plane.clusters;

  
  // loop over each pair of clusters
  for (size_t i = 0; i < clusters.size(); i++) {
    Cluster& c1 = clusters[i];
    if (c1.size == 0) continue; // skip deleted cluster
    
    for (size_t j = i+1; j < clusters.size(); j++) {
      Cluster& c2 = clusters[j];
      if (c2.size == 0) continue;
      
      // check if clusters c1 and c2 share common channel
      bool haveIntersection = false;

//        if(c1.charge_total<c2.charge_total/clear_ratio)c1.size = 0;
//        else if(c2.charge_total<c1.charge_total/clear_ratio)c2.size = 0;
      
      for (size_t k = 0; k < c1.size; k++) {
        for (size_t l = 0; l < c2.size; l++ ) {
          if (MM_reverse_map[c1.bin[k]] == MM_reverse_map[c2.bin[l]]) {
            haveIntersection = true;
            goto endcheck;
          }
        }
      }
      endcheck: ;
      
      if (haveIntersection) {
        // mark weakest cluster for detetion (size=0)
        if (c1.charge_total > c2.charge_total)
          c2.size = 0;
        else if (c1.charge_total < c2.charge_total)
          c1.size = 0;
      }
    }
  }
  
  // drop deleted clusters:
  //   remove_if() moves deleted clusters to the tail of array
  //   erase() truncates tail with deleted clusters
  clusters.erase(remove_if(clusters.begin(), clusters.end(), isClusterDeleted), clusters.end());

  plane.nclusters = (int)clusters.size();
}



void DoMMClusClear(MicromegaPlane& plane)
{
  double clear_ratio = 5.0;

  vector<Cluster>& clusters = plane.clusters;

  DoMMClusClear1(plane);
  
  // loop over each pair of clusters
  for (size_t i = 0; i < clusters.size(); i++) {
    Cluster& c1 = clusters[i];
    if (c1.size == 0) continue; // skip deleted cluster
    
    for (size_t j = i+1; j < clusters.size(); j++) {
      Cluster& c2 = clusters[j];
      if (c2.size == 0) continue;
      
      // check if clusters c1 and c2 share common channel
      bool haveIntersection = false;

        if(c1.charge_total<c2.charge_total/clear_ratio)c1.size = 0;
        else if(c2.charge_total<c1.charge_total/clear_ratio)c2.size = 0;
      
      for (size_t k = 0; k < c1.size; k++) {
        for (size_t l = 0; l < c2.size; l++ ) {
          if (MM_reverse_map[c1.bin[k]] == MM_reverse_map[c2.bin[l]]) {
            haveIntersection = true;
            goto endcheck;
          }
        }
      }
      endcheck: ;
      
      if (haveIntersection) {
        // mark weakest cluster for detetion (size=0)
        if (c1.charge_total > c2.charge_total)
          c2.size = 0;
        else if (c1.charge_total < c2.charge_total)
          c1.size = 0;
      }
    }
  }
  
  // drop deleted clusters:
  //   remove_if() moves deleted clusters to the tail of array
  //   erase() truncates tail with deleted clusters
  clusters.erase(remove_if(clusters.begin(), clusters.end(), isClusterDeleted), clusters.end());
}

void DoMMClustering(MicromegaPlane& plane)
{
  const vector<CS::uint32>& MM_array = plane.strips;
  const vector<double>& MM_strip_time = plane.strip_time;
  const vector<double>& MM_strip_t02 = plane.strip_t02;
  const vector<double>& MM_strip_t12 = plane.strip_t12;
  const vector<double>& MM_strip_sigma02 = plane.strip_sigma02;
  const vector<double>& MM_strip_sigma12 = plane.strip_sigma12;

  vector<Cluster>& clusters = plane.clusters;
  
  clusters.clear();
  
  // scan through all strips
  int bin = 0;
  
  while (true) {
    // skip 'empty' strips
    while (bin < MM_size && MM_array[bin] == 0) bin++;
    
    // end check
    if (bin >= MM_size) break;
    
    // accumulate cluster strips
    Cluster c;
    
    while (bin < MM_size && MM_array[bin] != 0) {
      c.add_strip(bin, MM_array[bin], MM_strip_time[bin], MM_strip_t02[bin], MM_strip_t12[bin], MM_strip_sigma02[bin], MM_strip_sigma12[bin]);
      bin++;
    }
    
    // skip small clusters
    if (c.size < MM_min_cluster_size) continue;
    
    // record cluster
    clusters.push_back(c);
    
    // end check
    if (bin >= MM_size) break;
  }
  
  if (MM_clear_clusters) DoMMClusClear(plane);
  
  //order clusters by variable of choice
  sort(clusters.begin(),clusters.end(),MM_maxcluster_bycharge ? cmpByTotalCharge : cmpBySize);
  
  /*
  cout << "Nclusters = " << clusters.size() << endl;
  for (size_t i = 0; i < clusters.size(); ++i)
    cout << i << " : size = " << clusters[i].size
         << ", total charge = " << clusters[i].charge_total
         << ", position = " << clusters[i].position()
         << endl;
  
  cout << "Max cluster index = " << plane.bestClusterIndex()
       << ", position = " << plane.bestHitStrip()
       << endl;
  */
}

void MMCalculatePos(Micromega& mm)
{
  //best hit is saved as first entry
  Hit besthit = mm.hits[0];
  mm.xpos = besthit.xpos;
  mm.ypos = besthit.ypos;
}

void DoMMReco(Micromega& mm, const MM_calib& calib, const DetGeo& geom)
{
  mm.calib = &calib;
  mm.geom = &geom;
  DoMMClustering(mm.xplane);
  DoMMClustering(mm.yplane);
  if (mm.hasHit())
    {
      vector<Cluster>& xclusters = mm.xplane.clusters;
      vector<Cluster>& yclusters = mm.yplane.clusters;
      vector<Hit>& hits = mm.hits;
      //calculate possible track candidate (put more trust on xplane)
      for(unsigned int i(0);i < xclusters.size();++i)
       for(unsigned int j(0);j < yclusters.size();++j)
        {
         Hit newhit;
         newhit.xcluster = &xclusters[i];
         newhit.ycluster = &yclusters[j];
         newhit.geom = &geom;
         newhit.CalculatePos();
         hits.push_back(newhit);
        }
      
      //TODO: sort hit on some variable that exploit information on both X and Y
      if(sortMMhits)
       sort(hits.begin(),hits.end(),MM_maxcluster_bycharge ? hitByTotalCharge : hitBySize);
      
      MMCalculatePos(mm);
    }
}
