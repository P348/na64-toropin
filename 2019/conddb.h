#pragma once

#include <fstream>
#include <sstream>
#include "TVector3.h"

//MM flags and constant
const int MM_apv_time_samples = 3;
const int MM_size = 320;
const double MM_strip_step = 0.25; // MM strip-to-strip distance in mm
const unsigned int MM_min_cluster_size = 2;
const bool MM_maxcluster_bycharge = true;
const bool MM_clear_clusters = true;
const bool MM_time_ratio_sigma=true;
const bool MM_time_ratio_simple=false;
const bool sortMMhits = false;

// Micromega
struct time_parameters
{
  //parameters for the timing
  /*parameters of the function described in: https://arxiv.org/pdf/1708.04087.pdf
    in the order:
    0:  r0
    1:  t0
    2:  a0
  */  
  double r0;
  double t0;
  double a0;
};

struct time_errors
{
    //covariance matrix follow
  /*first three entries are the standard deviation squared of the parameters,
    after that the covariance are given, in the following order assuming symmetric matrix:
    0: r02
    1: t02
    2: a02
    3: r0t0
    4: r0a0
    5: t0a0
   */
  double r02;
  double t02;
  double a02;
  double r0t0;
  double r0a0;
  double t0a0;
};

struct MM_time_point {
  time_parameters parameters;
  time_errors errors;

  //calculate time and error
  double time(const double& q_ratio) const 
  {
    return parameters.t0+parameters.a0*log((parameters.r0/q_ratio)-1); 
  }

  double error(const double& q_num,const double& q_den,const double& sigma_n) const
{
  
  double q_ratio = q_num/q_den;
  //calculate partial derivates for each parameters
  double sigma_r=(sigma_n+(1/sqrt(12)))*((sqrt(pow(q_num,2)+pow(q_den,2)))/pow(q_den,2));
  double dtdr=(parameters.a0*parameters.r0)/(pow(q_ratio,2)*(parameters.r0/q_ratio-1));
  double dtda0=log((parameters.r0/q_ratio)-1);
  double dtdt0=1;
  double dtdr0=parameters.a0/(q_ratio*(parameters.r0/q_ratio-1));
  //calculating final error with gaussian error propagation
  double sigma_t=sqrt((pow(dtdr,2)*pow(sigma_r,2))+(pow(dtdr0,2)*errors.r02)+(pow(dtdt0,2)*errors.t02)+(pow(dtda0,2)*errors.a02)+2*(dtdt0*dtdr0*errors.r0t0)+2*(dtda0*dtdr0*errors.r0a0)+2*(dtda0*dtdt0*errors.t0a0));
  
  return sigma_t;  
}
};

struct MM_plane_timing {
  //two ratio for each plane: A0/A2 and A1/A2
  MM_time_point a02;
  MM_time_point a12;
};

struct MM_plane_calib {
  double sigma[64];
  MM_plane_timing timing;  

  MM_plane_calib()
  {
    for (size_t i = 0; i < 64; ++i) sigma[i] = 0;
    
    timing.a02 = {{0,0,0}, {0,0,0,0,0,0}};
    timing.a12 = {{0,0,0}, {0,0,0,0,0,0}};
  }
};

  ostream& operator<<(ostream& os,const MM_plane_calib& e)
  {
    os << "PEDESTALS: " << endl;
     for(unsigned int i(0);i<64;++i)
      {
        os << "channel Nr." << i << " has pedestal: " << e.sigma[i] << endl;
      }
     
     os << "TIMING CONSTANT: " << endl;
     os << "A02: " << endl;
     os << "PARAMETERS: " << "r0: " << e.timing.a02.parameters.r0 << " t0: " << e.timing.a02.parameters.t0 << " a0: " << e.timing.a02.parameters.a0 <<  endl;
     os << "ERRORS: " << "r02: " << e.timing.a02.errors.r02 << " t02: " << e.timing.a02.errors.t02 << " a02 " << e.timing.a02.errors.a02 << " r0t0: " << e.timing.a02.errors.r0t0 << " r0a0: " << e.timing.a02.errors.r0a0 << " t0a0: " << e.timing.a02.errors.t0a0 << endl;
     
     os << "A12: " << endl;
     os << "PARAMETERS: " << "r0: " << e.timing.a12.parameters.r0 << " t0: " << e.timing.a12.parameters.t0 << " a0: " << e.timing.a12.parameters.a0 <<  endl;
     os << "ERRORS: " << "r02: " << e.timing.a12.errors.r02 << " t02: " << e.timing.a12.errors.t02 << " a02: " << e.timing.a12.errors.a02 << " r0t0: " << e.timing.a12.errors.r0t0 << " r0a0: " << e.timing.a12.errors.r0a0 << " t0a0: " << e.timing.a12.errors.t0a0 << endl;
     
     return os;
  }

struct MM_calib {
  MM_plane_calib xcalib;
  MM_plane_calib ycalib;
  string name;
  
  void ReadSigmaCalib(const string name)
  {
    ifstream inFile(name.c_str());
    //check if file is open
    if(!inFile)
      {
        cerr << "ERROR, cannot open file: " << name << endl;
        return;
      }
    
    unsigned int counter = 0;
    string tag;    
    while(getline(inFile,tag))
      {
        // skip comments
        if((tag.find("#") == 0)) continue;
        
        // read calibration data
        double dummy;
        double sigma;
        istringstream instring (tag);
        instring >> dummy >> dummy >> sigma >> dummy >> dummy;
        
        //calibration of x plane
        if(counter%2 == 0)
          {
            xcalib.sigma[counter/2] = sigma;
          }
        //calibration of y plane
        else
          {
            ycalib.sigma[counter/2] = sigma;
          }
        
        ++counter;
      }
    
    //cout << "Successfully read calibration from file: " << name << endl;
  }
};

ostream& operator<<(ostream& os,const MM_calib& e)
{
  os << "MM has name: " << e.name << endl;
  os << "CALIBRATION OF X PLANE: "<< endl;
  os << e.xcalib << endl;
  os << "CALIBRATION OF Y PLANE: "<< endl;
  os << e.ycalib << endl;
  return os;
}


//declare calibration
MM_calib MM1_calib;
MM_calib MM2_calib;
MM_calib MM3_calib;
MM_calib MM4_calib;
MM_calib MM5_calib;
MM_calib MM6_calib;
MM_calib MM7_calib;
MM_calib MM8_calib;
//dummy calibration
MM_calib MM_nocalibration;
  
map<int, vector<int> > MM_map; // electronic channel (0-63) to detector channel (strip 0-319) map
map<int, int> MM_reverse_map; // detector channel to electronic channel map (reverse map)


//geometry of the setup summarized
struct DetGeo {
  TVector3 pos;
  double Angle;

  DetGeo(): Angle(0) {}
};

// Struct for GEM geometry description
struct GEMGeometry {
    double size;
    double center;
    double pitch;
    double angle;
    // absolute position of the detector
    TVector3 pos;
    
    bool isSet() const { return (size != 0.); }
};

DetGeo ECAL_pos;
DetGeo S2_pos;
DetGeo ST2_pos;
DetGeo SRD_pos;
DetGeo S1_pos;
DetGeo pipe_pos;
DetGeo MAGD_pos;
DetGeo MAGU_pos;
DetGeo Veto_pos;
DetGeo S0_pos;
DetGeo ST1_pos;
DetGeo HCAL0_pos;
//MicroMegas
DetGeo MM1_pos;
DetGeo MM2_pos;
DetGeo MM3_pos;
DetGeo MM4_pos;
DetGeo MM5_pos;
DetGeo MM6_pos;
DetGeo MM7_pos;
DetGeo MM8_pos;
//GEM
DetGeo GM1_pos;
DetGeo GM2_pos;
DetGeo GM3_pos;
DetGeo GM4_pos;
GEMGeometry GM01Geometry;
GEMGeometry GM02Geometry;
GEMGeometry GM03Geometry;
GEMGeometry GM04Geometry;
// magnetic field
double MAG_field; // Tesla
double NominalBeamEnergy;

void Reset_Geometry()
{
  const DetGeo none;
  ECAL_pos = none;
  S2_pos   = none;
  GM2_pos  = none;
  ST2_pos  = none;
  GM1_pos  = none;
  SRD_pos  = none;
  S1_pos   = none;
  pipe_pos = none;
  MAGD_pos = none;
  MAGU_pos = none;
  Veto_pos = none;
  S0_pos   = none;
  ST1_pos  = none;
  HCAL0_pos = none;
  MM1_pos = none;
  MM2_pos = none;
  MM3_pos = none;
  MM4_pos = none;
  MM5_pos = none;
  MM6_pos = none;
  MM7_pos = none;
  MM8_pos = none;
  MAG_field = 0;
  
  // TODO: set default to 0. and explicitly define actual beam energy
  //       in corresponding Init_Geometry_YYYY() functions
  NominalBeamEnergy = 100.;
  
  const GEMGeometry gmnone = {0, 0, 0, 0, TVector3()};
  GM01Geometry = gmnone;
  GM02Geometry = gmnone;
  GM03Geometry = gmnone;
  GM04Geometry = gmnone;
}


// pedestal length
const int SADC_pedestal_length = 8;

// limit the range of SADC waveform for signal amplitude search
// (usefull to avoid pileup effect in the head and tail of waveform)
const size_t SADC_signal_range_begin = 10;
const size_t SADC_signal_range_end = 24;

// sampling interval
const float SADC_sampling_interval = 12.5; // ns

// t0 search method:
//  kT0_MaxSample   : index of sample with maximum amplitude
//  kT0_CenterOfMass: waveform center of mass index
//  kT0_RisingEdge  : index of waveform rising edge
enum {kT0_MaxSample, kT0_CenterOfMass, kT0_RisingEdge};
const int SADC_t0_method = kT0_RisingEdge;

// best peak search method:
//  kPeak_MaxAmplitude: 
//  kPeak_BestTiming: 
enum {kPeak_MaxAmplitude, kPeak_BestTiming};
const int SADC_peak_method = kPeak_BestTiming;

// trigger amplitude threshold (ADC counts)
const int TRIG_raw_threshold = 500;

// hodoscope amplitude threshold
const double HODO_threshold = 10.;
// Hodoscope (chile) threshold 
const double HODO2_threshold = 50.;

// MeV to GeV convertion
const double MeV = 0.001;

// SADC calibration data
struct CaloCalibData {
  string name;   // channel name
  float K;       // energy
  float tmean;   // mean time
  float tsigma;  // time spread
  bool isTmeanRelative; // tmean meaning: true - difference to master time, false - absolute time
  
  // timing calibration is defined
  bool hasTcalib() const { return (tsigma < 9999.); }
  
  float texpected(const float master) const
  {
    if (isTmeanRelative) return (master + tmean);
    
    return tmean;
  }
  
  string detname() const
  {
    const size_t pos = name.find("-");
    return (pos != string::npos) ? name.substr(0, pos) : name;
  }
  
  CaloCalibData(const std::string n = ""): name(n), K(1.), tmean(0.), tsigma(10000.), isTmeanRelative(true) {}
};

ostream& operator<<(ostream& os, const CaloCalibData& c)
{
  os << "name=" << c.name
     << " detname()=" << c.detname()
     << " K=" << c.K
     << " hasTcalib()=" << c.hasTcalib()
     << " tmean=" << c.tmean << "ns (" << (c.isTmeanRelative ? "relative" : "absolute") << ")"
     << " tsigma=" << c.tsigma << "ns";
  
  return os;
}

CaloCalibData BGO_calibration[8];
CaloCalibData SRD_calibration[3];
CaloCalibData ECAL_calibration[2][6][6];
CaloCalibData HCAL_calibration[4][3][3];
CaloCalibData VETO_calibration[6];
CaloCalibData WCAL_calibration[3];
CaloCalibData WCAT_calibration;
CaloCalibData VTWC_calibration;
CaloCalibData VTEC_calibration;
CaloCalibData S1_calibration;
CaloCalibData S2_calibration;
CaloCalibData S3_calibration;
CaloCalibData S4_calibration;
CaloCalibData T2_calibration;
CaloCalibData V1_calibration;
CaloCalibData V2_calibration;
CaloCalibData MUON_calibration[4];
CaloCalibData ECALSUM_calibration[6];
CaloCalibData HOD_calibration[3][2][16]; // indeces: [z index][0=x/1=y plane][cell id]
CaloCalibData CALO_nocalibration;

// reset calibration coefficients
void Reset_Calib()
{
  typedef CaloCalibData none;
  
  CALO_nocalibration = none();
  
  for (int x = 0; x < 8; ++x) {
    std::ostringstream oss;
    oss << "BGO-" << x;
    BGO_calibration[x] = none(oss.str());
  }
  
  for (int x = 0; x < 3; ++x) {
    std::ostringstream oss;
    oss << "SRD-" << x;
    SRD_calibration[x] = none(oss.str());
  }
  
  for (int i = 0; i < 2; ++i) {
    for (int x = 0; x < 6; ++x) {
      for (int y = 0; y < 6; ++y) {
        std::ostringstream oss;
        oss << "ECAL" << i << "-" << x << "-" << y;
        ECAL_calibration[i][x][y] = none(oss.str());
      }
    }
  }
  
  for (int i = 0; i < 4; ++i) {
    for (int x = 0; x < 3; ++x) {
      for (int y = 0; y < 3; ++y) {
        std::ostringstream oss;
        oss << "HCAL" << i << "-" << x << "-" << y;
        HCAL_calibration[i][x][y] = none(oss.str());
      }
    }
  }

  for (int i = 0; i < 6; ++i) {
    std::ostringstream oss;
    oss << "VETO-" << i;
    VETO_calibration[i] = none(oss.str());
  }

  for (int i = 0; i < 3; ++i) {
    std::ostringstream oss;
    oss << "WCAL-" << i;
    WCAL_calibration[i] = none(oss.str());
  }
  
  WCAT_calibration = none("WCAT");
  
  VTWC_calibration = none("VTWC");

  VTEC_calibration = none("VTEC");

  S1_calibration = none("S1");
  S2_calibration = none("S2");
  S3_calibration = none("S3");
  S4_calibration = none("S4");
  T2_calibration = none("T2");
  V1_calibration = none("V1");
  V2_calibration = none("V2");
  
  for (int i = 0; i < 4; ++i) {
    std::ostringstream oss;
    oss << "MUON" << i;
    MUON_calibration[i] = none(oss.str());
  }
  
  for (int i = 0; i < 6; ++i) {
    std::ostringstream oss;
    oss << "ECALSUM-" << i;
    ECALSUM_calibration[i] = none(oss.str());
  }
  
  for (int z = 0; z < 3; ++z)
    for (int p = 0; p < 2; ++p)
      for (int i = 0; i < 16; ++i) {
        // only HOD2 has 16 cells
        if ((z != 2) && (i == 15)) continue;
        
        const bool isX = (p == 0);
        std::ostringstream oss;
        oss << "HOD" << z << (isX ? "X" : "Y") << "-" << i;
        HOD_calibration[z][p][i] = none(oss.str());
      }
  
  //reset MM calib
  const MM_calib mmnone;
  MM_nocalibration = mmnone;
  
  MM1_calib=mmnone;
  MM2_calib=mmnone;
  MM3_calib=mmnone;
  MM4_calib=mmnone;
  MM5_calib=mmnone;
  MM6_calib=mmnone;
  MM7_calib=mmnone;
  MM8_calib=mmnone;

  //set MM names
  MM1_calib.name = "MM1";
  MM2_calib.name = "MM2";
  MM3_calib.name = "MM3";
  MM4_calib.name = "MM4";
  MM5_calib.name = "MM5";
  MM6_calib.name = "MM6";
  MM7_calib.name = "MM7";
  MM8_calib.name = "MM8";  
  //reset geometry
  Reset_Geometry();
}

// === calibration data for 2015 runs ===
// init calibration coefficients
// calibration data received from alexander.toropin@cern.ch
void Init_Calib_2015_BGO_ECAL_HCAL()
{
  // BGO
  const float calbgo[8] = {820, 1142, 1088, 1066, 483, 1317, 1316, 1161};
  
  for (int i = 0; i < 8; i++)
    BGO_calibration[i].K = 0.064 / calbgo[i]; // in GeV
  
  
  // ECAL
      // arrays for calibration (Ecal)
      const int Ndim = 36;

      //float c0maxEC[2] = {3.9, 82.9};
      float c0maxEC[2] = {3.04, 61.2};

      // calibration from run 411
      //float c0EC[2][Ndim] = {
      // Ecal plane 0:
      //        {1028,  378,  806, 1834,  722, 1498,
      //         624,  646,  896,  725,  459,  767,
      //         1126,  482,  780, 1013,  977, 1855,
      //         1741, 1788, 1105,  763, 1575,  469,
      //         501,  346, 1418,  883,  944, 1547,
      //         731,  998, 1279, 1037,  726,  946},
      // Ecal plane 1:        
      //        {1874, 1528, 586, 1397, 2304, 3502,
      //         2219, 2280, 1263, 894, 1008, 3106,
      //         770, 2028, 2859, 795, 2344, 1311,
      //         0,  2627, 2866, 2638, 2621, 1423,
      //         2560, 2821, 1391, 1383, 972, 864,
      //         2483, 1237, 794, 1513, 1356, 976}
      //      };
        
      // calibration from run 439
      //{0,0 0,1 0,2 0,3 0,4 0,5
      // 1,0 1,1 1,2 1,3 1,4 1,5
      // .......................
      //float c0EC[2][Ndim] = {
      //        // Ecal plane 0:
      //        {2053, 1838, 2074, 1749, 1850, 1908,
      //         1889, 1981, 2191, 1779, 2500, 1980,
      //         1906, 1597, 2373, 2047, 1574, 1949,
      //         2053, 1587, 1822, 1730, 1826, 1079,
      //         1641, 2202, 2256, 1410, 1622, 1983,
      //         1696, 1130, 2096, 1315, 1622, 1757},
      //        // Ecal plane 1:
      //        {2698, 2874, 3377, 2430, 2330, 3190,
      //         2564, 2510, 2743, 2730, 2314, 1337,
      //         2110, 2774, 2344, 2639, 2178, 2863,
      //         0,    2891, 2685, 2678, 3127, 2790,
      //         2646, 2918, 2584, 2813, 2607, 3073,
      //         2407, 2209, 2450, 2441, 3900, 3100}
      //};

     // calibration from runs 531-549
      //{0,0 0,1 0,2 0,3 0,4 0,5
      // 1,0 1,1 1,2 1,3 1,4 1,5
      // .......................
      float c0EC[2][Ndim] = { // for 80 GeV e-
        // Ecal plane 0:
        {2053, 1838, 2074, 1749, 1850, 1908,
         1889, 1776, 1820, 1480, 2500, 1980,
         1906, 1262, 1707, 1742, 1582, 1949,
         2053, 1336, 1476, 1533, 1646, 1079,
         1641, 2202, 1760, 1856, 1497, 1983,
         1696, 1130, 2096, 1315, 1622, 1757},
        // Ecal plane 1:
        {2698, 2874, 3377, 2430, 2330, 3190,
         2564, 2202, 2300, 2196, 2314, 1337,
         2110, 2266, 2276, 2290, 3632, 2863,
         0,    2398, 2613, 2418, 2479, 2790,
         2646, 2238, 2224, 2360, 2333, 3073,
         2407, 2209, 2560, 2441, 3900, 3100}
      };

      for (int ipl = 0; ipl < 2; ipl++) {
        int l = 0;
        for (int i = 0; i < 6; i++) {
          for (int j = 0; j < 6; j++) {
            float ratio = c0EC[ipl][l] > 0 ? c0maxEC[ipl]/c0EC[ipl][l] : 1;
            ECAL_calibration[ipl][i][j].K = ratio;
            l++;
          }
        }
      }
  
  // HCAL
  for (int d = 0; d < 4; ++d)
    for (int x = 0; x < 3; ++x)
      for (int y = 0; y < 3; ++y)
        HCAL_calibration[d][x][y].K = 80./2732.;
}

// === calibration data of 2016A runs ===
void Init_Calib_2016A_ECAL_cal1()
{
  // calibration data collected during runs 1070-1071 (3-Jul-2016)
  //{0,0 0,1 0,2 0,3 0,4 0,5
  // 1,0 1,1 1,2 1,3 1,4 1,5
  //........................
  float c0maxEC[2] = {5., 95.};
  float c0EC[2][6][6] = { // for 100 GeV e-
    // Ecal plane 0:
    {{1715, 1077, 1314, 1265, 1394, 1473},
     {1288, 1128, 1433, 1401, 1300, 1595},
     {1372, 1291, 1498, 1383, 1514, 1507},
     {1548, 1597, 1635, 1536, 1529, 1329},
     {1454, 1428, 1587, 1376, 1656, 1596},
     {1269, 1635, 1609, 1459, 1501, 1635}},
    // Ecal plane 1:
    {{2602, 2278, 2540, 2517, 2590, 2644},
     {2378, 2177, 2617, 2590, 2106, 3629},
     {2219, 2627, 2399, 2439, 3050, 2723},
     {2569, 2680, 2592, 2838, 2653, 2657},
     {1993, 2573, 2659, 2607, 1856, 2559},
     {2572, 2281, 2565, 2273, 2374, 2638}}
  };  
  // normalized to 100 GeV
  for (int ipl = 0; ipl < 2; ipl ++) 
    for (int j = 0; j < 6; j++) 
      for (int i = 0; i < 6; i++) {
        if (ipl == 1) ECAL_calibration[ipl][i][j].K = c0maxEC[ipl]*0.7456/c0EC[ipl][i][j];
        if (ipl == 0) ECAL_calibration[ipl][i][j].K = c0maxEC[ipl]*0.8006/c0EC[ipl][i][j];
      }
}

void Init_Calib_2016A_BGO()
{  
  // calibration during runs 1130 - 1154
  //   (0, 1, 2, 3, 4, 5, 6, 7)
  float calBGO[8] = {1701, 1261, 1772, 1390, 1197, 1471, 2054, 1396};
  float c0BGO(0.064); // 64 MeV

  for (int i = 0; i < 8; i++)
    BGO_calibration[i].K = c0BGO/calBGO[i]; // in GeV
}

void Init_Calib_2016A_SRD()
{
  // calibration data in runs 1581, 1582
  // SRD had only 2 channels during 2016A
  // 42 MeV is mean energy deposition in active media,
  // total mean energy deposition is 80 MeV (active media + absorber)
  // TODO: change calibration to 42MeV to be in line with 2016B and 2017
  SRD_calibration[0].K = 0.080 / 3365.;
  SRD_calibration[1].K = 0.080 / 1831.;
  SRD_calibration[2].K = 0.;
}

void Init_Calib_2016A_HCAL_cal1()
{
  // preliminary calibration of HCAL
  
  //                 (0,0), (0,1), (0,2), (1,0), (1,1), (1,2), (2,0), 2,1), (2,2)
  //                  ...........................................................
  //                  100 GeV, pi-
  float calHC[4][3][3] = {{{2569, 2378, 2410}, {2485, 2358, 2475}, {2402, 2454, 2552}},
                          {{   1,    1,    1}, {   1,    1,    1}, {   1,    1,    1}},
                          {{2358, 3062, 2601}, {2683, 2766, 2816}, {2693, 2710, 2872}},
                          {{2284, 2468, 2115}, {2515, 2510, 2141}, {2299, 2204, 2230}}};

  // normalized to 100 GeV
  for (int ipl = 0; ipl < 4; ipl++) 
    for (int j = 0; j < 3; j++) 
      for (int i = 0; i < 3; i++)
        HCAL_calibration[ipl][i][j].K = 100.*100./125./calHC[ipl][i][j];
}

void Init_Calib_2016A_ECAL_cal2()
{
      // ............. 10 -Jul-2016 ... run 1493 ...............
      //   {0,0 0,1 0,2 0,3 0,4 0,5
      //    1,0 1,1 1,2 1,3 1,4 1,5
      //......................................................................
      float c0maxEC[2] = {5., 95.};
      float c0EC[2][6][6] = { // for 100 GeV e-
        // Ecal plane 0: (PS)
        {{1565, 902.3, 1125, 1234, 869.1, 1413},
         {886.4, 969.1, 1357, 1003, 1284, 1126},
         {1180, 1040, 1135, 1843, 1183, 1276},
         {1252, 1307, 1218, 1342, 1409, 972.6},
         {1129, 1024, 1123, 986.8, 1273, 1470},
         {993.7, 1264, 1308, 1001, 1551, 1331}},
        // Ecal plane 1: (EC)
        {{2149, 2044, 2131, 2744, 2373, 2290},
         {2387, 2078, 2070, 2174, 2132, 3244},
         {2154, 2412, 2405, 1957, 2362, 2087},
         {2548, 2540, 2687, 2369, 2507, 2552},
         {2305, 2360, 1729, 2357, 2287, 2465},
         {2389, 1935, 2011, 2512, 2412, 2416}}
      };
  
      for (int ipl = 0; ipl < 2; ipl ++) 
        for (int j = 0; j < 6; j++) 
          for (int i = 0; i < 6; i++) {
            if (ipl == 1) ECAL_calibration[ipl][i][j].K = c0maxEC[ipl]*0.7456/c0EC[ipl][i][j];
            if (ipl == 0) ECAL_calibration[ipl][i][j].K = c0maxEC[ipl]*0.8006/c0EC[ipl][i][j];
          }
}

void Init_Calib_2016A_HCAL_cal2()
{  
      // Hcal calibration  HC0, HC1, HC2, HC3
      //              [0] (0,0), (0,1), (0,2), (1,0), (1,1), (1,2), (2,0), 2,1), (2,2)
      //              [1]  ...........................................................
      //                  100 GeV, pi-
      float cal0HC[4][3][3] = {
    {{2284, 2468, 2115}, {2515, 2510, 2141}, {2299, 2204, 2230}},
    {{2569, 2378, 2410}, {2485, 2358, 2475}, {2402, 2454, 2552}},
    {{2358, 3062, 2601}, {2683, 2766, 2816}, {2693, 2710, 2872}},
    {{1320, 1480, 1520}, {3600, 1400, 1520}, {1680, 1320,  960}}
      };
     
      // normalized to 100 GeV
      for (int ipl = 0; ipl < 4; ipl++) {
        for (int j = 0; j < 3; j++)
          for (int i = 0; i < 3; i++)
        HCAL_calibration[ipl][i][j].K = 100.*0.655/cal0HC[ipl][i][j];
     }
}

// === calibration data 2016B ===
// data is taken from pcdmrc01:/online/detector/calibrs/2016.xml/ECALT.xml on 2016-11-17:
// -rw-rw-r-- 1 daq daq 2673 Oct 24 22:41 /online/detector/calibrs/2016.xml/ECALT.xml
void Init_Calib_2016B_ECAL()
{
  // for 100 GeV e-
  float c0maxEC[2] = {5., 95.};
  //   {0,0 0,1 0,2 0,3 0,4 0,5
  //    1,0 1,1 1,2 1,3 1,4 1,5
  float c0EC[2][6][6] = {
    {
      // Ecal plane 0: (PS)
      {1747, 1166, 1886, 1709, 1382, 1896},
      {1177, 1753, 1530, 1508, 2021, 1847},
      {1504, 1344, 1512, 1654, 1700, 1711},
      {1604, 1321, 1621, 1465, 1711, 1340},
      {1798,  883, 1584, 1526, 1561, 1864},
      {1195, 1530, 1846, 1476, 1668, 1398}
    },
    {
      // Ecal plane 1: (EC)
      {2652, 2019, 2305, 2538, 2358, 2506},
      {2638, 2516, 2752, 2231, 2370, 2504},
      {1622, 2431, 2518, 2357, 2587, 2845},
      {2672, 2513, 2273, 2189, 2404, 2482},
      {2702, 2582, 2523, 2299, 2307, 2054},
      {2661, 2072, 3101, 2369, 2249, 2399}
    }
  };
  
  for (int ipl = 0; ipl < 2; ipl ++)
    for (int j = 0; j < 6; j++)
      for (int i = 0; i < 6; i++) {
        if (ipl == 0) ECAL_calibration[ipl][i][j].K = c0maxEC[ipl]*0.8006/c0EC[ipl][i][j];
        if (ipl == 1) ECAL_calibration[ipl][i][j].K = c0maxEC[ipl]*0.7456/c0EC[ipl][i][j];
      }
  
  // tmean define expected channel timing vs. master time
  //   texpected = master + tmean
  // 
  // the value computed as
  //   tmean = <tchannel - master>
  //     tchannel - channel "rising edge" time: Cell::t0_RisingEdge()
  //     master - S1 counter "center of mass" time: S1.t0_CenterOfMass(15, 32)
  //     <> - average over statistics
  float tmean[2][6][6] = {
    {
      // Ecal plane 0: (PS)
      {-42.9, -50.7, -62.7, -59.3, -52.9, -41.7},
      {-57.0, -55.0, -58.0, -55.9, -56.5, -61.5},
      {-55.0, -61.7, -61.6, -53.8, -51.0, -56.1},
      {-56.4, -52.6, -64.0, -56.1, -52.0, -68.1},
      {-55.4, -66.3, -52.0, -61.3, -54.8, -59.7},
      {-40.6, -54.9, -61.7, -60.6, -62.1, -38.2}
    },
    {
      // Ecal plane 1: (EC)
      {-38.8, -42.7, -44.8, -52.8, -39.9, -38.0},
      {-47.6, -42.7, -45.2, -46.9, -46.9, -37.8},
      {-44.3, -43.4, -44.0, -43.3, -42.3, -54.5},
      {-55.8, -32.8, -32.2, -32.2, -33.6, -55.2},
      {-57.6, -33.7, -31.6, -37.6, -43.1, -51.3},
      {-36.0, -59.4, -61.4, -60.9, -60.5, -32.8}
    }
  };

  float tsigma[2][6][6] = {
    {
      // Ecal plane 0: (PS)
      {4.0, 4.6, 4.0, 4.7, 4.9, 4.6},
      {3.0, 4.8, 4.2, 3.1, 4.3, 4.7},
      {3.2, 4.6, 4.8, 3.3, 4.5, 4.2},
      {4.7, 4.9, 3.1, 2.3, 3.4, 4.3},
      {6.5, 7.2, 7.2, 3.4, 4.3, 5.5},
      {6.3, 5.4, 5.0, 4.0, 4.6, 6.1}
    },
    {
      // Ecal plane 1: (EC)
      {4.0, 4.0, 4.1, 3.7, 4.9, 4.8},
      {3.3, 3.4, 3.0, 3.5, 4.0, 5.3},
      {3.0, 4.6, 3.0, 2.3, 3.9, 4.2},
      {4.1, 3.8, 2.3, 1.8, 3.7, 4.5},
      {3.4, 3.4, 4.3, 3.2, 4.4, 5.0},
      {4.8, 4.4, 4.3, 5.0, 5.1, 5.4}
    }
  };

  for (int ipl = 0; ipl < 2; ipl ++)
    for (int j = 0; j < 6; j++)
      for (int i = 0; i < 6; i++) {
        ECAL_calibration[ipl][i][j].tmean = tmean[ipl][i][j];
        ECAL_calibration[ipl][i][j].tsigma = tsigma[ipl][i][j];
      }

}

// data is taken from pcdmrc01:/online/detector/calibrs/2016.xml/HCAL.xml on 2016-11-17:
// -rw-rw-r-- 1 daq daq 3318 Oct 27 16:21 /online/detector/calibrs/2016.xml/HCAL.xml
void Init_Calib_2016B_HCAL()
{
  // Hcal calibration  HC0, HC1, HC2, HC3
  //              [0] (0,0), (0,1), (0,2), (1,0), (1,1), (1,2), (2,0), 2,1), (2,2)
  //              [1]  ...........................................................
  //                  100 GeV, pi-
  float cal0HC[4][3][3] = {
    {{2489, 2503, 2493}, {2493, 2502, 2491}, {2502, 2476, 2490}},
    {{2295, 2280, 2362}, {2390, 2320, 2125}, {2382, 2211, 2572}},
    {{2306, 2472, 2284}, {2473, 2522, 2482}, {2467, 2500, 2397}},
    {{2526, 2368, 2948}, {3150, 2573, 2478}, {2438, 2493, 2772}}
  };
  
  // normalized to 100 GeV
  for (int ipl = 0; ipl < 4; ipl++) {
    for (int j = 0; j < 3; j++)
      for (int i = 0; i < 3; i++)
        HCAL_calibration[ipl][i][j].K = 100.*0.655/cal0HC[ipl][i][j];
  }
  
  // see Init_Calib_2016B_ECAL() for tmean definition
  float tmean[4][3][3] = {
    {{-40., -45., -39.}, {-38., -38., -38.}, {-40., -35., -36.}},
    {{-35., -42., -40.}, {-39., -31., -32.}, {-37., -43., -39.}},
    {{-43., -33., -40.}, {-41., -37., -33.}, {-35., -39., -35.}},
    {{-44., -35., -40.}, {-33., -30., -40.}, {-36., -34., -32.}}
  };

  for (int ipl = 0; ipl < 4; ipl++) {
    for (int j = 0; j < 3; j++) {
      for (int i = 0; i < 3; i++) {
        HCAL_calibration[ipl][i][j].tmean = tmean[ipl][i][j];
        HCAL_calibration[ipl][i][j].tsigma = 4.;
      }
    }
  }

}

// calibrations prepared by Vladimir Poliakov
void Init_Calib_2016B_SRD()
{
  // 42 MeV is mean energy deposition in active media of SRD (based on simulations by M.Kirsanov)
  SRD_calibration[0].K = 0.042 / 1080.;
  SRD_calibration[1].K = 0.042 / 1900.;
  SRD_calibration[2].K = 0.042 / 1740.;
  
  // see Init_Calib_2016B_ECAL() for tmean definition
  SRD_calibration[0].tmean = -59.6;
  SRD_calibration[1].tmean = -54.1;
  SRD_calibration[2].tmean = -54.1;
  
  SRD_calibration[0].tsigma = 2.2;
  SRD_calibration[1].tsigma = 1.8;
  SRD_calibration[2].tsigma = 1.9;
}

void Init_Calib_2016B_VETO()
{
  VETO_calibration[0].K = 0.01 / 800.; // preliminarily checked ising dimuons in visible mode setup
  VETO_calibration[1].K = 0.01 / 650.;
  VETO_calibration[2].K = 0.01 / 600.;
  VETO_calibration[3].K = 0.01 / 650.;
  VETO_calibration[4].K = 0.01 / 820.;
  VETO_calibration[5].K = 0.01 / 900.;

  //VETO_calibration[0].K = 0.0096 / (1753./2.); // calibration from 30.12.2016 from Samoilenko
  //VETO_calibration[1].K = 0.0096 / (1320./2.);
  //VETO_calibration[2].K = 0.0096 / (1841./2.);
  //VETO_calibration[3].K = 0.0096 / (1550./2.);
  //VETO_calibration[4].K = 0.0096 / 820.;
  //VETO_calibration[5].K = 0.0096 / 900.;

  //Donskov: pmtime is used
  // see Init_Calib_2016B_ECAL() for tmean definition
  VETO_calibration[0].tmean = -49.6;
  VETO_calibration[1].tmean = -50.0;
  VETO_calibration[2].tmean = -49.4;
  VETO_calibration[3].tmean = -50.9;
  VETO_calibration[4].tmean = -49.9;
  VETO_calibration[5].tmean = -51.2;

  for(int iveto=0; iveto < 6; iveto++)
    VETO_calibration[iveto].tsigma = 6.;
}

// === Additional corrections to the calibration data 2016B ===
// === Can be revised or removed after the implementation of the LED corrections
void Corr_Calib_2016B(const int run)
{
  //ECAL corrections to put the peak in the run 2363 to 100 GeV
  for (int ipl = 0; ipl < 2; ipl ++)
    for (int j = 0; j < 6; j++)
      for (int i = 0; i < 6; i++) {
        if (ipl == 0) ECAL_calibration[ipl][i][j].K *= 1.09*1.0989;
        if (ipl == 1) ECAL_calibration[ipl][i][j].K *= 1.0989;
      }

  //HCAL corrections to put the dimuon peak to the place predicted by MC, mainly run 2351
  float mkfact[4];
  mkfact[0] = 1.4;
  mkfact[1] = 1.38;
  mkfact[2] = 1.237;
  mkfact[3] = 1.21;
  for (int ipl = 0; ipl < 4; ipl++) {
    for (int j = 0; j < 3; j++)
      for (int i = 0; i < 3; i++)
        HCAL_calibration[ipl][i][j].K *= mkfact[ipl];
  }

  //Corect the HCAL channel in module 2 recommutated after the HCAL calibration
  if(run >= 2114) HCAL_calibration[2][2][1].K *= 0.25;
}

void Init_Calib_2016B_VISMODE()
{
  WCAL_calibration[0].K = 100. / 1990.;
  //Donskov: pmtime is used
  // see Init_Calib_2016B_ECAL() for tmean definition
  WCAL_calibration[0].tmean = -36.5;
  WCAL_calibration[0].tsigma = 1.8;

  VTWC_calibration.K = 0.75 * 0.002 / 340.; // preliminarily checked using dimuons in visible mode setup
  VTWC_calibration.tmean = -52.5;
  VTWC_calibration.tsigma = 2.6;

  VTEC_calibration.K = 0.0038 / 300.; // preliminarily checked using dimuons in visible mode setup (0.002 changed to 0.0038)
  VTEC_calibration.tmean = -71.1;
  VTEC_calibration.tsigma = 24.9;

  S2_calibration.K = 0.00048 / 140.; // preliminarily checked using dimuons in visible mode setup
  S2_calibration.tmean = -27.5;
  S2_calibration.tsigma = 2.9;
}


// === calibration data 2017 ===
// data is taken from pcdmrc01:/online/detector/calibrs/2017.xml/ECALT.xml on 2017-09-14:
// -rw-r--r-- 1 daq daq 2673 Sep 14 10:38 /online/detector/calibrs/2017.xml/ECALT.xml
// Energy calibration calculated by Alexander Toropin, based on runs 3119-3159 (second stage of calibration runs)
void Init_Calib_2017_ECAL()
{
  // for 100 GeV e-
  const float factor[2] = {
     5. * 0.84191 * 100./111.,
    95. * 0.84191 * 100./111.
  };
  
  //   {0,0 0,1 0,2 0,3 0,4 0,5
  //    1,0 1,1 1,2 1,3 1,4 1,5
  const float peakmean[2][6][6] = {
    {
      // Ecal plane 0: (Preshower)
      {1081, 1439, 1367, 1460, 1564, 1418},
      {1328, 1383, 1471, 1308, 1490, 1642},
      {1323, 1107, 1278, 1321, 1396, 1426},
      {1387, 1398, 1401, 1445, 1428, 1069},
      {1393, 1051, 1265, 1508, 1358, 1521},
      {1323, 1310, 1619, 1410,  724, 1253}
    },
    {
      // Ecal plane 1: (EC)
      {2345, 2331, 2335, 2156, 2260, 2432},
      {2401, 2347, 2151, 2035, 2343, 2235},
      {2350, 2356, 2356, 2324, 2325, 2271},
      {2040, 2185, 2287, 2267, 2407, 2190},
      {2417, 2616, 2177, 2292, 2341, 2183},
      {2680, 2028, 2297, 2279, 2144, 2219}
    }
  };
  
  for (int d = 0; d < 2; d++)
    for (int x = 0; x < 6; x++)
      for (int y = 0; y < 6; y++)
        ECAL_calibration[d][x][y].K = factor[d] / peakmean[d][x][y];


  // tmean define expected channel timing vs. master time
  //   texpected = master + tmean
  //
  // the value computed as
  //   tmean = <tchannel - master>
  //     tchannel - channel "rising edge" time: Cell::t0_RisingEdge()
  //     master - T2 counter "rising edge" time: T2.t0_RisingEdge()
  //     <> - average over statistics
  //
  //  Time constants from the Donskov code pcdmfs01 /online/detector/calo/H4/INVIS/time_ph_part.cc
  //     
  float tmean[2][6][6] = {
    {
      // Ecal plane 0: (PS)
      {-75.3, -45.6, -51.7, -48.3, -47.9, -72.2},
      {-48.8, -50.9, -44.4, -45.8, -45.8, -47.5},
      {-44.8, -49.6, -49.6, -42.8, -44.0, -44.1},
      {-44.5, -44.6, -54.5, -44.7, -45.1, -57.6},
      {-45.2, -57.2, -43.0, -49.3, -44.8, -48.8},
      {-67.3, -44.3, -56.6, -51.0, -52.8, -70.0}
    },
    {
      // Ecal plane 1: (EC)
      {-65.6, -32.2, -31.9, -38.4, -34.0, -70.6},
      {-34.7, -65.0, -66.0, -69.1, -68.5, -24.8},
      {-33.6, -63.2, -67.2, -67.3, -61.5, -44.3},
      {-41.5, -59.9, -59.4, -58.5, -64.2, -42.7},
      {-43.0, -62.0, -57.6, -60.4, -69.7, -36.8},
      {-68.5, -47.2, -47.1, -50.0, -48.1, -71.0}
    }
  };

  float tsigma[2][6][6] = {
    {
      // Ecal plane 0: (PS)
      {6.6, 9.0, 8.4, 6.9, 6.6, 7.8},
      {9.6, 7.5, 8.4, 11.1, 9.0, 10.5},
      {9.3, 10.8, 9.0, 6.9, 7.5, 7.2},
      {7.5, 9.3, 9.3, 6.6, 6.3, 12.6},
      {10.5, 9.3, 8.4, 8.4, 6.3, 6.6},
      {15.0, 7.2, 13.5, 15.6, 9.0, 10.5}
    },
    {
      // Ecal plane 1: (EC)
      {10.8, 5.1, 7.2, 9.3, 9.3, 10.8},
      {5.1, 7.8, 5.7, 6.3, 7.2, 4.8},
      {6.6, 5.1, 6.0, 5.4, 4.8, 6.0},
      {5.4, 6.6, 5.1, 5.4, 7.5, 5.4},
      {5.4, 6.0, 4.2, 6.0, 6.6, 5.1},
      {12.0, 5.7, 6.0, 5.7, 4.5, 12.0}
    }
  };

  for (int ipl = 0; ipl < 2; ipl ++)
    for (int j = 0; j < 6; j++)
      for (int i = 0; i < 6; i++) {
        ECAL_calibration[ipl][i][j].tmean = tmean[ipl][i][j];
        ECAL_calibration[ipl][i][j].tsigma = tsigma[ipl][i][j];
      }

}

// Calibration prepared by Vladimir Samoylenko
// Details:
//   ELOG / Message ID: 122 / Entry time: Sun Sep 10 21:09:40 2017
//   http://pcdmfs01.cern.ch:8080/Main/122
void Init_Calib_2017_VETO()
{
  // mean energy deposition in one cell (MC by M.Kirsanov)
  const float factor = 9.8*MeV;
  // Landau fit MPV parameter of amplitude distribution
  const float MPV[6] = {590, 501, 590, 637, 610, 566};
  
  for (int i = 0; i < 6; i++)
    VETO_calibration[i].K = factor / MPV[i];
}

// data is taken from pcdmrc01 on 2017-09-19:
// -rw-rw-r-- 1 daq daq 1941 Sep 15 11:05 /online/detector/calibrs/2017.xml/HCAL.xml
// which was extracted from paper logbook by Alexander Toropin
// calibration runs:
//   HCAL0   2902 - 2910
//   HCAL1   ???
//   HCAL2   2850 - 2859
//   HCAL3   2843 - 2849
void Init_Calib_2017_HCAL()
{
  // beam pi- 100 GeV
  const float factor = 100. * 0.655;
  
  // HCAL0 [0] (0,0), (0,1), (0,2), (1,0), (1,1), (1,2), (2,0), 2,1), (2,2)
  // HCAL1 [1]  ...........................................................
  const float amplitude[4][3][3] = {
    {{2529, 2519, 2531}, {2484, 2524, 2497}, {2506, 2468, 2497}},
    {{2453, 2467, 2591}, {2520, 2536, 2450}, {2499, 2558, 2566}},
    {{2423, 2433, 2480}, {2568, 2521, 2559}, {2459, 2570, 2546}},
    {{2528, 2425, 2445}, {2501, 2423, 2470}, {2400, 2550, 2380}}
  };
  
  for (int d = 0; d < 4; d++)
    for (int x = 0; x < 3; x++)
      for (int y = 0; y < 3; y++)
        HCAL_calibration[d][x][y].K = factor / amplitude[d][x][y];

  //HCAL corrections to put the dimuon peak to the place predicted by MC, mainly run 3387
  const float mkfact[4] = {1.27, 1.48, 1.21, 1.};
  for (int d = 0; d < 4; d++)
    for (int x = 0; x < 3; x++)
      for (int y = 0; y < 3; y++)
        HCAL_calibration[d][x][y].K *= mkfact[d];
  
  // see Init_Calib_2017_ECAL() for tmean definition
  // Time constants from the Donskov code pcdmfs01 /online/detector/calo/H4/INVIS/time_ph_part.cc
  // tsigma values are not copied in details from the code above, values assigned according to the
  // general tendency (they are big anyway)
  // 
  float tmean[4][3][3] = {
    {{-60.6, -68.7, -58.4}, {-58.1, -61.5, -59.8}, {-65.1, -56.5, -59.5}},
    {{-62.9, -66.0, -70.7}, {-67.1, -56.6, -58.7}, {-62.3, -68.0, -72.9}},
    {{-68.5, -54.3, -57.8}, {-72.5, -61.2, -58.4}, {-61.4, -57.4, -59.2}},
    {{-65.7, -64.0, -66.8}, {-63.7, -64.3, -68.6}, {-67.2, -64.5, -57.7}}
  };

  for (int ipl = 0; ipl < 4; ipl++) {
    for (int j = 0; j < 3; j++) {
      for (int i = 0; i < 3; i++) {
        HCAL_calibration[ipl][i][j].tmean = tmean[ipl][i][j];
        HCAL_calibration[ipl][i][j].tsigma = 8.;
        if(ipl == 1) HCAL_calibration[ipl][i][j].tsigma = 14.;
      }
    }
  }

}

// data is extracted from pcdmfs01 on 2017-10-03:
// -rw-r--r-- 1 daq daq 397 Sep 24 22:22 /online/detector/calibrs/2017.xml/WCAL.xml
// constants calculated by S.Donskov and V.Poliakov based on
//   - some electrons calibration run for WCAL[0] and WCAL[1]
//   - some muons calibration run for Wcatcher
//   - energy deposition simulations by M.Kirsanov
void Init_Calib_2017_WCAL(const int run)
{
  // values are in "MeV / ADC count"
  WCAL_calibration[0].K =  3.1*MeV;
  WCAL_calibration[1].K = 58.0*MeV;
  WCAT_calibration.K =  5.0*MeV;
  
  // Wcatcher analog signal was split to be included in trigger
  // which decreased amplitude by 2 times
  if (run >= 3504) WCAT_calibration.K *= 2.0;

  if (run <= 3462) WCAL_calibration[1].K *= (100./96.1); // This and below: correct WCAL calibration using runs 3400, 3466, 3521
  if (run >= 3463) WCAL_calibration[1].K *= (100./106.); // correction (approximate) for 30X geometry. Partly explaned by HV increase.

  // see Init_Calib_2017_ECAL() for tmean definition
  // taken from Donskov code pcdmfs01 /online/detector/calo/H4/INVIS/time_ph_part.cc
  //
  WCAL_calibration[0].tmean = -44.8;
  WCAL_calibration[1].tmean = -36.6;
  WCAT_calibration.tmean = -38.8;

  WCAL_calibration[0].tsigma = 4.2;
  WCAL_calibration[1].tsigma = 3.6;
  WCAT_calibration.tsigma = 5.4;
}

// calibrations prepared by Vladimir Poliakov and extracted from
//   https://twiki.cern.ch/twiki/pub/P348/RunsInfo2017/list_runs_Sep_2017_v3.pdf
//   (7 September record / SRD MIP amplitude in ADC counts)
void Init_Calib_2017_SRD()
{
  // 42 MeV is mean energy deposition in active media of SRD (based on simulations by M.Kirsanov)
  SRD_calibration[0].K = 0.042 / 1050.;
  SRD_calibration[1].K = 0.042 / 1070.;
  SRD_calibration[2].K = 0.042 / 1040.;

  // see Init_Calib_2017_ECAL() for tmean definition
  // taken from pcdmfs01 /online/detector/calo/H4/INVIS/time_ph_part.cc
  //
  SRD_calibration[0].tmean = -78.3;
  SRD_calibration[1].tmean = -78.9;
  SRD_calibration[2].tmean = -77.2;

  SRD_calibration[0].tsigma = 0.6 * 8.4;
  SRD_calibration[1].tsigma = 0.6 * 7.8;
  SRD_calibration[2].tsigma = 0.6 * 7.8;
}

void Init_Calib_2017_MasterTime(const int run)
{
  // T2 timing calibration data extracted from T2 rising edge histograms of runs 3191-3573
  // NOTE: T2 is available only starting from run 3191
  T2_calibration.isTmeanRelative = false;
  T2_calibration.tmean = 210.;
  T2_calibration.tsigma = 6.6;
  
  // S2 timing extracted from runs 3183-3190
  if (3183 <= run && run <= 3190) {
    S2_calibration.isTmeanRelative = false;
    S2_calibration.tmean = 239.;
    S2_calibration.tsigma = 6.0;
  }
  
  if (3163 <= run && run <= 3182) {
    S2_calibration.isTmeanRelative = false;
    S2_calibration.tmean = 200.;
    S2_calibration.tsigma = 6.0;
  }
  
  if (3119 <= run && run <= 3162) {
    S2_calibration.isTmeanRelative = false;
    S2_calibration.tmean = 214.;
    S2_calibration.tsigma = 6.5;
  }
}

void Init_Calib_2017_Counters()
{
  // Calibrated using dimuons from 3387 - 3389
  // Time calibration from dimuons and hadron calib run 3378
  //
  V2_calibration.K = 0.0075 / 1250.;
  S4_calibration.K = 0.00023 / 130.;
  //
  V2_calibration.tmean = -51.8;
  S4_calibration.tmean = -37.;
  //
  V2_calibration.tsigma = 4.;
  S4_calibration.tsigma = 4.;
}


// === calibration data 2018 ===
// data source (at pcdmfs01):
// -rw-r--r-- 1 daq daq 2713 May 20 12:38 /online/detector/calo/H4/calibrs/2018.xml/ECALT.xml
void Init_Calib_2018_ECAL(const int run)
{
  // factors extracted from Donskov's constants file
  // for 100 GeV e-
  float factor[2] = {
    0.05 * 100.,
    0.78 * 100.
  };
  
  // corrected using runs 3897, 3918, 3947, 4085, 4124, 4185 // LIV
  const float corr_prs = 1.06;
  const float corr_ecal = 0.932;
  
  if (run >= 3897 && run < 4098) {
    factor[0] *= corr_prs;
    factor[1] *= corr_ecal;
  }
  
  // only Ecal[0] correction is necessary, Ecal[1] corrected by HV adjastment
  if (run >= 4098) factor[0] *= corr_prs;
  
  
  //   {0,0 0,1 0,2 0,3 0,4 0,5
  //    1,0 1,1 1,2 1,3 1,4 1,5
  const float peakmean[2][6][6] = {
    {
      // Ecal plane 0: (Preshower)
      {1545, 1326, 1504, 1350, 1360, 1333},
      {1276, 1487, 1468,  955, 1347, 1572},
      {1451,  968, 1400, 1392, 1260, 1538},
      {1476, 1294, 1247, 1358, 1330, 1703},
      {1398, 1198, 1349, 1716, 1429, 1295},
      {1364, 1419, 1347, 1246, 1429, 1472}
    },
    {
      // Ecal plane 1: (EC)
      {2373, 2169, 2410, 2405, 2163, 2388},
      {2363, 2431, 2066, 2307, 2488, 2700},
      {2739, 2199, 2454, 2193, 2832, 2165},
      {2941, 2879, 2436, 2109, 2303, 1807},
      {2374, 2498, 2307, 2195, 2482, 2195},
      {1981, 2081, 2317, 2330, 2834, 2325}
    }
  };
  
  // tmean define expected channel timing vs. master time
  //   texpected = master + tmean
  //
  // the value computed as
  //   tmean = <tchannel - master>
  //     tchannel - channel "rising edge" time: Cell::t0_RisingEdge()
  //     master - T2 counter "rising edge" time: T2.t0_RisingEdge()
  //     <> - average over statistics
  
  // tmean is the same as in the 2017 except of
  // channel ECAL1-3-3:  -58.5  -->  -63.5
  const float tmean[2][6][6] = {
    {
      // Ecal plane 0: (PS)
      {-75.3, -45.6, -51.7, -48.3, -47.9, -72.2},
      {-48.8, -50.9, -44.4, -45.8, -45.8, -47.5},
      {-44.8, -49.6, -49.6, -42.8, -44.0, -44.1},
      {-44.5, -44.6, -54.5, -44.7, -45.1, -57.6},
      {-45.2, -57.2, -43.0, -49.3, -44.8, -48.8},
      {-67.3, -44.3, -56.6, -51.0, -52.8, -70.0}
    },
    {
      // Ecal plane 1: (EC)
      {-65.6, -32.0, -31.9, -38.4, -34.0, -70.6},
      {-34.7, -65.0, -66.0, -69.1, -68.5, -24.8},
      {-33.6, -63.2, -67.2, -67.3, -61.5, -44.3},
      {-41.5, -59.9, -59.4, -63.5, -64.2, -42.7},
      {-43.0, -62.0, -57.6, -60.4, -69.7, -36.8},
      {-68.5, -47.2, -47.1, -50.0, -48.1, -71.0}
    }
  };
  
  // tsigma is set 3 times higher than in original data source
  // (the same as in 2017 calibration data)
  const float tsigma[2][6][6] = {
    {
      // Ecal plane 0: (PS)
      {6.6, 9.0, 8.4, 6.9, 6.6, 7.8},
      {9.6, 7.5, 8.4, 11.1, 9.0, 10.5},
      {9.3, 10.8, 9.0, 6.9, 7.5, 7.2},
      {7.5, 9.3, 9.3, 6.6, 6.3, 12.6},
      {10.5, 9.3, 8.4, 8.4, 6.3, 6.6},
      {15.0, 7.2, 13.5, 15.6, 9.0, 10.5}
    },
    {
      // Ecal plane 1: (EC)
      {10.8, 5.1, 7.2, 9.3, 9.3, 10.8},
      {5.1, 7.8, 5.7, 6.3, 7.2, 4.8},
      {6.6, 5.1, 6.0, 5.4, 4.8, 6.0},
      {5.4, 6.6, 5.1, 5.4, 7.5, 5.4},
      {5.4, 6.0, 4.2, 6.0, 6.6, 5.1},
      {12.0, 5.7, 6.0, 5.7, 4.5, 12.0}
    }
  };
  
  for (int d = 0; d < 2; d++)
    for (int x = 0; x < 6; x++)
      for (int y = 0; y < 6; y++) {
        CaloCalibData& c = ECAL_calibration[d][x][y];
        c.K = factor[d] / peakmean[d][x][y];
        c.tmean = tmean[d][x][y];
        c.tsigma = tsigma[d][x][y];
      }
}

// data source (at pcdmfs01):
// -rw-r--r-- 1 daq daq 1997 May 19 18:24 /online/detector/calo/H4/calibrs/2018.xml/HCAL.xml
void Init_Calib_2018_HCAL(const int run)
{
  // factor as defined in Donskov's codes
  // beam hadrons 100 GeV
  const float factor = 1. * 100.;
  
  // HCAL0 [0] (0,0), (0,1), (0,2), (1,0), (1,1), (1,2), (2,0), 2,1), (2,2)
  // HCAL1 [1]  ...........................................................
  const float peakmean[4][3][3] = {
    {{2102, 2281, 2302}, {2318, 2344, 2303}, {2407, 2114, 2069}},
    {{2363, 2408, 2283}, {2352, 2374, 2411}, {2486, 2342, 2262}},
    {{1880, 1994, 1551}, {2252, 2000, 1863}, {2273, 2203, 2184}},
    {{2245, 1917, 1829}, {2208, 2850, 2454}, {1920, 1850, 2679}}
  };
  
  // tmean definition: see Init_Calib_2018_ECAL()
  // the data is the same as in 2017
  const float tmean[4][3][3] = {
    {{-60.6, -68.7, -58.4}, {-58.1, -61.5, -59.8}, {-65.1, -56.5, -59.5}},
    {{-62.9, -66.0, -70.7}, {-67.1, -56.6, -58.7}, {-62.3, -68.0, -72.9}},
    {{-68.5, -54.3, -57.8}, {-72.5, -61.2, -58.4}, {-61.4, -57.4, -59.2}},
    {{-65.7, -64.0, -66.8}, {-63.7, -64.3, -68.6}, {-67.2, -64.5, -57.7}}
  };
  
  // tsigma the same as 2017
  // (see Init_Calib_2017_HCAL() for details on values)
  const float tsigma[4] = {8., 14., 8., 8.};

  //HCAL corrections to put the dimuon peak to the place predicted by MC, mainly run 3898 3899 3900
  float mkfact[4] = {0.84, 0.81, 0.76, 1.};

  //HCAL corrections to put the dimuon peak to the place predicted by MC, mainly runs 4224, 4244, 4245, 4298 visible mode

  const float corr_hcal0_vism = 1.12;
  const float corr_hcal1_vism = 1.41;

  if (run >= 4224) {
        mkfact[0] *= corr_hcal0_vism;
        mkfact[1] *= corr_hcal1_vism;
        }  
  
  for (int d = 0; d < 4; d++)
    for (int x = 0; x < 3; x++)
      for (int y = 0; y < 3; y++) {
        CaloCalibData& c = HCAL_calibration[d][x][y];
        c.K = mkfact[d] * factor / peakmean[d][x][y];
        c.tmean = tmean[d][x][y];
        c.tsigma = tsigma[d];
      }
}

// data source:
// * energy (page1 / 18May record):
//   https://twiki.cern.ch/twiki/pub/P348/RunsInfo2018/list_runs_May_2018_v1.pdf
//
// * timing (at pcdmfs01):
//   -rw-rw-r-- 1 daq daq 514 May 20 17:39 /online/detector/calo/H4/calibrs/2018.xml/SRDT.xml
void Init_Calib_2018_SRD()
{
  // 42 MeV is mean energy deposition in active media of SRD
  // (based on simulations by M.Kirsanov)
  SRD_calibration[0].K = 0.042 / 1515.;
  SRD_calibration[1].K = 0.042 / 1260.;
  SRD_calibration[2].K = 0.042 / 1050.;
  
  SRD_calibration[0].tmean = -68.0;
  SRD_calibration[1].tmean = -68.0;
  SRD_calibration[2].tmean = -68.0;
  
  SRD_calibration[0].tsigma = 2.0;
  SRD_calibration[1].tsigma = 2.0;
  SRD_calibration[2].tsigma = 2.0;
}

// data source (at pcdmfs01):
// -rw-r--r-- 1 daq daq 982 May 28 20:17 /online/detector/calo/H4/calibrs/2018.xml/VETO.xml
void Init_Calib_2018_VETO(const int run)
{
  // mean energy deposition in one cell (MC by M.Kirsanov)
  const float factor = 9.8*MeV;
  
  // Landau fit MPV parameter of amplitude distribution
  // initial calibration
  const float mpvinit[6] = {790, 772, 820, 820, 790, 800};
  // new calibration done in runs 4013, 4014
  const float mpv4013[6] = {790, 772, 820, 400, 600, 800};
  // veto3 & veto5 counters swapped and HV on veto5 encreased  before run 4061
  const float mpv4061[6] = {790, 772, 820, 800, 600, 750};
  
  const float tmean[6] = {-77.4, -78.8, -75.5, -76.7, -78.4, -78.9};
  
  const float tsigmainit[6] = {3.4, 3.3, 3.2, 3.1, 3.7, 3.7};
  const float tsigma4013[6] = {5.0, 5.0, 5.0, 5.0, 5.0, 5.0};
  
  const float* mpv = 0;
  if (run < 4013) mpv = mpvinit;
  else if (run < 4061) mpv = mpv4013;
  else mpv = mpv4061;
  
  const float* tsigma = 0;
  if (run < 4013) tsigma = tsigmainit;
  else tsigma = tsigma4013;
  
  for (int i = 0; i < 6; i++) {
    CaloCalibData& c = VETO_calibration[i];
    c.K = factor / mpv[i];
    c.tmean = tmean[i];
    c.tsigma = tsigma[i];
  }
}

// data source (at pcdmfs01):
// -rw-r--r-- 1 daq daq  582 Jun 14 22:36 /online/detector/calo/H4/calibrs/2018.xml/WCAL.xml
// -rw-r--r-- 1 daq daq 1422 Jun 17 13:09 /online/detector/calo/H4/calibrs/2018.xml/BEAMCOUNTERS.xml
void Init_Calib_2018_WCAL(const int run)
{
  // values are in "MeV / ADC count"

  WCAL_calibration[0].K = 4.0*MeV;
  WCAL_calibration[1].K = 68.3*MeV;
  WCAL_calibration[2].K = .017*MeV;

  // corrected using runs visible mode 4238, 4263, 4288  // LIV

  const float corr_wcal0 = 1.42;
  const float corr_wcal1 = 1.08;
  // corrected using runs 4239  // LIV
  const float corr_wcal2 = 3.6;

  if (run >= 4224) {
  WCAL_calibration[0].K *= corr_wcal0;
  WCAL_calibration[1].K *= corr_wcal1;
  WCAL_calibration[2].K *= corr_wcal2;
  }
  
  // TODO: check calibration
  WCAT_calibration.K =  5.0*MeV;
  
  WCAL_calibration[0].tmean = -51.;
  WCAL_calibration[1].tmean = -49.;
  WCAL_calibration[2].tmean = -35.;
  WCAT_calibration.tmean = -65.0;
  
  // visible mode
  if (4215 <= run)
    WCAT_calibration.tmean = -59.0;

  WCAL_calibration[0].tsigma = 3.;
  WCAL_calibration[1].tsigma = 3.;
  WCAL_calibration[2].tsigma = 5.;
  WCAT_calibration.tsigma = 5.0;
}

void Init_Calib_2018_MasterTime()
{
  // T2 timing calibration data extracted from T2 rising edge histograms of run 4038
  T2_calibration.isTmeanRelative = false;
  T2_calibration.tmean = 207.;
  T2_calibration.tsigma = 5.6;
}

// data source (at pcdmfs01):
// -rw-r--r-- 1 daq daq 1422 Jun 17 13:09 /online/detector/calo/H4/calibrs/2018.xml/BEAMCOUNTERS.xml
void Init_Calib_2018_Counters(const int run)
{
  if (run <= 4214) {
    V2_calibration.K = 0.0075 / 610.;
    V2_calibration.tmean = -37.0;
    V2_calibration.tsigma = 7.;
  }
  
  if (4215 <= run) {
    V2_calibration.K = 0.0075 / 700.;
    V2_calibration.tmean = -35.25;
    V2_calibration.tsigma = 7.;
  }

  // corrected using runs visible mode 4239, 4264-4267  // LIV

  const float corr_s4 = 1.177;

  if (4224 <= run) {
  S4_calibration.K = corr_s4 * 0.00023 / 130.;
  S4_calibration.tmean = -37.;
  S4_calibration.tsigma = 4.;
  }
}


// === MM mapping ===
void print_MM_map(map<int, vector<int> > map)
{
    for (auto it = map.begin(); it != map.end(); ++it) {
      cout << "ch#" << it->first << " strips = ";
      for (size_t j = 0; j < it->second.size(); ++j) {
        cout << it->second[j] << " ";
      }
      cout << endl;
    }
}

void Init_MM_mapping()
{
  MM_map.clear();
  MM_map[ 0] = {  0,  64, 128, 192, 256 };
  MM_map[ 1] = {  1, 119, 133, 219, 297 };
  MM_map[ 2] = {  2, 110, 138, 246, 274 };
  MM_map[ 3] = {  3, 101, 143, 209, 315 };
  MM_map[ 4] = {  4,  92, 148, 236, 292 };
  MM_map[ 5] = {  5,  83, 153, 199, 269 };
  MM_map[ 6] = {  6,  74, 158, 226, 310 };
  MM_map[ 7] = {  7,  65, 163, 253, 287 };
  MM_map[ 8] = {  8, 120, 168, 216, 264 };
  MM_map[ 9] = {  9, 111, 173, 243, 305 };
  MM_map[10] = { 10, 102, 178, 206, 282 };
  MM_map[11] = { 11,  93, 183, 233, 259 };
  MM_map[12] = { 12,  84, 188, 196, 300 };
  MM_map[13] = { 13,  75, 129, 223, 277 };
  MM_map[14] = { 14,  66, 134, 250, 318 };
  MM_map[15] = { 15, 121, 139, 213, 295 };
  MM_map[16] = { 16, 112, 144, 240, 272 };
  MM_map[17] = { 17, 103, 149, 203, 313 };
  MM_map[18] = { 18,  94, 154, 230, 290 };
  MM_map[19] = { 19,  85, 159, 193, 267 };
  MM_map[20] = { 20,  76, 164, 220, 308 };
  MM_map[21] = { 21,  67, 169, 247, 285 };
  MM_map[22] = { 22, 122, 174, 210, 262 };
  MM_map[23] = { 23, 113, 179, 237, 303 };
  MM_map[24] = { 24, 104, 184, 200, 280 };
  MM_map[25] = { 25,  95, 189, 227, 257 };
  MM_map[26] = { 26,  86, 130, 254, 298 };
  MM_map[27] = { 27,  77, 135, 217, 275 };
  MM_map[28] = { 28,  68, 140, 244, 316 };
  MM_map[29] = { 29, 123, 145, 207, 293 };
  MM_map[30] = { 30, 114, 150, 234, 270 };
  MM_map[31] = { 31, 105, 155, 197, 311 };
  MM_map[32] = { 32,  96, 160, 224, 288 };
  MM_map[33] = { 33,  87, 165, 251, 265 };
  MM_map[34] = { 34,  78, 170, 214, 306 };
  MM_map[35] = { 35,  69, 175, 241, 283 };
  MM_map[36] = { 36, 124, 180, 204, 260 };
  MM_map[37] = { 37, 115, 185, 231, 301 };
  MM_map[38] = { 38, 106, 190, 194, 278 };
  MM_map[39] = { 39,  97, 131, 221, 319 };
  MM_map[40] = { 40,  88, 136, 248, 296 };
  MM_map[41] = { 41,  79, 141, 211, 273 };
  MM_map[42] = { 42,  70, 146, 238, 314 };
  MM_map[43] = { 43, 125, 151, 201, 291 };
  MM_map[44] = { 44, 116, 156, 228, 268 };
  MM_map[45] = { 45, 107, 161, 255, 309 };
  MM_map[46] = { 46,  98, 166, 218, 286 };
  MM_map[47] = { 47,  89, 171, 245, 263 };
  MM_map[48] = { 48,  80, 176, 208, 304 };
  MM_map[49] = { 49,  71, 181, 235, 281 };
  MM_map[50] = { 50, 126, 186, 198, 258 };
  MM_map[51] = { 51, 117, 191, 225, 299 };
  MM_map[52] = { 52, 108, 132, 252, 276 };
  MM_map[53] = { 53,  99, 137, 215, 317 };
  MM_map[54] = { 54,  90, 142, 242, 294 };
  MM_map[55] = { 55,  81, 147, 205, 271 };
  MM_map[56] = { 56,  72, 152, 232, 312 };
  MM_map[57] = { 57, 127, 157, 195, 289 };
  MM_map[58] = { 58, 118, 162, 222, 266 };
  MM_map[59] = { 59, 109, 167, 249, 307 };
  MM_map[60] = { 60, 100, 172, 212, 284 };
  MM_map[61] = { 61,  91, 177, 239, 261 };
  MM_map[62] = { 62,  82, 182, 202, 302 };
  MM_map[63] = { 63,  73, 187, 229, 279 };
  
  //cout << "MM electronics channels to strips map:" << endl;
  //print_MM_map(MM_map);
  
  MM_reverse_map.clear();
  
  for (auto i = MM_map.begin(); i != MM_map.end(); ++i) {
    const int channel = i->first;
    const vector<int>& strips = i->second;
    
    for (size_t j = 0; j < strips.size(); ++j) {
      const int strip = strips[j];
      MM_reverse_map[strip] = channel;
    }
  }
  
  //cout << "MM strips to electronics channels map:" << endl;
  //for (auto it = MM_reverse_map.begin(); it != MM_reverse_map.end(); ++it) {
  //  cout << "strip = " << it->first << " channel = " << it->second << endl;
  //}
}

// calibrations by Dipanwita Banerjee
void Init_Calib_2016B_MM()
{
  //read pedestals sigmas
  const string MMPedestalDirectory = "pedestal2016/";
  MM1_calib.ReadSigmaCalib(MMPedestalDirectory+"MM01_2016.ped");
  MM2_calib.ReadSigmaCalib(MMPedestalDirectory+"MM02_2016.ped");
  MM3_calib.ReadSigmaCalib(MMPedestalDirectory+"MM03_2016.ped");
  MM4_calib.ReadSigmaCalib(MMPedestalDirectory+"MM04_2016.ped");
  
 //timing calibration
 //MM1
 //parameters
 MM1_calib.xcalib.timing.a02.parameters= {1.55,143.4,-47.58};
 MM1_calib.xcalib.timing.a12.parameters= {1.313,82.29,-70.8};
 MM1_calib.ycalib.timing.a02.parameters= {1.129,124.1,-37.91};
 MM1_calib.ycalib.timing.a12.parameters= {1.216,79.28,-68.22};
 //errors
 MM1_calib.xcalib.timing.a02.errors= {0.0048,25.703,5.57,0.35,-0.15,-10.9};
 MM1_calib.xcalib.timing.a12.errors= {0.0026,33.424,29.744,0.285,-0.262,-28.143}; 
 MM1_calib.ycalib.timing.a02.errors= {0.0017,14.7,5.64,0.152,-0.085,-7.42};
 MM1_calib.ycalib.timing.a12.errors= {0.0011,15.968,16.158,0.131,-0.129,-14.086};
  //MM2
 //parameters
 MM2_calib.xcalib.timing.a02.parameters= {1.517,137.2,-45.47};
 MM2_calib.xcalib.timing.a12.parameters= {1.273,74.82,-62.92};
 MM2_calib.ycalib.timing.a02.parameters= {1.138,116.3,-32.67};
 MM2_calib.ycalib.timing.a12.parameters= {1.174,70.64,-54.13};
 //errors
 MM2_calib.xcalib.timing.a02.errors= {0.0043,23.312,5.996,0.311,-0.145,-10.556};
 MM2_calib.xcalib.timing.a12.errors= {0.0001,1.222,1.570,0.011,-0.012,-1.142};
 MM2_calib.ycalib.timing.a02.errors= {0.0018,13.761,6.658,0.142,-0.089,-6.917};
 MM2_calib.ycalib.timing.a12.errors= {0.0002,1.89,3.26,0.017,-0.023,-1.752};
 //MM3
 //parameters
 MM3_calib.xcalib.timing.a02.parameters= {1.472,132,-42.94};
 MM3_calib.xcalib.timing.a12.parameters= {1.296,79.43,-58.72};
 MM3_calib.ycalib.timing.a02.parameters= {1.11,113.4,-31.17};
 MM3_calib.ycalib.timing.a12.parameters= {1.131,69.48,-47.17};
 //errors
 MM3_calib.xcalib.timing.a02.errors= {0.021,105.43,17.065,1.489,-0.575,-40.422};
 MM3_calib.xcalib.timing.a12.errors= {0.001,9.936,8.275,0.098,-0.087,-8.416};
 MM3_calib.ycalib.timing.a02.errors= {0.0024,17.353,5.57,0.195,-0.104,-8.799};
 MM3_calib.ycalib.timing.a12.errors= {2.55E-5,0.214,0.352,0.0022,-0.0028,-0.220};
 //MM4
 //parameters
 MM4_calib.xcalib.timing.a02.parameters= {1.419,130.1,-40.04};
 MM4_calib.xcalib.timing.a12.parameters= {1.195,71.45,-51.83};
 MM4_calib.ycalib.timing.a02.parameters= {1.077,118.9,-31.86};
 MM4_calib.ycalib.timing.a12.parameters= {1.191,76.86,-61.35};
 //errors
 MM4_calib.xcalib.timing.a02.errors= {0.0017,9.67,3.91,0.124,-0.075,-5.56};
 MM4_calib.xcalib.timing.a12.errors= {0.0008,4.37,19.96,0.041,-0.115,-4.29};
 MM4_calib.ycalib.timing.a02.errors= {3.7E-5,0.313,0.212,0.003,-0.002,-0.203};
 MM4_calib.ycalib.timing.a12.errors= {0.005,42.52,114.3,0.435,-0.739,-56.69};

 //put names
} //end Init_Calib_2016B_MM

void Init_Calib_2017_MM()
{
  //READ pedestals from file
  const string MMPedestalDirectory = "pedestal2017/";
  MM1_calib.ReadSigmaCalib(MMPedestalDirectory+"MM01.ped");
  MM2_calib.ReadSigmaCalib(MMPedestalDirectory+"MM02.ped");
  MM3_calib.ReadSigmaCalib(MMPedestalDirectory+"MM03.ped");
  MM4_calib.ReadSigmaCalib(MMPedestalDirectory+"MM04.ped");
  MM5_calib.ReadSigmaCalib(MMPedestalDirectory+"MM05.ped");
  MM7_calib.ReadSigmaCalib(MMPedestalDirectory+"MM07.ped");
  
  //UPSTREAM CONSTANT
MM1_calib.xcalib.timing.a02.parameters= {1.27062,142.427,-33.2835};
MM1_calib.xcalib.timing.a02.errors= {1.86258,6991.31,5598.28,110.198,-95.9439,-5458.81};
MM1_calib.xcalib.timing.a12.parameters= {1.15026,105.008,-35.5102};
MM1_calib.xcalib.timing.a12.errors= {0.336042,2063.89,8577.5,-5.8154,-50.4027,2104.8};
MM2_calib.xcalib.timing.a02.parameters= {1.24329,143.317,-37.5259};
MM2_calib.xcalib.timing.a02.errors= {3.49489,16168.5,10637,233.754,-183.549,-12032.7};
MM2_calib.xcalib.timing.a12.parameters= {1.16999,104.549,-42.4817};
MM2_calib.xcalib.timing.a12.errors= {0.868475,2455.26,18936.7,11.1406,-123.101,44.7095};
MM3_calib.xcalib.timing.a02.parameters= {1.02052,130.81,-36.366};
MM3_calib.xcalib.timing.a02.errors= {2.69802,14731.7,20320.8,190.86,-221.41,-14822.3};
MM3_calib.xcalib.timing.a12.parameters= {1.13799,89.3839,-55.8788};
MM3_calib.xcalib.timing.a12.errors= {4.18576,10354.8,144836,58.8096,-761.729,-3589.2};
MM4_calib.xcalib.timing.a02.parameters= {1.2061,144.51,-37.9135};
MM4_calib.xcalib.timing.a02.errors= {4.17452,20989.6,12713.7,291.881,-219.539,-15117.9};
MM4_calib.xcalib.timing.a12.parameters= {1.16135,104.76,-44.9047};
MM4_calib.xcalib.timing.a12.errors= {1.34475,3481.26,28213.8,31.9155,-187.919,-2443.59};
MM1_calib.ycalib.timing.a02.parameters= {1.11978,135.537,-28.1138};
MM1_calib.ycalib.timing.a02.errors= {1.24223,4416.39,5000.11,69.131,-70.4358,-3702.9};
MM1_calib.ycalib.timing.a12.parameters= {1.15096,102.104,-39.369};
MM1_calib.ycalib.timing.a12.errors= {1.00427,3026.15,22637.4,2.39151,-142.966,1971.33};
MM2_calib.ycalib.timing.a02.parameters= {1.18926,139.605,-29.9941};
MM2_calib.ycalib.timing.a02.errors= {1.68738,6112.13,5363.87,96.9505,-86.7835,-4804.24};
MM2_calib.ycalib.timing.a12.parameters= {1.21094,107.743,-41.9657};
MM2_calib.ycalib.timing.a12.errors= {1.36482,3141.26,21955.8,36.9759,-165.631,-2791.92};
MM3_calib.ycalib.timing.a02.parameters= {1.07237,139.053,-33.3187};
MM3_calib.ycalib.timing.a02.errors= {3.03495,15235.8,12587.2,208.919,-181.643,-12181.8};
MM3_calib.ycalib.timing.a12.parameters= {1.18184,105.753,-46.6008};
MM3_calib.ycalib.timing.a12.errors= {2.44053,6487.61,43302.6,88.3015,-313.361,-8593.57};
MM4_calib.ycalib.timing.a02.parameters= {1.18478,140.521,-33.1136};
MM4_calib.ycalib.timing.a02.errors= {2.3714,9854.93,7764.07,148.399,-126.264,-7687.45};
MM4_calib.ycalib.timing.a12.parameters= {1.18762,106.448,-43.1446};
MM4_calib.ycalib.timing.a12.errors= {1.31737,3192.02,23844.7,33.6313,-170.167,-2547.32};
//DOWNSTREAM CONSTANT
MM5_calib.xcalib.timing.a02.parameters= {1.20437,308.222,-30.4724};
MM5_calib.xcalib.timing.a02.errors= {0.643998,2081.09,3570.47,31.4746,-44.1057,-1830.09};
MM5_calib.xcalib.timing.a12.parameters= {1.15947,268.884,-43.3712};
MM5_calib.xcalib.timing.a12.errors= {0.56849,3869.76,20113.3,-19.0831,-102.753,5490.7};
MM7_calib.xcalib.timing.a02.parameters= {1.25153,317.657,-31.6756};
MM7_calib.xcalib.timing.a02.errors= {1.21648,4272.54,3905.57,68.5415,-64.2775,-3411.74};
MM7_calib.xcalib.timing.a12.parameters= {1.22831,279.732,-47.8268};
MM7_calib.xcalib.timing.a12.errors= {1.01886,2446.53,20355.6,25.4067,-139.8,-2184.15};
MM5_calib.ycalib.timing.a02.parameters= {1.21338,314.193,-28.5284};
MM5_calib.ycalib.timing.a02.errors= {1.77169,5583.04,5518.21,93.3147,-88.7577,-4409.83};
MM5_calib.ycalib.timing.a12.parameters= {1.16234,275.631,-39.0516};
MM5_calib.ycalib.timing.a12.errors= {1.00864,3437.57,23577,-3.96983,-145.636,3201.28};;
MM7_calib.ycalib.timing.a02.parameters= {1.07888,315.156,-24.2368};
MM7_calib.ycalib.timing.a02.errors= {0.892518,3093.94,2968.9,47.4262,-43.9682,-2178.98};
MM7_calib.ycalib.timing.a12.parameters= {1.04605,274.51,-32.2398};
MM7_calib.ycalib.timing.a12.errors= {0.341839,4458.32,12351.1,-17.6304,-59.4534,5470.99};

} //end Init_Calib_2017_MM

void Init_Calib_2018_MM()
{
  //READ pedestals from file
  const string MMPedestalDirectory = "../pedestal2018/";
  MM1_calib.ReadSigmaCalib(MMPedestalDirectory+"MM01.ped");
  MM2_calib.ReadSigmaCalib(MMPedestalDirectory+"MM02.ped");
  MM3_calib.ReadSigmaCalib(MMPedestalDirectory+"MM03.ped");
  MM4_calib.ReadSigmaCalib(MMPedestalDirectory+"MM04.ped");
  MM5_calib.ReadSigmaCalib(MMPedestalDirectory+"MM05.ped");
  MM7_calib.ReadSigmaCalib(MMPedestalDirectory+"MM06.ped");
  
  //UPSTREAM CONSTANT --- copy pasted from 2017 for now
MM1_calib.xcalib.timing.a02.parameters= {1.27062,142.427,-33.2835};
MM1_calib.xcalib.timing.a02.errors= {1.86258,6991.31,5598.28,110.198,-95.9439,-5458.81};
MM1_calib.xcalib.timing.a12.parameters= {1.15026,105.008,-35.5102};
MM1_calib.xcalib.timing.a12.errors= {0.336042,2063.89,8577.5,-5.8154,-50.4027,2104.8};
MM2_calib.xcalib.timing.a02.parameters= {1.24329,143.317,-37.5259};
MM2_calib.xcalib.timing.a02.errors= {3.49489,16168.5,10637,233.754,-183.549,-12032.7};
MM2_calib.xcalib.timing.a12.parameters= {1.16999,104.549,-42.4817};
MM2_calib.xcalib.timing.a12.errors= {0.868475,2455.26,18936.7,11.1406,-123.101,44.7095};
MM3_calib.xcalib.timing.a02.parameters= {1.02052,130.81,-36.366};
MM3_calib.xcalib.timing.a02.errors= {2.69802,14731.7,20320.8,190.86,-221.41,-14822.3};
MM3_calib.xcalib.timing.a12.parameters= {1.13799,89.3839,-55.8788};
MM3_calib.xcalib.timing.a12.errors= {4.18576,10354.8,144836,58.8096,-761.729,-3589.2};
MM4_calib.xcalib.timing.a02.parameters= {1.2061,144.51,-37.9135};
MM4_calib.xcalib.timing.a02.errors= {4.17452,20989.6,12713.7,291.881,-219.539,-15117.9};
MM4_calib.xcalib.timing.a12.parameters= {1.16135,104.76,-44.9047};
MM4_calib.xcalib.timing.a12.errors= {1.34475,3481.26,28213.8,31.9155,-187.919,-2443.59};
MM1_calib.ycalib.timing.a02.parameters= {1.11978,135.537,-28.1138};
MM1_calib.ycalib.timing.a02.errors= {1.24223,4416.39,5000.11,69.131,-70.4358,-3702.9};
MM1_calib.ycalib.timing.a12.parameters= {1.15096,102.104,-39.369};
MM1_calib.ycalib.timing.a12.errors= {1.00427,3026.15,22637.4,2.39151,-142.966,1971.33};
MM2_calib.ycalib.timing.a02.parameters= {1.18926,139.605,-29.9941};
MM2_calib.ycalib.timing.a02.errors= {1.68738,6112.13,5363.87,96.9505,-86.7835,-4804.24};
MM2_calib.ycalib.timing.a12.parameters= {1.21094,107.743,-41.9657};
MM2_calib.ycalib.timing.a12.errors= {1.36482,3141.26,21955.8,36.9759,-165.631,-2791.92};
MM3_calib.ycalib.timing.a02.parameters= {1.07237,139.053,-33.3187};
MM3_calib.ycalib.timing.a02.errors= {3.03495,15235.8,12587.2,208.919,-181.643,-12181.8};
MM3_calib.ycalib.timing.a12.parameters= {1.18184,105.753,-46.6008};
MM3_calib.ycalib.timing.a12.errors= {2.44053,6487.61,43302.6,88.3015,-313.361,-8593.57};
MM4_calib.ycalib.timing.a02.parameters= {1.18478,140.521,-33.1136};
MM4_calib.ycalib.timing.a02.errors= {2.3714,9854.93,7764.07,148.399,-126.264,-7687.45};
MM4_calib.ycalib.timing.a12.parameters= {1.18762,106.448,-43.1446};
MM4_calib.ycalib.timing.a12.errors= {1.31737,3192.02,23844.7,33.6313,-170.167,-2547.32};
//DOWNSTREAM CONSTANT
MM5_calib.xcalib.timing.a02.parameters= {1.20437,308.222,-30.4724};
MM5_calib.xcalib.timing.a02.errors= {0.643998,2081.09,3570.47,31.4746,-44.1057,-1830.09};
MM5_calib.xcalib.timing.a12.parameters= {1.15947,268.884,-43.3712};
MM5_calib.xcalib.timing.a12.errors= {0.56849,3869.76,20113.3,-19.0831,-102.753,5490.7};
MM6_calib.xcalib.timing.a02.parameters= {1.25153,317.657,-31.6756};
MM6_calib.xcalib.timing.a02.errors= {1.21648,4272.54,3905.57,68.5415,-64.2775,-3411.74};
MM6_calib.xcalib.timing.a12.parameters= {1.22831,279.732,-47.8268};
MM6_calib.xcalib.timing.a12.errors= {1.01886,2446.53,20355.6,25.4067,-139.8,-2184.15};
MM5_calib.ycalib.timing.a02.parameters= {1.21338,314.193,-28.5284};
MM5_calib.ycalib.timing.a02.errors= {1.77169,5583.04,5518.21,93.3147,-88.7577,-4409.83};
MM5_calib.ycalib.timing.a12.parameters= {1.16234,275.631,-39.0516};
MM5_calib.ycalib.timing.a12.errors= {1.00864,3437.57,23577,-3.96983,-145.636,3201.28};;
MM6_calib.ycalib.timing.a02.parameters= {1.07888,315.156,-24.2368};
MM6_calib.ycalib.timing.a02.errors= {0.892518,3093.94,2968.9,47.4262,-43.9682,-2178.98};
MM6_calib.ycalib.timing.a12.parameters= {1.04605,274.51,-32.2398};
MM6_calib.ycalib.timing.a12.errors= {0.341839,4458.32,12351.1,-17.6304,-59.4534,5470.99};

} //end Init_Calib_2018_MM


void Init_Geometry_2016A_part1()
{
  // measured by Dipanwita Banerjee
  // Micromegas offsets actual for runs range: "start of 2016A" - #1426
  // offsets was measured just before run #1389
  ECAL_pos.pos = TVector3(-340,      0,       0);
  MM4_pos.pos  = TVector3(-300,    -10,    -640);
  MM3_pos.pos  = TVector3(-260,     -8,   -1840);
  MAGD_pos.pos = TVector3(   0,      0,  -19215+500+4000); // position of magnet downstream side (magnet length = 4 meters)
  MAGU_pos.pos = TVector3(   0,      0,  -19215+500);      // position of magnet upstream side
  MM2_pos.pos  = TVector3(-4.6,   13.1,  -19215);
  MM1_pos.pos  = TVector3(-8.32,  10.6,  -20015);
  
  MM1_pos.Angle = M_PI/4;
  MM2_pos.Angle = M_PI/4;
  MM3_pos.Angle = M_PI/4;
  MM4_pos.Angle = M_PI/4;
}

void Init_Geometry_2016A_part2()
{
  // measured by Dipanwita Banerjee
  // Micromegas offsets actual for runs range: #1427 - "end of 2016A"
  // offsets was measured just before run #1427
  MM1_pos.pos  = TVector3(  -5,  14,   -20015);
  MM2_pos.pos  = TVector3(   0,  16,   -19215);
  MAGU_pos.pos = TVector3(   0,   0,   -19215+500);      // position of magnet upstream side
  MAGD_pos.pos = TVector3(   0,   0,   -19215+500+4000); // position of magnet downstream side (magnet length = 4 meters)
  MM3_pos.pos  = TVector3(-300,  -8,    -1840);
  MM4_pos.pos  = TVector3(-320, -10,     -640);
  ECAL_pos.pos = TVector3(-350,   0,        0);
  
  MM1_pos.Angle = M_PI/4;
  MM2_pos.Angle = M_PI/4;
  MM3_pos.Angle = M_PI/4;
  MM4_pos.Angle = M_PI/4;
}

// measured by Dipanwita Banerjee
void Init_Geometry_2016B()
{
  // MM offsets actual for runs range: #2000 - ...
  ECAL_pos.pos = TVector3(  -380,   0,      0);
  ST2_pos.pos  = TVector3(     0,   0,  -2210);
  S2_pos.pos   = TVector3(     0,   0,   -713);
  SRD_pos.pos  = TVector3(     0,   0,  -2871);
  S1_pos.pos   = TVector3(     0,   0,  -2940);
  pipe_pos.pos = TVector3(     0,   0,  -3216);
  MAGD_pos.pos = TVector3(     0,   0, -20100+598+4000);
  MAGU_pos.pos = TVector3(     0,   0, -20100+598);      
  Veto_pos.pos = TVector3(     0,   0, -20235);
  S0_pos.pos   = TVector3(     0,   0, -20379);
  ST1_pos.pos  = TVector3(     0,   0, -20668);
  //GEM position
  GM2_pos.pos  = TVector3(     0,   0,  -1174);
  GM1_pos.pos  = TVector3(     0,   0,  -2233);
  //MM position
  MM1_pos.pos  = TVector3(    -2,   1, -20900);
  MM2_pos.pos  = TVector3(  -1.5, -11, -20100);
  MM3_pos.pos  = TVector3(  -315,   0,  -2490);
  MM4_pos.pos  = TVector3(  -350,   0,   -924);
  //MMangles
  MM1_pos.Angle = M_PI/4;
  MM2_pos.Angle = M_PI/4;
  MM3_pos.Angle = M_PI/4;
  MM4_pos.Angle = M_PI/4;
  //magnetic field
  MAG_field = 2.1;
}

// measured by Dipanwita Banerjee
void Init_Geometry_2017()
{
  // MM offsets actual for runs range: #2000 - ...
  ECAL_pos.pos = TVector3(     0,   0,     0);  
  pipe_pos.pos = TVector3(     0,   0,  -4260);
  MAGD_pos.pos = TVector3(     0,   0, -18130+170+4300);//magnet downstream side
  MAGU_pos.pos = TVector3(     0,   0, -18130+170);//magnet upstream side
  //GEM position
  GM1_pos.pos  = TVector3(     -294,   0,  -1126-234-1022-621);
  GM2_pos.pos  = TVector3(     -349,   0,  -1126);
  GM3_pos.pos  = TVector3(     -342,   0,  -1126-234-1022);
  GM4_pos.pos  = TVector3(     -343,   0,  -1126-234);  
  //MM position
  //////////////////////////////////////////////////////////////////
  ////first values is coordinate respect the beam axis in the beam area
  ////second cordinate is the correction for the beam axis measured
  //////////////////////////////////////////////////////////////////
  MM1_pos.pos  = TVector3(-17,-6,-19670) + TVector3(-1.3+0.2,-0.1-0.06,0);
  MM2_pos.pos  = TVector3(-6,8,-19580) + TVector3(-3.09,-13.82,0);
  MM3_pos.pos  = TVector3(-17,-1.5,-18220) + TVector3(-1.3,0.2,0);
  MM4_pos.pos  = TVector3(-8,2.7,-18130) + TVector3(4.2,-10.7,0);
  MM5_pos.pos  = TVector3(-317,0,-3170) + TVector3(-8.1,-8.5,0);
  MM7_pos.pos  = TVector3(-366,5,-1030) + TVector3(2.4,-5.25,0);
  //MM angles
  MM1_pos.Angle = M_PI/4;
  MM2_pos.Angle = 3*M_PI/4;
  MM3_pos.Angle = M_PI/4;
  MM4_pos.Angle = 3*M_PI/4;
  MM5_pos.Angle = M_PI/4;
  MM7_pos.Angle = M_PI/4;
  //magnetic field Tesla
  MAG_field = 1.8;
  
  // GEMs
  // TODO: the geometry was extracted from some detectors.dat and
  //       coordinate system does not match with conddb.h
  GM01Geometry = {
                          /*size*/ 256,
                          /*center*/ 0,
                          /*pitch mm*/ 0.4,
                          /*angle*/ 0.,
                          /*abs pos mm*/ GM1_pos.pos  // TODO: from detectors.dat {-295.10777,3.09302,16620.}
  };
  GM02Geometry = {
                          /*size*/ 256,
                          /*center*/ 0,
                          /*pitch mm*/ 0.4,
                          /*angle*/ 0.,
                          /*abs pos mm*/ GM2_pos.pos  // TODO: from detectors.dat {-34.381352,2.368,18605.0}
  };
  GM03Geometry = {
                          /*size*/ 256,
                          /*center*/ 0,
                          /*pitch mm*/ 0.4,
                          /*angle*/ 0.,
                          /*abs pos mm*/ GM3_pos.pos  // TODO: from detectors.dat {-349.46117,0.02498,17252.05}
  };
  GM04Geometry = {
                          /*size*/ 256,
                          /*center*/ 0,
                          /*pitch mm*/ 0.4,
                          /*angle*/ 0.,
                          /*abs pos mm*/ GM4_pos.pos  // TODO: from detectors.dat {-349.42229,7.41795,18371.05}
  };
}

// INVISIBLE mode, beam energy 100 GeV
// setup geometry of runs 3574 - 4207
void Init_Geometry_2018_invis100(const int run)
{
 //////////////////////////////////////////////////////////////////////////
 ///// Downstream detectors, Measured from ECAL position in Z and the
 //////from the deflection of the original beam line in X, second
 //////vector on the right if present represents a corrections
 //////for the tracking
 /////////////////////////////////////////////////////////////////////////
////  ECAL_pos.pos = TVector3(-391.3,   0,     0);  // X as calculated by Alexander Toropin
  ECAL_pos.pos = TVector3(     0,   0,     0);
  HCAL0_pos.pos = TVector3(     0,   0,    89);
  pipe_pos.pos = TVector3(     0,   0,  -4197);
  //Magnet positions  
////  MAGU_pos.pos = TVector3(     0,   0, -17466);//magnet upstream side
  #if 0
  /////////////////////////////////////
  /// this is the true magnet position,
  /// for simplicity we just use 4m magnet neglecting
  /// the gap to simplify calculations
  ///////////////////////////////////////////
  MAGD_pos.pos = TVector3(     0,   0, -12402);//magnet downstream side
  #endif
  // fake magnet position
////  MAGD_pos.pos = TVector3(     0,   0, -13466);//magnet downstream side

  MAGD_pos.pos = TVector3(     0,   0, -13466 - 2000 + 2453);//magnet downstream side
  MAGU_pos.pos = TVector3(     0,   0, -17466 + 2000 - 2453);//magnet upstream side


  //MM position
  //////////////////////////////////////////////////////////////////
  ////first values is coordinate respect the beam axis in the beam area
  ////second cordinate is the correction for the beam axis measured
  //////////////////////////////////////////////////////////////////
//  MM1_pos.pos  = TVector3(-10,   0,  -19671) + TVector3(0,                  0,   0 );
//  MM2_pos.pos  = TVector3(17,    0,  -18201) + TVector3(4.378,         -3.564,   0 );
//  MM3_pos.pos  = TVector3(-319,  0,  -3646 ) + TVector3(1.1,           -20.48,   0 );
//  MM4_pos.pos  = TVector3(-329,  0,  -3452 ) + TVector3(5.06-3, -20.48+0.7781,   0 );
//  MM5_pos.pos  = TVector3(-381,  0,  -1191 ) + TVector3(4.4,           -5.381,   0 );
// MM6_pos.pos  = TVector3(-397,  0,  -979  ) + TVector3(18.4,    -5.381-10.15,   0 );



  MM1_pos.pos  = TVector3(-10,   0,  -19671) + TVector3(0,                  0,   0 ) + TVector3(0,   +0.013,   0 ) + TVector3(11.1 + 4.0, 2.17 - 2.5, 0);
  MM2_pos.pos  = TVector3(17,    0,  -18201) + TVector3(4.378,         -3.564,   0 ) + TVector3(0,   +0.034,   0 ) + TVector3(11.1 + 4.0, 2.17 - 2.5, 0);
  MM3_pos.pos  = TVector3(-319,  0,  -3646 ) + TVector3(1.1,           -20.48,   0 ) + TVector3(7.06,+0.227,   0 ) + TVector3(11.1 + 4.0, 2.17 - 2.5, 0);
  MM4_pos.pos  = TVector3(-329,  0,  -3452 ) + TVector3(5.06-3, -20.48+0.7781,   0 ) + TVector3(5.84,-0.644,   0 ) + TVector3(11.1 + 4.0, 2.17 - 2.5, 0);
  MM5_pos.pos  = TVector3(-381,  0,  -1191 ) + TVector3(4.4,           -5.381,   0 ) + TVector3(-7.5,+0.219,   0 ) + TVector3(11.1 + 4.0, 2.17 - 2.5, 0);
  MM6_pos.pos  = TVector3(-397,  0,  -979  ) + TVector3(18.4,    -5.381-10.15,   0 ) + TVector3(-8.4,+0.155,   0 ) + TVector3(11.1 + 4.0, 2.17 - 2.5, 0);


  if (run > 3896 && run <= 3957)
  {
     MM3_pos.pos += TVector3(0, -0.047, 0);
     MM4_pos.pos += TVector3(0,  0.823, 0);
     MM5_pos.pos += TVector3(0, -0.015, 0);
     MM6_pos.pos += TVector3(0,  0.014, 0);
  }

  if (run > 3957 && run <= 4095)
  {
     MM3_pos.pos += TVector3(0,  0.000, 0);
     MM4_pos.pos += TVector3(0,  0.386, 0);
     MM5_pos.pos += TVector3(0, -0.013, 0);
     MM6_pos.pos += TVector3(0,  0.014, 0);
  }


  //magnetic field Tesla
////  MAG_field = 1.72;
  MAG_field = 1.8;


  //MM angles
  MM1_pos.Angle = -M_PI/4;
  MM2_pos.Angle = M_PI/4;
  MM3_pos.Angle = -M_PI/4;
  MM4_pos.Angle = -M_PI/4;
  MM5_pos.Angle = M_PI/4;
  MM6_pos.Angle = -M_PI/4;
  
  // GEMs, not yet corrected for tracking
  GM01Geometry = {
                          /*size*/ 256,
                          /*center*/ 0,
                          /*pitch mm*/ 0.4,
                          /*angle*/ 0.,
                          /*abs pos mm*/ {-259,0,-863}
  };
  GM02Geometry = {
                          /*size*/ 256,
                          /*center*/ 0,
                          /*pitch mm*/ 0.4,
                          /*angle*/ 0.,
                          /*abs pos mm*/ {-233,0,-1606}
  };
  GM03Geometry = {
                          /*size*/ 256,
                          /*center*/ 0,
                          /*pitch mm*/ 0.4,
                          /*angle*/ 0.,
                          /*abs pos mm*/ {-251,0,-1052}
  };
  GM04Geometry = {
                          /*size*/ 256,
                          /*center*/ 0,
                          /*pitch mm*/ 0.4,
                          /*angle*/ 0.,
                          /*abs pos mm*/ {-242,0,-1437}
  };
  
  // propagate to alternative GEM geometry
  GM1_pos.pos  = GM01Geometry.pos;
  GM2_pos.pos  = GM02Geometry.pos;
  GM3_pos.pos  = GM03Geometry.pos;
  GM4_pos.pos  = GM04Geometry.pos;
  
  // TODO: unify GEM geometry definition
  

  //////////////////////////////////////
  //Additional detector measured but not used for Tracking:
  ////////////////////////////////
  ///Straw measured are based on position of X plane, distance between X and Y plane
  /// was reported to be 15 mm
  /////////////////////////////////////////////////////
  //// StrawX6.pos = TVector3(0,0,-1866);
  //// StrawX7.pos = TVector3(0,0,-736);
  //// StrawX8.pos = TVector3(0,0,-3344);
  /////////////////////////////////////////
  //// Veto , S1 and tube begin were measured upstream
  ///////////////////////////////////////////////
  ///// pipe_begin.pos = TVector3(0,0,-17922);
  ///// Veto.pos = TVector3(0,0,-18731);
  ///// S1.pos = TVector3(0,0,-18833.5);
  /////////////////////////////////////////////////
  ///// left side of ECAL was perfectly aligned with
  //// with undeflected beam axis. Veto and HCAL
  //// are placed immediately after that
  ////////////////////////////////////////////////
  //// Measured beam outlet until after ring of the tube
  ///////////////////////////////////////////////////
  ///// BeamOutlet.pos = TVector3(0,0,−20073);
  ///////////////////////////////////////////////////
  
} //end Geometry 2018

// VISIBLE mode, beam energy 150 GeV
// setup geometry of runs 3574 - 4207
void Init_Geometry_2018_vis150()
{
 //////////////////////////////////////////////////////////////////////////
 ///// Downstream detectors, Measured from ECAL position in Z and the
 //////from the deflection of the original beam line in X, second
 //////vector on the right if present represents a corrections
 //////for the tracking
 /////////////////////////////////////////////////////////////////////////
  //WARNING: rough geometry using list_runs_may_2018_v2.pdf
  ECAL_pos.pos = TVector3(-391.3,   0,     0);  // X as calculated by Alexander Toropin
  pipe_pos.pos = TVector3(     0,   0,  -4197);
  //Magnet positions  
  MAGU_pos.pos = TVector3(     0,   0, -24111);//magnet upstream side
  #if 0
  /////////////////////////////////////
  /// this is the true magnet position,
  /// for simplicity we just use 4m magnet neglecting
  /// the gap to simplify calculations
  ///////////////////////////////////////////
  MAGD_pos.pos = TVector3(     0,   0, -19137);//magnet downstream side
  #endif
  // fake magnet position
  MAGD_pos.pos = TVector3(     0,   0, -20111);//magnet downstream side

  //MM position
  //////////////////////////////////////////////////////////////////
  ////first values is coordinate respect the beam axis in the beam area
  ////second cordinate is the correction for the beam axis measured for tracking
  //////////////////////////////////////////////////////////////////
  MM1_pos.pos  = TVector3(-10,   0,  -25948) + TVector3(0,                  0,   0 );  
  MM2_pos.pos  = TVector3(-10,  0,  -25581) + TVector3(4.4,           -5.381,   0 );
  MM3_pos.pos  = TVector3(17,    0,  -24846) + TVector3(4.378,         -3.564,   0 );
  MM4_pos.pos  = TVector3(17,    0,  -24479) + TVector3(4.378,         -3.564,   0 );
  //deflection calculated, not measured, corrected for point deflection algorithm
  MM5_pos.pos  = TVector3(-217,  0,  -6950 ) + TVector3(44.6,           -20.48,   0 );
  MM6_pos.pos  = TVector3(-220,  0,  -6750 ) + TVector3(38.06,        -10.7019,   0 );

  //magnetic field Tesla
  MAG_field = 1.72;
  

  //MM angles
  MM1_pos.Angle = -M_PI/4;
  MM2_pos.Angle = -M_PI/4;
  MM3_pos.Angle =  M_PI/4;
  MM4_pos.Angle =  M_PI/4;
  MM5_pos.Angle = -M_PI/4;
  MM6_pos.Angle = -M_PI/4;
  
  // GEMs, position received from Michael Hoesgen
  GM01Geometry = {
                          /*size*/ 256,
                          /*center*/ 0,
                          /*pitch mm*/ 0.4,
                          /*angle*/ 0.,
                          /*abs pos mm*/ {-300,0,-581}
  };
  GM02Geometry = {
                          /*size*/ 256,
                          /*center*/ 0,
                          /*pitch mm*/ 0.4,
                          /*angle*/ 0.,
                          /*abs pos mm*/ {-304,0,-1591}
  };
  GM03Geometry = {
                          /*size*/ 256,
                          /*center*/ 0,
                          /*pitch mm*/ 0.4,
                          /*angle*/ 0.,
                          /*abs pos mm*/ {-298,0,-1040}
  };
  GM04Geometry = {
                          /*size*/ 256,
                          /*center*/ 0,
                          /*pitch mm*/ 0.4,
                          /*angle*/ 0.,
                          /*abs pos mm*/ {-306,0,-1424}
  };
  
  // propagate to alternative GEM geometry
  GM1_pos.pos  = GM01Geometry.pos;
  GM2_pos.pos  = GM02Geometry.pos;
  GM3_pos.pos  = GM03Geometry.pos;
  GM4_pos.pos  = GM04Geometry.pos;
  
  // TODO: unify GEM geometry definition
  
  //magnetic field Tesla
  MAG_field = 1.72;
  
  // incoming beam
  NominalBeamEnergy = 150.;

  //////////////////////////////////////
  //Additional detector measured but not used for Tracking:
  ////////////////////////////////
  ///Straw measured are based on position of X plane, distance between X and Y plane
  /// was reported to be 15 mm
  /////////////////////////////////////////////////////
  //// StrawX6.pos = TVector3(0,0,-1866);
  //// StrawX7.pos = TVector3(0,0,-736);
  //// StrawX8.pos = TVector3(0,0,-3344);
  /////////////////////////////////////////
  //// Veto , S1 and tube begin were measured upstream
  ///////////////////////////////////////////////
  ///// pipe_begin.pos = TVector3(0,0,-17922);
  ///// Veto.pos = TVector3(0,0,-18731);
  ///// S1.pos = TVector3(0,0,-18833.5);
  /////////////////////////////////////////////////
  ///// left side of ECAL was perfectly aligned with
  //// with undeflected beam axis. Veto and HCAL
  //// are placed immediately after that
  ////////////////////////////////////////////////
  //// Measured beam outlet until after ring of the tube
  ///////////////////////////////////////////////////
  ///// BeamOutlet.pos = TVector3(0,0,−20073);
  ///////////////////////////////////////////////////
  
} //end Geometry 2018 VIS

void Init_Reconstruction(const int run)
{
  Reset_Calib();
  
  // test beam 2015: runs range 330 - 629
  if (330 <= run && run <= 629) Init_Calib_2015_BGO_ECAL_HCAL();
  
  // test beam 2016A: runs range 938 - 1600
  if (938 <= run && run <= 1371) Init_Calib_2016A_ECAL_cal1();
  if (1372 <= run && run <= 1600) Init_Calib_2016A_ECAL_cal2();
  if (1130 <= run && run <= 1600) Init_Calib_2016A_BGO();
  if (1456 <= run && run <= 1600) Init_Calib_2016A_SRD();
  if (1115 <= run && run <= 1600) Init_Calib_2016A_HCAL_cal2();
  if (938 <= run && run <= 1426) Init_Geometry_2016A_part1();
  if (1427 <= run && run <= 1600) Init_Geometry_2016A_part2();
  
  // test beam 2016B: runs range 1776 - 2551
  if (1776 <= run && run <= 2551) Init_Calib_2016B_ECAL();
  if (1776 <= run && run <= 2551) Init_Calib_2016B_HCAL();
  if (1776 <= run && run <= 2551) Init_Calib_2016B_SRD();
  if (1776 <= run && run <= 2551) Init_Calib_2016B_VETO();
  if (1776 <= run && run <= 2551) Corr_Calib_2016B(run);
  if (2458 <= run && run <= 2551) Init_Calib_2016B_VISMODE();
  if (1776 <= run && run <= 2551) Init_Calib_2016B_MM();
  if (1776 <= run && run <= 2551) Init_Geometry_2016B();
  
  // test beam 2017: runs range 2817 - 3573
  if (2817 <= run && run <= 3573) Init_Calib_2017_ECAL();
  if (2817 <= run && run <= 3573) Init_Calib_2017_VETO();
  if (2817 <= run && run <= 3573) Init_Calib_2017_HCAL();
  if (2817 <= run && run <= 3573) Init_Calib_2017_WCAL(run);
  if (2817 <= run && run <= 3573) Init_Calib_2017_SRD();
  if (2817 <= run && run <= 3573) Init_Calib_2017_MM();
  if (2817 <= run && run <= 3573) Init_Calib_2017_MasterTime(run);
  if (2817 <= run && run <= 3573) Init_Geometry_2017();
  if (2817 <= run && run <= 3573) Init_Calib_2017_Counters();

  // test beam 2018: runs range from 3574 - 4310
  if (3855 <= run && run <= 4207) Init_Geometry_2018_invis100(run);
  if (3855 <= run && run <= 4310) Init_Calib_2018_MM();
  
  // TODO: check range
  if (3870 <= run && run <= 4310) Init_Calib_2018_ECAL(run);
  if (3870 <= run && run <= 4310) Init_Calib_2018_HCAL(run);
  if (3870 <= run && run <= 4310) Init_Calib_2018_SRD();
  if (3870 <= run && run <= 4310) Init_Calib_2018_VETO(run);
  if (3870 <= run && run <= 4310) Init_Calib_2018_WCAL(run);
  if (3574 <= run && run <= 4310) Init_Calib_2018_MasterTime();
  if (3574 <= run && run <= 4310) Init_Calib_2018_Counters(run);

  //geometry for visible mode 2018, exact position to be checked with tracking
  if (4224 <= run && run <= 4310) Init_Geometry_2018_vis150();
  
  // MicroMega strip to electronics channel mapping
  Init_MM_mapping();
}
