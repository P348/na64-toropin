
#define OUTPUT 1

#define PROFILE 1
#define LEDCORR 1

#define WCATCHER 0

#define Variant1 0
#define Variant3 1

#define noCALIB 1 // if 1 it means it is not a calibration process

#include "proc_2019.h"

#define corrTime  0.
#define cell_size 38.3

float XY(float xi) {
  float x, x2;
  int i = (int) (xi+0.5);
  x = xi - (float)(i);
  x2 = x * x;
  return (x*(0.22672+x2*(3.60329+x2*(-24.1255+x2*(159.2098-x2*283.4825))))+i); /* 100 GeV ??? */  
}

#if PROFILE
//=================================================================================
// shower profile obtained from data using MM coordinates
#include "shower.cc"
#endif // end if PROFILE

static bool debug = false;//true;

//========================================================================================
void PlotECplane(std::vector<Cell> cells, int Plane)
{
  if (cells.size() < 2) return;
  if (Plane < 0 || Plane > 1) return;

  double EnergyTotal = 0;
  Cell cell;
  int RegionSize = 6;
  int iyMax = 5;
  float Mean[6][6];

  int Iy = iyMax;
  for (int j = 0; j < RegionSize; j++) {
    for (int i = 0; i < RegionSize; i++) {
      Mean[i][j] = 0;
      for (auto& it : cells) {
	cell = it;
	if (cell.plane != Plane) continue;
	if (cell.ix == i && cell.iy == Iy) {
	  if (cell.intime) {
	    Mean[i][j] = cell.signalcal;
	    EnergyTotal += cell.signalcal;
	  }
	  break;
	}
      }
    }
    Iy--;
  }  
  int ixbins = RegionSize*11;
  int iybins = RegionSize*11;

  printf("   ");
  for (int l = 0; l < ixbins; l++) printf("-");
  printf("\n");
  Iy = iyMax;
  for (int j = 0; j < RegionSize; j++) {
    printf("   %4d |", Iy);
    for (int i = 0; i < RegionSize; i++) {
      if (Mean[i][j] > 0) printf("%7.2f  |",Mean[i][j]);
      else printf("         |");
    }
    printf(" \n");
    Iy--;
    printf("   ");
    for (int l = 0; l < ixbins; l++) printf("-");
    printf("\n");
  }
  printf("   ix-> |");
  for (int i = 0; i < RegionSize; i++) {
    printf("  %3d    |", i);
  }
  printf("\n\n");
}


//==============================================================================================
void FindEcalCluster(std::vector<Cell> &cells, int Plane, float cal[2][6][6], CellReco &cellreco,
		     Calibs calibs)
// processing of ECAL data
{
  bool debug1 = false;//true;

  int iyMax(-1), ixMax(-1);
  float x(0), y(0);
  
  float cluster[3][3], clusterNocal[3][3], amp[6][6];
  bool intime[3][3];

  cellreco.Amax = 0;
  cellreco.plane = Plane;
  cellreco.time = 0;
  cellreco.ixMax = ixMax;
  cellreco.iyMax = iyMax;
  cellreco.secondhit = 0;
  cellreco.sum9 = 0;
  cellreco.sum9cal = 0;
  cellreco.sum36 = 0;
  cellreco.sum36cal = 0;
  cellreco.badEnergy = 0;
  cellreco.totalEnergy = 0;
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++) cellreco.cluster[i][j] = 0;
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++) cellreco.intime[i][j] = false;
  for (int i = 0; i < 6; i++) 
    for (int j = 0; j < 6; j++) cellreco.ampl36[i][j] = 0;

  
  bool secondHit = false;
  
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++) {
      cellreco.cluster[i][j] = 0;
      intime[i][j] = false;
    }

  if (cells.size() < 1) return;
  if (Plane < 0 || Plane > 1) return;

  for (int i = 0; i < 6; i++)
    for (int j = 0; j < 6; j++) {
      amp[i][j] = 0;
    }
  
  Cell cell;
  float amax(0), time(0);
  for (auto& it : cells) {
    cell = it;
    if (cell.plane != Plane) continue;
    int loc = ec_loc + ec_zxy2lin(Plane,cell.ix,cell.iy);
    float ledcorr = 1.;
#if LEDCORR    
    if (ledcell[loc].Led > 10.) ledcorr = calibs.ledECAL[Plane][cell.ix][cell.iy]/ledcell[loc].Led;
#endif    
    it.signalcal = cell.signal * cal[Plane][cell.ix][cell.iy] * ledcorr;
    if (cell.signal > amax && cell.intime) {
      amax = cell.signal;
      time = cell.time;
      ixMax = cell.ix;
      iyMax = cell.iy;
    }
  }
  if (ixMax < 0 || iyMax < 0) return;

  //----------- debug --------------------
  if ((ixMax != 3 || iyMax != 3) && Plane == 1) return; // only EC cell (3,3)
   
#if 1
  if (debug1) {
    std::cout << "\n ------------------ Looking for cluster in EC Plane "
	      << Plane << "  ---------------\n" << std::endl;
  }
#endif
  
  float sum9(0), sum9cal(0);
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++) {
      cluster[i][j] = 0;
      clusterNocal[i][j] = 0;
    }

  for (auto& it : cells) {
    cell = it;
    
    if (cell.plane != Plane) continue;
      int jx = cell.ix;
      int jy = cell.iy;
      if (cell.intime) amp[jx][jy] = cell.signalcal; // for debug

    if (abs(cell.ix - ixMax) < 2 && abs(cell.iy - iyMax) < 2) { // select 3x3 matrix
      int ix = cell.ix-ixMax+1;
      int iy = cell.iy-iyMax+1;
      if (ix >=0 && ix <= 2 && iy >= 0 && iy <= 2) {
	intime[ix][iy] = cell.intime;
	if (cell.intime) {
	  clusterNocal[ix][iy] = cell.signal;
	  cluster[ix][iy] = cell.signalcal;
	  sum9 += cell.signal;
	  sum9cal += cell.signalcal;
	  it.used = 1;
	}
      }
    }
  }
  
  if (debug1) {
    //std::cout << "  ------------------------------------ " << std::endl;
    for (int j = 0; j < 6; j++) {
      printf(" %d ", 5-j);
      for (int i = 0; i < 6; i++) 
	printf (" %7.2f", amp[i][5-j]);
      printf("\n");    
    }
    printf("   ");
    for (int i = 0; i < 6; i++)  printf (" %7d", i);
    printf(" \n");
    
    std::cout << " ------------ no calibration -----------" << std::endl;
    for (int j = 0; j < 3; j++) {
      for (int i = 0; i < 3; i++) 
	printf(" %7.2f", clusterNocal[i][2-j]);
      printf("\n");
    }    
    std::cout << "  amax = " << amax << "  sum9 = " << sum9 << "  ratio = " << amax/sum9
	      << "  sum9cal = " << sum9cal << std::endl;
    
    std::cout << "\n ------------ calibrated Ecal cluster -----------" << std::endl;
    for (int j = 0; j < 3; j++) {
      for (int i = 0; i < 3; i++) 
	printf(" %7.2f", cluster[i][2-j]);
      printf("\n");
    }
  } // end if (debug1)
  
  float sum36(0), sum36cal(0), badEnergy(0); // sum36 => sum25

  for (auto& it : cells) {
    cell = it;
    if (cell.plane != Plane) continue;
    if ((cell.ix >= 0 && cell.ix < 6) && (cell.iy >= 0 && cell.iy < 6)) { // 6x6 cells
      if (cell.intime) {
	sum36 += cell.signal;  // total energy in time
	sum36cal += cell.signalcal; // total energy (calibrated) in time
      } else {
	badEnergy += cell.signalcal; // bad energy (calibrated) out of time
      }
    }
  }
  
  float totalEnergy(0);
  for (auto& it : cells) {
    cell = it;
    if (cell.plane != Plane) continue;    
    if ((cell.ix >= 0 && cell.ix < 6) && (cell.iy >= 0 && cell.iy < 6)) {
      totalEnergy += cell.signalcal; // total energy (calibrated)
    }
  }

  if (debug1) std::cout << "  sum36 = " << sum36 << "  sum36cal = " << sum36cal
			<< "  badEnergy = " << badEnergy << std::endl;

  // look for additional hit (second ?)
  float amax1(0);
  int ixMax1(0), iyMax1(0);
  for (auto& it : cells) {
    cell = it;
    if (cell.plane != Plane) continue;
    if (cell.used || !cell.intime) continue;
    if (cell.signalcal > amax1) {
      amax1 = cell.signalcal;
      ixMax1 = cell.ix;
      iyMax1 = cell.iy;
    }
  }

  debug1 = false;//true;

  cellreco.Amax = amax;
  cellreco.time = time;
  cellreco.ixMax = ixMax;
  cellreco.iyMax = iyMax;
  if (secondHit) {
    cellreco.secondhit = 1;
    //std::cout << "  secondhit flag was set !" << std::endl;
  }
  cellreco.sum9 = sum9;
  cellreco.sum9cal = sum9cal;
  cellreco.sum36 = sum36;
  cellreco.sum36cal = sum36cal;
  cellreco.badEnergy = badEnergy;
  cellreco.totalEnergy = totalEnergy;
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++) cellreco.cluster[i][j] = cluster[i][j];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++) cellreco.intime[i][j] = intime[i][j];
  for (int j = 0; j < 6; j++) {
    for (int i = 0; i < 6; i++) 
     cellreco.ampl36[i][j] = amp[i][j];
  }  
  return;
}

//=======================================================================================
void FindHcalMaxCell(std::vector<Cell> cells, int Plane, float calHC[4][3][3],
		     CellReco &cellreco, Calibs calibs)
// processing of HCAL data
{
  bool debug2 = false;//true;
  
  int iyMax(-1), ixMax(-1);
  float cluster[3][3], ampl[3][3];
  //float delta(19.3); // Hcal cell size in cm 

  cellreco.Amax = 0;
  cellreco.plane = Plane;
  cellreco.ixMax = ixMax;
  cellreco.iyMax = iyMax;
  cellreco.sum9 = 0;
  cellreco.sum9cal = 0;
  cellreco.badEnergy = 0;
  cellreco.totalEnergy = 0;
  
  if (cells.size() < 2) return;
  if (Plane < 0 || Plane > 3) return;

  if (debug2) {
    std::cout << "\n      ------------------ Looking for cluster in HC Plane "
	      << Plane << "  ---------------\n" << std::endl;
  }
  
  Cell cell;
  float amax(0);
  for (auto& it : cells) {
    cell = it;
    if (cell.plane != Plane) continue;
    if (cell.signal > amax) {
      amax = cell.signal;
      ixMax = cell.ix;
      iyMax = cell.iy;
    }
  }
  if (ixMax < 0 || iyMax < 0) return;


  float sum9 = 0, sum9cal = 0;
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++) {
      cluster[i][j] = 0;
      ampl[i][j] = 0;
    }
           
  float badEnergy(0), totalEnergy(0);
  for (auto& it : cells) {
    cell = it;
    if (cell.plane != Plane) continue;
    ampl[cell.ix][cell.iy] = cell.signal;

    int loc = hc_loc + hc_zxy2lin(Plane,cell.ix,cell.iy);
    float ledcorr = 1.;
#if LEDCORR    
    if (ledcell[loc].Led > 10.) ledcorr = calibs.ledHCAL[Plane][cell.ix][cell.iy]/ledcell[loc].Led;
#endif
    float signalcal = cell.signal*calHC[Plane][cell.ix][cell.iy] * ledcorr;
    sum9 += cell.signal;
    if (cell.intime) {
      sum9cal += signalcal;
      cluster[cell.ix][cell.iy] = signalcal;
    } else {
      badEnergy += signalcal;
    }
    totalEnergy += signalcal;
  }

  if (debug2)
    {
      std::cout << "  ----- Hcal cluster (in-time)  ------ Plane = " << Plane
		<< " ------ "<< std::endl;
      std::cout << /*"  Sum9 = " << sum9 <<*/ "  sum9Cal = " << sum9cal << std::endl;
      for (int j = 2; j >= 0; j--) {
	for (int i = 0; i < 3; i++) 
	  printf(" %8.2f ", cluster[i][j]);
	printf("\n");
      }
      std::cout << "  ----- Hcal cluster (all)  ------ Plane = " << Plane
		<< " ------ "<< std::endl;
      std::cout << "  Sum9 = " << sum9 << std::endl;
      for (int j = 2; j >= 0; j--) {
	for (int i = 0; i < 3; i++) 
	  printf(" %8.2f ", ampl[i][j]);
	printf("\n");
      }
    }  // end if(debug2)
  
  cellreco.Amax = amax;
  cellreco.ixMax = ixMax;
  cellreco.iyMax = iyMax;
  cellreco.sum9 = sum9;
  cellreco.sum9cal = sum9cal;
  cellreco.badEnergy = badEnergy;
  cellreco.totalEnergy = totalEnergy;
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++) cellreco.cluster[i][j] = cluster[i][j];

  return;
}  

//======================================================================================

// ---- Use all cells of Ecal cluster -------------------

double na64xy0(double x) { 

  const double pol9[10] = {2.34583e+02, -1.58166e+03, 6.18984e+05, -6.62362e+06, 3.64489e+07, 
			   -9.51367e+07, 7.78454e+07, 1.48502e+08, -3.51502e+08, 2.05300e+08};

  const double norm = 156236; // function(0.86) = d/2 - integral
  const double d2 = 19.1;     // half of Ecal cell size in cm
  
  if (x > 0.86) x = 0.86;
  double out = pol9[0]*x + 0.5*pol9[1]*x*x + 1./3.*pol9[2]*x*x*x + 0.25*pol9[3]*x*x*x*x +
    0.2*pol9[4]*x*x*x*x*x + 1./6.*pol9[5]*x*x*x*x*x*x + 1./7.*pol9[6]*x*x*x*x*x*x*x +
    0.125*pol9[7]*x*x*x*x*x*x*x*x + 1./9.*pol9[8]*x*x*x*x*x*x*x*x*x +
    0.1*pol9[9]*x*x*x*x*x*x*x*x*x*x;
  return d2*out/norm;
}

void na64xy0coord(float Cluster[3][3], double &x0, double &y0, bool debug1) {
  
  const double d2 = 19.1;      // half of Ecal cell size in cm

  // ------ along horizontal axis ---------------
  float A[3], x(0), sign(0);
  for (int i = 0; i < 3; i++) A[i] = 0;
  
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) A[i] += Cluster[i][j];
  }
  
  x = -1;  
  if (A[2] >= A[0]) {
    x = (A[1]-A[2])/(A[1]+A[2]);
    sign = 1.;
  }
  else if (A[0] >= A[2]) {
    x = (A[1]-A[0])/(A[1]+A[0]);
    sign = -1.;
  }
  //if (debug1) printf(" A[0] =%5.1f A[1] =%6.1f A[2] =%5.1f etaX = %5.3f\n",A[0],A[1],A[2],x);

  x0 = d2 - na64xy0(x);
  x0 *= sign;
  
  // ----- along vertical axis --------------
  for (int i = 0; i < 3; i++) A[i] = 0;
  for (int j = 0; j < 3; j++) {
    for (int i = 0; i < 3; i++) A[j] += Cluster[i][j];
  }
  
  x = -1;  
  if (A[2] >= A[0]) {
    x = (A[1]-A[2])/(A[1]+A[2]);
    sign = 1.;
  }
  else if (A[0] >= A[2]) {
    x = (A[1]-A[0])/(A[1]+A[0]);
    sign = -1.;
  }
  //if (debug1) printf(" A[0] =%5.1f A[1] =%6.1f A[2] =%5.1f etaY = %5.3f\n",A[0],A[1],A[2],x);

  y0 = d2 - na64xy0(x);
  y0 *= sign;  
}

//=============== Check Bad bursts ============================================

/////////// Corrections by VV

bool bad_burst[400][200];

unsigned int list_badrun[] =
{
  //4091,
  //4092,
  //4093,
  0
};

void loadBadBurst()
{
  for (int iR = 0; iR < 400; iR++)
    for (int iB = 0; iB < 200; iB++)
      bad_burst[iR][iB] = false;
  
  FILE * fi = fopen("../badburst.dat", "r");
  
  int RunNumber;
  int BurstNumber;
  
  while (!feof(fi))
    {
      char buf[201];
      
      fgets(buf, 201, fi);
      sscanf(buf, "%d %d", &RunNumber, &BurstNumber);
      //     printf("%d %d\n", RunNumber, BurstNumber);
      bad_burst[RunNumber - 3890][BurstNumber] = true;
    }
  
  fclose(fi);
  
  int bad_burst_count = 0;
  for (int iR = 0; iR < 400; iR++)
    for (int iB = 0; iB < 200; iB++)
      {
	int iRun = 0;
	while (list_badrun[iRun] != 0) if (iR + 3890 == (int)list_badrun[iRun++])
					 bad_burst[iR][iB] = true;
	if (bad_burst[iR][iB]) bad_burst_count++;
      }
      printf("Number of badbursts  %d\n", bad_burst_count);
      //getchar();
}


bool badBurst(Header & header)
{
  return bad_burst[header.RunNumber - 3890][header.BurstNumber];
};


//////////////////////////////////////////////////////////////////////////////////////////
//========================================================================================
int main(int argc, const char *argv[]) 
//------------------------------------
{

  unsigned char error = 0; // success.
  
  bool debug = false;// true;
  TFile *hfile = 0;
  
  try
    {
      char *file_out = strdup("Calib.root");
      char *maps = strdup("/online/detector/maps");
      char *input_files_list = strdup("");
      int errors_list = 0, event_errors=0, chips_stat=0, event_header=0, filter_CAL=0, stat_TT=0;
      const uint32 trigger_mask_all_bits = 0xffffffff;
      uint32 trigger_mask = trigger_mask_all_bits;
      DaqEventsManager manager;
      
      struct poptOption options[] =
        {
	  { "events",     '\0',   POPT_ARG_INT,
	    NULL, 'N',
	    "Maximum number of events to be analyzed", "NUMBER" },
	  { "filter-CAL",'\0',    POPT_ARG_NONE,
	    &filter_CAL,  0,
	    "Use filter for calorimeter events calibration (random trigger" \
	    " and calibration events).",
	    "" },
	  { "out",        '\0',   POPT_ARG_STRING|POPT_ARGFLAG_SHOW_DEFAULT,
	    &file_out,  0,
	    "Name of the output .root file (decoded information drom detectors).",
	    "PATH" },
	  { "input",      '\0',   POPT_ARG_STRING|POPT_ARGFLAG_SHOW_DEFAULT,
	    &input_files_list,  'l',
	    "File name with the list of input raw files.", "PATH" },
	  { "maps",       '\0',   POPT_ARG_STRING|POPT_ARGFLAG_SHOW_DEFAULT,
	    &maps,  'm',
	    "Map file or direcory", "PATH" },
	  { "event-errors",'\0',  POPT_ARG_INT|POPT_ARGFLAG_SHOW_DEFAULT,
	    &event_errors, 0,
	    "Print the list of encounted problems per event. 0-no print," \
	    " 1-errors summary, 2-detailed list", "NUMBER" },
	  { "errors",     '\0',   POPT_ARG_NONE,
	    &errors_list, 0,
	    "Print the list of encounted problems at the end", "" },
	  { "stat",       '\0',   POPT_ARG_NONE,
	    &chips_stat, 0,
	    "Print chips statistics", "" },
	  { "stat-TT",    '\0',   POPT_ARG_NONE,
	    &stat_TT, 0,
	    "Print trigger time statistics (for calibration)", "" },
	  { "trigger",    '\0',   POPT_ARG_INT|POPT_ARGFLAG_SHOW_DEFAULT,
	    &trigger_mask, 0,
	    "Trigger mask (skip events if mask does not fit)", "NUMBER" },
	  { "sep",        '\0',   POPT_ARG_NONE,
	    &event_header, 0,
	    "Separate events on screen", "" },
	  POPT_AUTOHELP
	  POPT_TABLEEND
        };
      
      poptContext poptcont = poptGetContext(NULL,argc,argv,options,0);
      poptSetOtherOptionHelp(poptcont,
			     "<options...> <data-file> [<data-file> <data-file> ...]\n"
			     "  read COMPASS raw data.\n"
			     "  Author: Alexander Zvyagin <Alexander.Zvyagin@cern.ch>\n"
			    );
      
      if (debug) cout << " ================= Start ========================= " << endl;

      int Nev = 1; // events counter
      
      int rc;
      while ((rc = poptGetNextOpt(poptcont)) != -1)
        {
	  switch(rc)
            {
	    case 'N':
	      manager.SetEventsMax(atoi(poptGetOptArg(poptcont)));
	      break;
	      
	    case 'm':
	      manager.SetMapsDir(poptGetOptArg(poptcont));
	      break;
	      
	    case 'l':
	      {
		const char *s = poptGetOptArg(poptcont);
		ifstream f(s);
		if (!f.is_open())
		  {

		    printf("%s\n",s);
		    throw "Can not open a list file.";
		  }
		
		string fname;
		while (f>>fname) {
		  manager.AddDataSource(fname);
		} 
		break;
	      }
	      
	    default:
	      fprintf(stderr, "bad argument %s: %s\n",
		      poptBadOption(poptcont, POPT_BADOPTION_NOALIAS),
		      poptStrerror(rc));
	      throw "";
	    }
	}
      
      // Get all data files
      while (true)
	{
	  const char *data_file = poptGetArg(poptcont);
	  
	  if (data_file == NULL)
	    break;
	  
	  manager.AddDataSource(data_file);
	}
      
      if (manager.GetDataSources().empty())
	{
	  poptPrintHelp(poptcont,stdout,0);
	  return 1;
	}
      
      //manager.Print();
      if (file_out != NULL && *file_out != 0)
	{
          hfile = new TFile(file_out, "RECREATE", "P348 root tree");
	}
      
      Header header; // Event header
      Cell cell;
      Cuts cuts;

//------------------------------------------------------------------------------------
      // description of histograms to test processing of data
#include "plots.h"
//------------------------------------------------------------------------------------      

       int counter(0);  // event counter   
      // This is the main loop over events.
      
#if PROFILE     
      Shower shower; 
#endif

      // ============== load Bad Burst stuff ===================================      
      loadBadBurst();
      //========================================================================

      int RunNumber = 0;
      
      while (manager.ReadEvent())
	{
	  debug = false;	  
          counter++; 
          if (int(counter/10000)*10000 == counter) cout << " --- Event = " << counter << endl;

	  //if (counter > 10000) continue;
	
	  header.RunNumber = manager.GetEvent().GetRunNumber();
	  header.EventNumber = manager.GetEvent().GetEventNumberInRun();
	  header.BurstNumber = manager.GetEvent().GetBurstNumber();
	  header.NumberInBurst = manager.GetEvent().GetEventNumberInBurst();

	  //=============== Check Bad bursts ============================================

	  if (header.RunNumber == 4060) continue; // skip strange run
	  
	  bool badevent = badBurst(header);
	 
	  if (badevent) continue; //{
	  //std::cout << "  this event is bad!  Number = " << header.EventNumber
	  //	      << "  Run = " << header.RunNumber << std::endl;
	  //}

	  RecoEvent e;
	  e.Calibration(header);
	  
	  int numberinburst = manager.GetEvent().GetEventNumberInBurst();
	  int burstnumber = manager.GetEvent().GetBurstNumber();

	  if (debug) {
	    std::cout << "\n  RunNumber = " << header.RunNumber << "  EventNumber = "
		      << header.EventNumber << "  BurstNumber = " << burstnumber
		      << "  EventNumberInBurst = " << numberinburst << std::endl;
	    std::cout << "  =======================================================";
	    std::cout <<  "==============================" << std::endl << std::endl;
	  }
	  
	  if (trigger_mask != trigger_mask_all_bits &&
	      !(manager.GetEvent().GetTrigger()&trigger_mask))
	    continue;
	  
	  if (event_header)
	    {
	      printf("*********************************************\n");
	      printf("NEW EVENT. Trigger: ");
	      bits_print(cout,manager.GetEvent().GetTrigger());
	      printf("\n");
	      manager.GetEvent().GetHeader().Print();
	      printf("\n");
	    }

	  if (event_header)
	    {
	      printf("Decoding started ...\n");
	      fflush(stdout);
	    }
	  
	  // skip auxialary (non-physics) events
	  const DaqEvent::EventType type = manager.GetEvent().GetType();
	  if (type != DaqEvent::PHYSICS_EVENT) continue;
	  
	  bool decoded = manager.DecodeEvent();
	  //------------------------------------
	  
	  if (event_header)
	    printf("Decoding %s\n", decoded?"OK":"failed");
	  
	  for( vector<DaqError>::const_iterator 
		 e = manager.GetEvent().GetDaqErrors().errors.begin();
	       e != manager.GetEvent().GetDaqErrors().errors.end(); e++)
	    {
	      try
		{
		  CS::Exception v =* e;
		}
	      catch(const std::exception &e)
		{
		  printf("%s\n",e.what());
		}
	    }
	  
	  if (!decoded) continue;

          e.MMinit(header.RunNumber);	  
          e.DETinit();
	  
	  bool debug5 = debug;//true;//false;
	  
	  // Decode all digits of the event
	  for (auto& d_it : manager.GetEventDigits()) {
	    const Chip::Digit* digit = d_it.second;
	    
	    // process APV digits
	    const CS::ChipAPV::Digit* apv = dynamic_cast<const CS::ChipAPV::Digit*>(digit);
	    if (apv) {
               e.MMproc(apv);	     
	    }	    
	    // process SADC digits	    
	    const ChipSADC::Digit* sadc = dynamic_cast<const ChipSADC::Digit*>(digit);	    
	    if (sadc) {
	      // Does the detector name match the pattern?
	      e.DETproc(sadc);
	    }
	  }
	  
	  cuts.nTotal++; // total number of trigger events

#if 1
	  /////// by VV

	  int mpStraw = 0;
	  
	  for (auto& d_it : manager.GetEventDigits()) {
	    const Chip::Digit* digit = d_it.second;
	    
	    const ChipNA64TDC::Digit* tdc = dynamic_cast<const ChipNA64TDC::Digit*>(digit);

	    //if (tdc) std::cout << " tdc: " << tdc << std::endl;

	    if (tdc) {
	      if (tdc->GetDetID().GetName() == "ST04X" && tdc->GetTime() < 840 && tdc->GetTime() >
		  760 && tdc->GetWire() != 64)
		{
		  //printf("%10s  %d\n", tdc->GetDetID().GetName().c_str(), tdc->GetWire());
		  
		  mpStraw++;
		  
		}
	    }
	  }

	  //std::cout << "  mpStraw = " << mpStraw << std::endl;
	  
	  if (mpStraw > 5 || mpStraw == 0) continue; // only runs with good straw

#endif
	  cuts.cutStraw++; // total number of events after Straw cut
	  
	  bool EXY_OK(false), ECAL_OK(false), HCAL_OK(false), VETO_OK(false);
	  bool /*SHWR_OK(false),*/ SRD_OK(false), SRD_OK1(false);
	  bool MM_OK(false), CHI_SQ(false), EC_RATIO(false), HC2_MUON(false);
	  bool HC_PASS1(false), HC_PASS2(false), HC3_MUON(false), besttrack(false);
	  bool Angle12_cut(false), SRD_SUM(false), R0_Cut(false);
#if WCATCHER	  
	  bool WCAL_OK(false);
#endif	  
          // ------------ Check trigger --------------------------

          e.TRGcells.clear();
          e.Trigger();
	  
          float trig[6];
	  debug = false;

          for (int i = 0; i < 6; i++) trig[i] = 0;
	  float TmaxT2 = 0;

          for (auto& it : e.TRGcells)
            {
              cell = it;
              if (cell.plane == 0) {
		//trig[cell.plane] = cell.time - MasterTimeShift*12.5; // mstTime
		trig[cell.plane] = cell.time - e.calibs.timeT[2]; // mstTime
		
		if (debug) { 
		  std::cout << " trig[0] = " << trig[0] << "  cell.time = " << cell.time
			    << "  detTime = " << e.calibs.timeT[2] << std::endl;
		}
		
		TmaxT2 = cell.time;
		timet2->Fill(TmaxT2);
	      }
	      else if (cell.plane == 1) trig[cell.plane] = cell.time;
	      
              if (cell.signal > 0) histTRG[cell.plane]->Fill(trig[cell.plane], 1);
            }
          
          if (debug) { 
	    //cout << "\n ========= manager: ReadEvent " << Nev++ << " ===========" << endl;
            std::cout << " trig[0] = " << trig[0] << "  trig[1] = " << trig[1] << std::endl;
	  }
	  //debug = false;

	  //if (fabs(trig[0]) > 13) continue; // original cut on S2 time
	  if (fabs(trig[0]) > 15) continue; // original cut on S2 time // 3-May-2019
	  cuts.cutT2time++;

	  hNumberEventsInSpill->Fill(float(numberinburst));

	  e.masterTimeLoc = false;
	  e.DETcollectHits();
	  
	  CellReco cellreco, cellreco1;

	  //debug = true; 
	  
	  float ECintime[2], ECoutoftime[2];
	  for (int i = 0; i < 2; i++) {
	    ECintime[i] = 0;
	    ECoutoftime[i] = 0;
	  }
	  //--------------- loop over ECcells to make cuts -------------------------------------
	  //------------------------------------------------------------------------------------
	  for (auto& it : e.ECcells)
	    {
	      cell = it;
	      int ix = cell.ix;
	      int iy = cell.iy;
	      int plane = cell.plane;
	      float Amax = cell.signal;
	      
	      if (Amax > 0) histECP[plane][ix][iy]->Fill(Amax, 1);
	      
	      float deltaT = cell.deltaT;

	      if (cell.signal > 40) histECtime[plane][ix][iy]->Fill(deltaT, 1);

	      if (debug) {
		if (cell.intime) {
		  ECintime[plane] += Amax*e.calEC[plane][ix][iy];
		}
		else {
		  ECoutoftime[plane] += Amax*e.calEC[plane][ix][iy];
		}
	      }
	      if (debug && Amax > 0) printf("  EC%d: ix = %d  iy = %d  deltaT = %7.2f "\
					    "Ampl = %6.2f  Amplcal = %5.2f\n", plane, ix, iy, 
					    deltaT, Amax, Amax*e.calEC[plane][ix][iy]);	      

	    } //------------ end of loop over ECcells --------------------------------------

	  if (debug) {
	    printf(" PS energy InTime = %7.2f, OutOfTime = %7.2f\n", ECintime[0],
		   ECoutoftime[0]);
	    printf(" EC energy InTime = %7.2f, OutOfTime = %7.2f\n", ECintime[1],
		   ECoutoftime[1]);
	  }
	  //--------------------------------------------------------------------------------
	  //================================================================================
	  //=================================== EC =========================================
	  // ------------------------ process Ecal clusters --------------------------------	  
	  float SumEC(0), SumEC36(0), SumECbad(0), SumECtotal(0);
	  float PreshCal(0);
	  bool EcalBadEnergyPass(false);
	  
	  float cluster[3][3];
	  for (int i = 0; i < 3; i++)
	    for (int j = 0; j < 3; j++) cluster[i][j] = 0;

	  float Ampl36[6][6];
	  for (int i = 0; i < 6; i++)
	    for (int j = 0; j < 6; j++) Ampl36[i][j] = 0;
	  
	  float EC011 = 0;
	  float x33 = 1000.;
	  float y33 = 1000.;
	  double ECx0(0), ECy0(0); // coordinates of shower in cell(3,3) (using EC signals)

	  EXY_OK = false;
	  //SHWR_OK = false;
	  debug = false;//true;
	  // ----------------------- EC clusters ------------------------------
	  for (int ipl = 0; ipl < 2; ipl++) {
	    //---------------------------------------------------------------------------------
	    FindEcalCluster(e.ECcells, ipl, e.calEC, cellreco, e.calibs);//, spillNb,hECledcorrVSspill);
	    //---------------------------------------------------------------------------------

	    if (cellreco.Amax == 0) continue;
	    if (ipl == 1) EXY_OK = true; // cluster max is in (3,3)
	    
	    if (debug && ipl == 1) {
	      std::cout << "\n  Ecal plane = " << ipl << "  ixMax = " << cellreco.ixMax
			<< "  iyMax = " << cellreco.iyMax << "  Amax = " << cellreco.Amax
			<< "  time = " << cellreco.time << std::endl;
	      PlotECplane(e.ECcells, ipl);
	    }
	    
	    if (cellreco.sum9cal > 0) histECPcal[ipl]->Fill(cellreco.sum9cal, 1);
	    if (cellreco.sum9 > 0) histECPnocal[ipl]->Fill(cellreco.sum9, 1);
	    if (cellreco.sum36cal > 0) histECPcal36[ipl]->Fill(cellreco.sum36cal, 1);
	    if (cellreco.sum36 > 0) histECPnocal36[ipl]->Fill(cellreco.sum36, 1);

	    SumEC += cellreco.sum9cal;
	    SumEC36 += cellreco.sum36cal;
	    SumECtotal += cellreco.totalEnergy;
	    SumECbad += cellreco.badEnergy;

	    if (ipl == 0) PreshCal = cellreco.sum36cal;
	    for (int i = 0; i < 3; i++)
	      for (int j = 0; j < 3; j++) cluster[i][j] += cellreco.cluster[i][j];

	    for (int j = 0; j < 6; j++) {
	      for (int i = 0; i < 6; i++) 
		Ampl36[i][j] += cellreco.ampl36[i][j];
	    }
	  } // -------------- end of EC clusters ----------------------------------------
	  debug = false;

	  float AmplX[6], AmplY[6];
	  for (int i = 0; i < 6; i++) {
	    AmplX[i] = 0; AmplY[i] = 0;
	  }
	  for (int j = 0; j < 6; j++)
	    for (int i = 0; i < 6; i++) 
	      AmplY[j] += Ampl36[i][j];

	  
	  for (int i = 0; i < 6; i++)
	    for (int j = 0; j < 6; j++) 
	      AmplX[i] += Ampl36[i][j];

	  float AmplX3 = AmplX[3], AmplY3 = AmplY[3];
	  for (int i = 0; i < 6; i++) {
	    AmplY[i] /= AmplY3;
	    AmplX[i] /= AmplX3;
	  }
	  	  
#if noCALIB	  
	  if (!EXY_OK) continue; // cut Ecal empty cell (or beam does not hit cell (3,3))
#endif
	  cuts.cutEC33++;

	  ECAL_OK =  /*SHWR_OK &&*/  (SumECtotal < 150.) && ((SumECtotal - SumEC36) < 30.);
	  // && SumEC36 < 50.;

	  //=================================================================================
	  //===============================  HC  ============================================
	  // ------------------------- loop over HCcells -----------------------------------
	  float HCintime[4], HCoutoftime[4];
	  for (int i = 0; i < 4; i++) {
	    HCintime[i] = 0;
	    HCoutoftime[i] = 0;
	  }

	  for (auto& it : e.HCcells)
	    {
	      //if (debug) std::cout << " -------- process Hcal hits ---------" << std::endl; 
	      cell = it;
	      int ix = cell.ix;
	      int iy = cell.iy;
	      int plane = cell.plane;
	      float Amax = cell.signal;
	      if (Amax > 0) {
		histHCP[plane][ix][iy]->Fill(Amax, 1);		
		if (cell.intime) histHCPcal[plane][ix][iy]->Fill(Amax*e.calHC[plane][ix][iy], 1);
	      }

	      if (Amax > 0) {  // cut weak signals
		float deltaT = cell.deltaT;
		if (cell.signal > 40) histHCtime[plane][ix][iy]->Fill(deltaT, 1);		  

		if (debug) {
		  if (cell.intime) {
		    HCintime[plane] += Amax*e.calHC[plane][ix][iy];
		  }
		  else {
		    HCoutoftime[plane] += Amax*e.calHC[plane][ix][iy];
		  }
		}
		
		if (debug) printf("  HC%d: ix = %d  iy = %d  deltaT = %7.2f  " \
				  " Ampl = %6.2f  Amplcal = %5.2f\n", plane,
				  ix, iy, deltaT, Amax, Amax*e.calHC[plane][ix][iy]);	
	      }
	    } // ------------------------- end of loop over HCcells --------------------------

	  if (debug) {
	    for (int i = 0; i < 4; i++) {
	      printf(" HC%d energy InTime = %7.2f, OutOfTime = %7.2f\n", i, HCintime[i],
		     HCoutoftime[i]);
	    }
	  }
	  
	  // ---------- process Hcal clusters -------------------------------
	  float SumHC(0), SumHC2(0), eHC_muon[4];
	  float SumHCbad(0), SumHCbad2(0), SumHCtotal(0);
	  float SumHC012intime(0), SumHC012(0);
	  float SumHC0(0), SumHC0_11(0);
	  float HCclusters[4][3][3];

	  for (int k = 0; k < 4; k++) {
	    eHC_muon[k] = 0.;
	    for (int i = 0; i < 3; i++) 
	      for (int j = 0; j < 3; j++) HCclusters[k][i][j] = 0;
	  }
	  
	  for (int ipl = 0; ipl < 4; ipl++) {
	    FindHcalMaxCell(e.HCcells, ipl, e.calHC, cellreco1, e.calibs);//,spillNb,hHCledcorrVSspill);
	    if (cellreco1.Amax == 0) continue; // skip empty planes
#if !noCALIB	    
            //if (cellreco1.ixMax != 1 || cellreco1.iyMax != 1) continue; // ???????
#endif	    
	    for (int i = 0; i < 3; i++)
	      for (int j = 0; j < 3; j++) HCclusters[ipl][i][j] = cellreco1.cluster[i][j];
	    
	    if (debug) printf("Hcal%d ixMax = %d iyMax = %d  Amax = %6.2f Ampl = %6.2f"\
			      "  Amplcal = %5.2f\n", ipl, cellreco1.ixMax, cellreco1.iyMax,
			      cellreco1.Amax, cellreco1.sum9, cellreco1.sum9cal);

	    if (cellreco1.sum9 > 0) histHCPsum[ipl]->Fill(cellreco1.sum9, 1);
	    if (cellreco1.sum9cal > 0) histHCPsumcal[ipl]->Fill(cellreco1.sum9cal, 1);

	    SumHC += cellreco1.sum9cal;         // total sum of intime energies in all 4 planes
	    SumHCtotal += cellreco1.totalEnergy;
	    SumHCbad += cellreco1.badEnergy;
	    
	    eHC_muon[ipl] = cellreco1.sum9cal;

	    if (ipl < 2) {
	      SumHC2 += cellreco1.sum9cal;      // total sum of intime energies in first 2 planes
	      SumHCbad2 += cellreco1.badEnergy; // total sum of energies in first 2 planes
	    }

	    if (ipl < 3) {
	      SumHC012intime += cellreco1.sum9cal;
	      SumHC012 += cellreco1.totalEnergy;
	    }
	    if (ipl == 0) {
	      SumHC0 = cellreco1.sum9cal;
	      SumHC0_11 = HCclusters[ipl][1][1];
	    }
	    
	  } // ---------- end of process Hcal clusters -------------------

#if Variant1	  
	  HCAL_OK = (eHC_muon[0] > 4. && eHC_muon[0] < 7.) && eHC_muon[1] > 4. && eHC_muon[2] < 2.;
#endif	  
#if Variant3	  
	  HCAL_OK = (eHC_muon[0] > 4. && eHC_muon[0] < 7.);	  
#endif
		    
	  histHCtotalBad->Fill(SumHCbad, 1);
	  histHCtotalBad2->Fill(SumHCbad2, 1);
	  //=============================================================================
	  
	  hHCvsEC_EC33cuts->Fill(SumEC36, SumHC2); // the first plot	  	  
	  profile_EC33cuts->Fill(SumEC36, SumHC2);
	  
	  // ================== SRD ====================================================
	  float SumSRDcal(0);
	  float SRDThreshold(0.);
	  float SRDcal012[3];
	  
	  float SRDsignal[3];
	  debug = false;
	  
	  bool srdtok[3];
	  float SRDthird(1.3);
	  float srdMin[3] = {1.3, 1.3, 1.3}, srdMax = 80.;
	  //float srdMin[3] = {3., 3., 3.}, srdMax = 90.;

	  float SRDintime[3], SRDoutoftime[3];
	  for (int i = 0; i < 3; i++) {
	    SRDintime[i] = 0;
	    SRDoutoftime[i] = 0;
	  }

	  //================================================================================
	  //--------------------- loop over SRDcells ---------------------------------------
	  for (auto& it : e.SRDcells)
	    {
	      if (debug) std::cout << " ---------- process SRD hits ----------" << std::endl; 
	      cell = it;	      
	      int ix = cell.ix;
	      srdtok[ix] = false;
	      float Amax = cell.signal;
	      float Amaxcal = Amax*e.calSRD[ix];
	      SRDsignal[ix] = Amaxcal;
	      
	      histSRD[ix]->Fill(Amax, 1);	      
	      if (debug) {
		printf("  SRD:  ix = %d  ampl = %5.1f  Ampl.cal = %6.3f  calSRD = %6.4f",
		       ix, Amax, Amax*e.calSRD[ix], e.calSRD[ix]);
		printf("  time = %6.1f  intime = %1d\n", cell.time, cell.intime);
	      }
	      
	      float deltaT = cell.deltaT;
	      if (cell.signal > 40) histSRDtime[ix]->Fill(deltaT, 1);

	      if (debug) {
		if (cell.intime) {
		  SRDintime[ix] += Amax*e.calSRD[ix];
		}
		else {
		  HCoutoftime[ix] += Amax*e.calSRD[ix];
		}
	      }
			      
	      if (debug) std::cout << "  SRDplane" << ix << "  deltaT = " << deltaT
				   << "  signal = " << SRDsignal[ix] <<  std::endl;
	      
	      srdtok[ix] = (SRDsignal[ix] > srdMin[ix]) && (SRDsignal[ix] < srdMax)  &&
	        cell.intime;
	  
	      if (cell.intime) {
		histSRDcal[ix]->Fill(Amaxcal, 1);
		SumSRDcal += Amaxcal;
		SRDcal012[ix] = Amaxcal;
	      }
	    } //--------------------- end of loop over SRDcells -------------------------------

	  if (debug) {
	    for (int i = 0; i < 3; i++) {
	      printf(" SRD%d energy InTime = %7.2f, OutOfTime = %7.2f\n", i, SRDintime[i],
		     SRDoutoftime[i]);
	    }
	  }
	  
	  debug = false;

	  if (SumSRDcal > 0) hSumSRDcal->Fill(SumSRDcal, 1);

	  SRD_OK = (srdtok[0] && srdtok[1] && SRDsignal[2] > SRDthird) ||
	       (srdtok[0] && srdtok[2] && SRDsignal[1] > SRDthird) ||
	       (srdtok[1] && srdtok[2] && SRDsignal[0] > SRDthird);

#if noCALIB
	  //std::cout << "  SRD_OK = " << SRD_OK << "  SumSRDcal = " << SumSRDcal << std::endl;
	  
	  if (!SRD_OK) continue;
#endif	  
	  cuts.cutSRD++;	  
	  hHCvsEC_SRDcuts->Fill(SumEC36, SumHC2);	  

	  //==================================================================================
	    
	  VETO_OK = false;
	  float sumVETO(0), sumVETO23(0), sumVETO01(0), sumVETO45(0);
	  bool vetook[6];
	  //float veto[6], vetoCut(1.35);
	  //float veto[6], vetoCut(1.0);
	  float veto[6], vetoCut(0.5);
	  // ------------------------- loop over VETOcells ----------------------------------------
	  for (auto& it : e.VETOcells)
	    {
	      cell = it;
	      int ix = cell.ix;
	      veto[ix] = 0;
	      float Amax = cell.signal;
	      float Amaxcal = cell.signal*e.calVETO[ix];
	      vetook[ix] = cell.intime;  //time inside
	      if (cell.intime) veto[ix] = Amaxcal;
	      
	      float deltaT = cell.deltaT;
	      if (cell.signal > 40.) histVETOtime[ix]->Fill(deltaT, 1);
	      
	      if (debug) printf("  VETO%d: Vetocal = %6.4f  fabs(dt) = %5.2f\n",
				ix, Amaxcal, deltaT);
	      
	      if (Amax > 0) {
		histVETO[ix]->Fill(Amax, 1);
		histVETOcal[ix]->Fill(Amaxcal);
	      }
	      if (Amaxcal > 0 && vetook[ix]) sumVETO += Amaxcal;
	      if (Amaxcal > 0 && (ix == 0 || ix == 1) && vetook[ix]) sumVETO01 += Amaxcal;
	      if (Amaxcal > 0 && (ix == 2 || ix == 3) && vetook[ix]) sumVETO23 += Amaxcal;
	      if (Amaxcal > 0 && (ix == 4 || ix == 5) && vetook[ix]) sumVETO45 += Amaxcal;
	    } // ----------------------- end of loop over VETOcells --------------------------
	  
	  hSumVETO->Fill(sumVETO, 1);
	  hSumVETO01->Fill(sumVETO01, 1);
	  hSumVETO23->Fill(sumVETO23, 1);
	  hSumVETO45->Fill(sumVETO45, 1);
	  
	  VETO_OK = ((veto[0]+veto[1]) < vetoCut) && ((veto[2]+veto[3]) > 1.5) &&  // !!!! charged !!!!
	    ((veto[4]+veto[5]) < vetoCut);
	  //VETO_OK = ((veto[0]+veto[1]) < vetoCut) && ((veto[2]+veto[3]) < vetoCut) &&  // !!!! neutral !!!!
	  //  ((veto[4]+veto[5]) < vetoCut);

	  double sumVETO012345 = sumVETO01+sumVETO23+sumVETO45;

	  double WCALadc(0), WCALadcCal(0);
#if WCATCHER // ------------------------ WCAL processing ------------------------------------
	  debug = false;//true;
	  WCAL_OK = false;
	  bool wcalok[3];
	  float wcal[3], wcalCut(100.);
	  // ------------------------- loop over WCALcells -----------------------------------
	  for (auto& it : e.WCcells)
	    {
	      cell = it;
	      int ix = cell.ix;
	      wcal[ix] = 0;
	      if (ix != 2) continue;
	      
	      float Amax = cell.signal;
	      float Amaxcal = cell.signal*calWC[ix];

	      WCALadc = Amax;
	      WCALadcCal = Amaxcal;
	      
	      wcalok[ix] = cell.intime;  //time inside
	      if (cell.intime) wcal[ix] = Amaxcal;
	
	      float deltaT = cell.deltaT;
	      
	      if (debug) printf("  WCAL%d: WCAL = %7.2f  WCALcal = %6.4f  fabs(dt) = %5.2f\n",
				ix, Amax, Amaxcal, deltaT);
	      
	      if (Amax > 0) histWC[ix]->Fill(Amax, 1);
	      if (Amax > 0) histWCcal[ix]->Fill(Amaxcal, 1);
	      if (Amax > 0 && wcalok[ix]) histWCcalInTime[ix]->Fill(Amaxcal, 1);
	    } // ----------------------- end of loop over WCcells --------------------------

	  if (debug) std::cout << std::endl;
	  
	  WCAL_OK = (wcal[2] < wcalCut);
	  debug = false;
#endif	  
	  
#if noCALIB	 
	  if (!VETO_OK) continue;  // "normal" VETO
	  //if (VETO_OK) continue; // inverted VETO for tests
#endif
	  cuts.cutVETO++;
	  hHCvsEC_VETOcuts->Fill(SumEC36, SumHC2);	  

          //===================================================================================
	  //===================================================================================	  

	  if (SumEC > 0) histECtotal->Fill(SumEC, 1);
	  if (SumEC36 > 0) histECtotal36->Fill(SumEC36, 1);
	  if (SumECbad > 0) histECtotalBad->Fill(SumECbad, 1);
	  	 
	  double EcalTotalEnergy = SumECtotal;
	  
	  if (debug) std::cout << "  EcalTotalEnergy = " << SumECtotal << "  InTime = "
			       << SumEC36 << "  bad = " << SumECbad << "  ECAL_OK = "
			       << ECAL_OK << std::endl;
#if noCALIB	 	  
	  if (!ECAL_OK) continue; // 17-may-2019
#endif
	  cuts.cutEcalBadEnergy++;
	  
	  hHCvsEC_ECcuts->Fill(SumEC36, SumHC2);

	  hHCratioGoodOverTotal->Fill(SumHC2/(SumHC2+SumHCbad2));
				      
#if noCALIB
	  if (!HCAL_OK) continue;
#endif
	  cuts.cutHcalBadEnergy++;
	  
	  /*if (HCAL_OK)*/ hHCvsEC_HCbadEnergyCuts->Fill(SumEC36, SumHC2);

	  // -------------------------------------------------------------------------

	  if (debug) std::cout << "  HcalEnergy(1,0) = " << SumHC2 << "  EcalEnergy = "
			       << SumEC36 << std::endl << std::endl;

	  na64xy0coord(cluster, ECx0, ECy0, debug5);

	  //===============================================================

          e.MMhits();

          double angle12_34(0);          
          double angle12(0);
	  double chi2(10000.);

          //Modification in reconstruction        
	  if (e.MM1.hasHit()) {
            mm1X->Fill(e.MM1.xpos);
            mm1Y->Fill(e.MM1.ypos);
            if (debug5) std::cout << " e.MM1: x = " << e.MM1.xpos << "  y = " << e.MM1.ypos
				  << std::endl;
            mm1X_time->Fill(e.MM1.xplane.bestHitTime());
            mm1Y_time->Fill(e.MM1.yplane.bestHitTime());
            mm1->Fill(e.MM1.xpos, e.MM1.ypos);
	  }
	  if (e.MM2.hasHit()) {
            mm2X->Fill(e.MM2.xpos);
            mm2Y->Fill(e.MM2.ypos);
            if (debug5) std::cout << " e.MM2: x = " << e.MM2.xpos << "  y = " << e.MM2.ypos
				  << std::endl;
            mm2X_time->Fill(e.MM2.xplane.bestHitTime());
            mm2Y_time->Fill(e.MM2.yplane.bestHitTime());
            mm2->Fill(e.MM2.xpos, e.MM2.ypos);
	  }
	  if (e.MM3.hasHit()) {
            mm3X->Fill(e.MM3.xpos);
            mm3Y->Fill(e.MM3.ypos);
            if (debug5) std::cout << " e.MM3: x = " << e.MM3.xpos << "  y = " << e.MM3.ypos
				  << std::endl;
            mm3X_time->Fill(e.MM3.xplane.bestHitTime());
            mm3Y_time->Fill(e.MM3.yplane.bestHitTime());
            mm3->Fill(e.MM3.xpos, e.MM3.ypos);
	  }
	  if (e.MM4.hasHit()) {
	    mm4X->Fill(e.MM4.xpos);
            mm4Y->Fill(e.MM4.ypos);
            if (debug5) std::cout << " e.MM4: x = " << e.MM4.xpos << "  y = " << e.MM4.ypos
				  << std::endl;
            mm4X_time->Fill(e.MM4.xplane.bestHitTime());
            mm4Y_time->Fill(e.MM4.yplane.bestHitTime());
            mm4->Fill(e.MM4.xpos, e.MM4.ypos);
	  }
	  if (e.MM5.hasHit()) {
            mm5X->Fill(e.MM5.xpos);
            mm5Y->Fill(e.MM5.ypos);
            if (debug5) std::cout << " e.MM5: x = " << e.MM5.xpos << "  y = " << e.MM5.ypos
				  << std::endl;
            mm5X_time->Fill(e.MM5.xplane.bestHitTime());
            mm5Y_time->Fill(e.MM5.yplane.bestHitTime());
            mm5->Fill(e.MM5.xpos, e.MM5.ypos);
	  }
	  if (e.MM6.hasHit()) {
            mm6X->Fill(e.MM6.xpos);
            mm6Y->Fill(e.MM6.ypos);
            if (debug5) std::cout << " e.MM6: x = " << e.MM6.xpos << "  y = " << e.MM6.ypos
				  << std::endl;
            mm6X_time->Fill(e.MM6.xplane.bestHitTime());
            mm6Y_time->Fill(e.MM6.yplane.bestHitTime());
            mm6->Fill(e.MM6.xpos, e.MM6.ypos);
	  }

	  debug5 = false;//true;//false;//true;
	  
	  // simple track reconstruction
	  // --------------- my tracking ------------------------------
	  //TVector3 mm3_hit, mm4_hit;
	  //const SimpleTrack track = simple_tracking_4MM(e, mm3_hit, mm4_hit);

	  // --------------- volkov's tracking -------------------------
	  const SimpleTrack track = simple_tracking_4MM(e);
	  // extrapolate MM3->4 line on ECAL surface
	  // TODO: current definition is valid only for 2016B geometry
	  //const TVector3 mm3_hit = e.MM3.abspos();
	  //const TVector3 mm4_hit = e.MM4.abspos();

	  //mm3_hit_x->Fill(mm3_hit.X());
	  //mm3_hit_y->Fill(mm3_hit.Y());
	  //mm4_hit_x->Fill(mm4_hit.X());
	  //mm4_hit_y->Fill(mm4_hit.Y());

	  bool Good_Track = false;
	  int sum_mm1 = 0;
	  for (int iStrip = 0; iStrip < MM_size; iStrip++)
	    {
	      sum_mm1 += (int)e.MM1.xplane.strips[iStrip];
	      sum_mm1 += (int)e.MM1.yplane.strips[iStrip];
	    }
	  
	  int sum_mm2 = 0;
	  for (int iStrip = 0; iStrip < MM_size; iStrip++)
	    {
	      sum_mm2 += (int)e.MM2.xplane.strips[iStrip];
	      sum_mm2 += (int)e.MM2.yplane.strips[iStrip];
	    }
	  
	  int sum_mm3 = 0;
	  for (int iStrip = 0; iStrip < MM_size; iStrip++)
	    {
	      sum_mm3 += (int)e.MM3.xplane.strips[iStrip];
	      sum_mm3 += (int)e.MM3.yplane.strips[iStrip];
	    }
	  
	  int sum_mm4 = 0;
	  for (int iStrip = 0; iStrip < MM_size; iStrip++)
	    {
	      sum_mm4 += (int)e.MM4.xplane.strips[iStrip];
	      sum_mm4 += (int)e.MM4.yplane.strips[iStrip];
	    }
	  
	  int sum_mm5 = 0;
	  for (int iStrip = 0; iStrip < MM_size; iStrip++)
	    {
	      sum_mm5 += (int)e.MM5.xplane.strips[iStrip];
	      sum_mm5 += (int)e.MM5.yplane.strips[iStrip];
	    }
	  
	  int sum_mm6 = 0;
	  for (int iStrip = 0; iStrip < MM_size; iStrip++)
	    {
	      sum_mm6 += (int)e.MM6.xplane.strips[iStrip];
	      sum_mm6 += (int)e.MM6.yplane.strips[iStrip];
	    }
	  	  
	  if (
	      //sum_mm1 < 50000 &&
	      //sum_mm2 < 50000 &&
	      //sum_mm3 < 50000 &&
	      //sum_mm4 < 50000 &&
	      sum_mm3 < 60000 &&
	      sum_mm4 < 60000 &&
	      //sum_mm5 < 50000 &&
	      //sum_mm6 < 50000 &&
	      //track.chi2/track.ndf < 3.0
	      track.chi2/track.ndf < 5.0
	      )
	    {
	      Good_Track = true;
	      //std::cout << "  VERY GOOD Track !!!!" << std::endl;
	    }
	  
	  
	  //if (debug5) std::cout << "  track.isDefined = " << track.isDefined << std::endl;
	  //#if noCALIB	 	  
	  //if (!track.isDefined) continue; // temporary
	  //#endif
	  if (!Good_Track) continue;

	  besttrack = true;
	  cuts.cutMMgoodTrack++;

	  const double mom = track.momentum;	    
	  momentum->Fill(mom);

	  //if (debug5) {
	  //  std::cout << "  mm3_hit.x = " << mm3_hit.X() << "  mm3_hit.y = " << mm3_hit.Y();
	  //  std::cout << "  mm4_hit.x = " << mm4_hit.X() << "  mm4_hit.y = " << mm4_hit.Y()
	  //            << std::endl;
	  //}
	  
	  //const TVector3 P_ecal = extrapolate_line(mm3_hit, mm4_hit, ECAL_pos.pos.Z());

	  //if (debug5) cout << "  ecal track x = " << P_ecal.X() << " y = " << P_ecal.Y() << endl;
	  if (debug5) cout << "  ecal track x = " << track.ecalhit.X() << " y = "
			   << track.ecalhit.Y() << endl;

	  ECAL_pos_mom->Fill(track.ecalhit.X(), track.ecalhit.Y());
	  //ECAL_pos_mom->Fill(P_ecal.X(), P_ecal.Y());

	  // ---------------- Volkov's tracking	---------------------------------------  
	  //double X_corr = -402.4;//-405.5; //-391.5;// -391.2; // -391.5
	  //double Y_corr = 0;
	  //if (header.RunNumber < 4095) {
	  //  Y_corr = -0.22; //1.;// 9.3;    // 1.
	  //} else {
	  //  Y_corr = 2.91;
	  //}
	  //---------------------------------------------------------------------------
	  // ---------------- my tracking ---------------------------------------------  
	  double X_corr = -391.3; //-388.5;//-391.5;// -391.2; // -391.5
	  double Y_corr = 0.12; // 0.74; //1.2;//1.;
	  //if (header.RunNumber < 4095) {
	  //  Y_corr = -0.22; //1.;// 9.3;    // 1.
	  //} else {
	  //  Y_corr = 2.91;
	  //}
	  //---------------------------------------------------------------------------
	  
	  //if (debug5) std::cout << "\n  mom = " << mom << " ecal track x = " << P_ecal.X()
	  //			<< " y = " << P_ecal.Y() << endl;
	  if (debug5) std::cout << "\n  mom = " << mom << " ecal track x = " << track.ecalhit.X()
				<< " y = " << track.ecalhit.Y() << endl;

	  
	  //if (debug5) std::cout << "  DeltaX = " << (P_ecal.X() - X_corr)
	  //			<< "  DeltaY = " << (P_ecal.Y() - Y_corr)
	  //			<< std::endl;

	  if (debug5) std::cout << "  DeltaX = " << (track.ecalhit.X() - X_corr)
				<< "  DeltaY = " << (track.ecalhit.Y() - Y_corr)
				<< std::endl;

	  if (debug5) std::cout << track << std::endl;
	  
	  //ECAL_pos_mom->Fill(P_ecal.X() - 68.84, P_ecal.Y() + 33.47);
	  //===============================================================
	  const double mrad = 1e-3; 

 	  Ecal_pos_X->Fill(track.ecalhit.X());
	  Ecal_pos_Y->Fill(track.ecalhit.Y());
          	  
	  //const double mrad = 1e-3; 
	  angle12 = track.in_theta/mrad;
	  angle12_34 = track.in_out_angle/mrad;
	  chi2 = track.chi2; // only for Volkov's tracking

	  mom_chi2->Fill(chi2); // only for Volkov's tracking
	  
	  if (debug5) std::cout << " Angle12_34 = " << angle12_34 << "  angle12 = " << angle12
				<< "  chi2 = " << chi2 << std::endl;
				  
	  hAngle12->Fill(angle12);
          
	  float x0EC = track.ecalhit.X() - X_corr;  
	  float y0EC = track.ecalhit.Y() - Y_corr;  
	  float R0 = sqrt(x0EC*x0EC + y0EC*y0EC);
	  
	  EcalX0MM->Fill(x0EC);
	  EcalY0MM->Fill(y0EC);
          
	  // coordinates in the central cell of shower
	  na64xy0coord(cluster, ECx0, ECy0, debug5);

	  EcalX0->Fill(ECx0);
	  EcalY0->Fill(ECy0);
	  	  
	  //float R0 = sqrt(ECx0*ECx0 + ECy0*ECy0);
	  	  
	  if (debug5) {
	    std::cout << "MM:  x0EC = " << x0EC << "  y0EC = " << y0EC;// << std::endl;
	    std::cout << "  EC:  ECx0 = " << ECx0 << "  ECy0 = " << ECy0 << std::endl;
	  
	    std::cout << "  ------------ Ecal cluster -----------" << std::endl;
	    std::cout << "  ECx0 = " << ECx0 << "  ECy0 = " << ECy0 << std::endl;
	    for (int j = 0; j < 3; j++) {
	      for (int i = 0; i < 3; i++) 
	        printf(" %7.2f", cluster[i][2-j]);
	      printf("\n");
	    }	    
          }
          debug5 = false;

          /*if (HCAL_OK && besttrack)*/ hHCvsEC_GoodTrack->Fill(SumEC36, SumHC2);
	  

	  double ECratio(0);
#if PROFILE
	  double A[3][3];
	  
	  double Sum9Cal = 0;
	  for (int j = 0; j < 3; j++) 
	    for (int i = 0; i < 3; i++) {
	      Sum9Cal += cluster[i][2-j];
	  }

	  ECratio = SumEC36 > 0 ? (SumEC36-Sum9Cal)/SumEC36 : 0;
	  //ECratio = SumECtotal > 0 ? (SumECtotal-Sum9Cal)/SumECtotal : 0;
	  histRatio->Fill(ECratio);
	  hRatioVsEnergy->Fill(SumEC36, ECratio);
	  double ECdiff = SumEC36 > 0 ? (SumECtotal-SumEC36)/SumEC36 : 0;
	  //hECdiffOverECintime->Fill(ECdiff);
	  
	  float Ax[3], Ay[3];
	  for (int j = 0; j < 3; j++) {
	    Ay[2-j] = 0;
	    for (int i = 0; i < 3; i++) {
	      Ay[2-j] += cluster[i][2-j];
	    }
	  }
	  for (int i = 0; i < 3; i++) {
	    Ax[i] = 0;
	    for (int j = 0; j < 3; j++) {
	      Ax[i] += cluster[i][2-j];
	    }
	  }

	  if (Ay[0]-Ay[2] < 1.) Ycenter->Fill(track.ecalhit.Y());
	  if (Ax[0]-Ax[2] < 1.) Xcenter->Fill(track.ecalhit.X());
	  

	  bool mydebug5 = false;
	  
	  if (mydebug5) {
	    std::cout << "  ----------------  new  event  -------------------" << std::endl;
	    std::cout << " ---- x0 = " << x0EC << " y0 = " << y0EC << std::endl;
	    std::cout << "  Original shower: " << std::endl;
	    
	    for (int j = 0; j < 3; j++) {
	      for (int i = 0; i < 3; i++) {
		printf("  %6.2f ", cluster[i][2-j]);
	      }
	      printf("\n");
	    }

	    printf("  Ay[0] = %6.2f, Ay[1] = %6.2f, Ay[2] = %6.2f\n", Ay[0], Ay[1], Ay[2]);
	    printf("  Ax[0] = %6.2f, Ax[1] = %6.2f, Ax[2] = %6.2f\n", Ax[0], Ax[1], Ax[2]);
	  }	  


	  shower.getShower(x0EC, y0EC, Sum9Cal, *A);
	  //shower.getShower(ECx0, ECy0, Sum9Cal, *A);
	  
	  double chisq = 0;
	  for (int j = 0; j < 3; j++) {
	    for (int i = 0; i < 3; i++) {
	      if (cluster[i][2-j] > 0) chisq += pow((cluster[i][2-j] - A[2-j][i]),2)/
					 cluster[i][2-j];
	    }
	  }
	  shchisq->Fill(chisq);
	  
	  if (mydebug5) {
	    std::cout << "  Predicted shower sum9cal = " << Sum9Cal << std::endl;
	    for (int j = 0; j < 3; j++) {
	      for (int i = 0; i < 3; i++) {
		printf("  %6.2f ", A[2-j][i]);
	      }
	      std::cout << std::endl;
	    }
	    std::cout << "  ChiSq = " << chisq << std::endl;
	  }
	  mydebug5 = false;
	  
	  if (chisq < 8.) CHI_SQ = true;  
#endif // end if PROFILE

#if noCALIB	 	  
	  if (!CHI_SQ) continue;
#endif
	  hHCvsEC_ChiSQcuts->Fill(SumEC36, SumHC2);
	  cuts.cutChiSQ++;

	  //if (ECratio < 0.055) EC_RATIO = true;


// VAR 1: decay of A' in HC1, VAR 2: decay of A' in HC1 || HC2, VAR 3: decay of A' in HC2
	  
	  double hcratio1 = 0, hcratio2 = 0, hc12energyratio = 0;
	  // for Var 1
	  // For HC1: Ratio = (HCtotal - HCcenter) / HCtotal
	  double HCin1 =  HCclusters[1][1][1], HCtot1 = 0;
	  for (int i = 0; i < 3; i++) {
	    for (int j = 0; j < 3; j++) {
	      HCtot1 += HCclusters[1][i][j];
	    }
	  }
	  if (HCin1 > 0 && HCtot1 > 0) hcratio1 = (HCtot1 - HCin1) / HCtot1;
	  
	  // for VAR 3
	  // For HC2: Ratio = (HCtotal - HCcenter) / HCtotal
	  double HCin2 =  HCclusters[2][1][1], HCtot2 = 0;
	  for (int i = 0; i < 3; i++) {
	    for (int j = 0; j < 3; j++) {
	      HCtot2 += HCclusters[2][i][j];
	    }
	  }
	  
	  if (HCin2 > 0 && HCtot2 > 0) hcratio2 = (HCtot2 - HCin2) / HCtot2;

	  hc12energyratio = HCtot1 + HCtot2 > 0 ? HCtot2/(HCtot1+HCtot2) : 0;

	  // print hcratios
	  //std::cout << "  HCratio1 = " << hcratio1 << ",  HCratio2 = " << hcratio2 
	  //	      << ",  HC12EnergyRatio = " << hc12energyratio << std::endl;
										      
	  //if (hcratio1 > 0.05 || hcratio2 > 0.05) EC_RATIO = true;
	  
#if noCALIB	  
	  //if (!EC_RATIO) continue;
	  //if (EC_RATIO) continue; // 10-nov-2019
#endif
	  //cuts.cutECratio++; //10-nov-2019
	  //hHCvsEC_ECratiocuts->Fill(SumEC36, SumHC2);	  
	  //hHCvsEC_HCratioCuts->Fill(SumEC36, SumHC2); // 10-nov-2019	  

	  //if (SumHC2 < 20.) continue; // dimuons cut, Var 1, 2, 3; 7
	  //if (SumHC2 < 30.) continue; // dimuons cut Var 4, 5, 6;
	  //if (eHC_muon[2] < 15.) continue;
	  //if (eHC_muon[2] < 15. && eHC_muon[1] < 15.) continue;
	  if (eHC_muon[1] < 15.) continue;

	  
	  cuts.cutDimuons++;
	  hHCvsEC_DimuonsCut->Fill(SumEC36, SumHC2);	  

	  HCratio1->Fill(hcratio1);
	  HCratio2->Fill(hcratio2);
	  HC12energyRatio->Fill(hc12energyratio);


	  //if (hc12energyratio > 0.05) continue; // cut ratio = HC2/(HC1+HC2)
	  
      	  cuts.cutHC12ratio++;
	  hHCvsEC_HC12ratioCut->Fill(SumEC36, SumHC2);	  
	  
	  if (abs(mom-100.) < 5.) MM_OK = true;

#if noCALIB	 	  
	  //if (!MM_OK) continue; // 17-may-2019
#endif	  
	  cuts.cutMMmomentum++;
	  
	  hHCvsEC_MOMcuts->Fill(SumEC36, SumHC2);

	  int ixmax = -1, iymax = -1;
	  float Amax = 0, sumHC = 0;
	  int ncells1 = 0, ncells2 = 0;
	  int k = 0;
	  for (int j = 2; j >= 0; j--) {
	    //for (int k = 0; k < 4; k++) {
	    for (int i = 0; i < 3; i++) {
	      sumHC += HCclusters[k][i][j];
	      if (HCclusters[k][i][j] > 0.2) ncells1++;
	      if (HCclusters[k][i][j] > 0.5) ncells2++;
	      /////if (HCclusters[k][i][j] > 0.4) ncells2++;
	      //if (HCclusters[k][i][j] > 0.3) ncells2++; // 11-Dec-2018
	      if (HCclusters[k][i][j] > Amax) {
		ixmax = i;
		iymax = j;
		Amax = HCclusters[k][i][j];
	      }
	    }
	    //}
	  }
	  
	  histHCtotal->Fill(SumHC, 1);
	  histHCtotal2->Fill(SumHC2, 1);
	  
	  // Muons cut
	  if (eHC_muon[2] < 2.) HC2_MUON = true;  // Muons cut
	  //if (!HC2_MUON) continue; 
	  cuts.cutHCmuon++;
	  
	  hHCvsEC_HCmuonCut->Fill(SumEC36, SumHC2);

	  // special plots of R = (HCtotal - HC11) / HCtotal
	  //------------------------------------------------	  
	  float RHC0outOverHC0total = SumHC0 > 0. ? (SumHC0-SumHC0_11)/SumHC0 : 0.;
	  
	  // HC cuts
	  //HC_PASS1 = ncells1 >= 2 && sumHC-HCclusters[0][1][1] > HCclusters[0][1][1];
	  //if (HC_PASS1) continue; 
	  
	  HC_PASS2 = ncells2 >= 1 && sumHC-HCclusters[0][1][1] > HCclusters[0][1][1];
	  //if (HC_PASS2) continue; 

	  cuts.cutHCpass1++;
	  hHCvsEC_HCpassCut->Fill(SumEC36, SumHC2);
	  
	  if (SumSRDcal < 120.) SRD_SUM = true;
	  //if (SumSRDcal < 110.) SRD_SUM = true;
	  //if (!SRD_SUM) continue; 
	  cuts.cutSRDsum++;
	  hHCvsEC_SRDsum->Fill(SumEC36, SumHC2);

	  if (angle12 < 2.) Angle12_cut = true;
	  //if (!Angle12_cut) continue; // 17-may-2019
	  cuts.cutAngle12++;
	  hHCvsEC_Angle12cut->Fill(SumEC36, SumHC2);
	  
	  //if (eHC_muon[3] < 1.5) HC3_MUON = true;
	  if (eHC_muon[3] < 3.5) HC3_MUON = true;  // 16-Dec-2018
	  //if (eHC_muon[3] < 5.0) HC3_MUON = true;  // 16-Dec-2018
	  //if (!HC3_MUON) continue; 
	  cuts.cutHC3++;
	  hHCvsEC_HC3cut->Fill(SumEC36, SumHC2);

	  //if (R0 > 12.5) R0_Cut = true;
	  if (R0 > 20.) R0_Cut = true;
	  //if (R0_Cut) continue; // 17-may-2019
	  cuts.cutR0++;
	  hHCvsEC_R0cut->Fill(SumEC36, SumHC2);
	  	  
	  // OOT ratio cut 
	  //if (ECdiff > 0.028) continue; // 11-Dec-2018
	  cuts.cutOOTratio++;
	  hHCvsEC_OOTratioCut->Fill(SumEC36, SumHC2);

	  hHCvsDelta->Fill(SumEC36-100., SumHC2);

	  if ((SumHC2 > 2.5 && SumHC2 < 15.) && (SumEC36 > 0. && SumEC36 < 55.)) {
	    hSumVETO01sel->Fill(sumVETO01, 1);
	    hSumVETO23sel->Fill(sumVETO23, 1);
	    hSumVETO45sel->Fill(sumVETO45, 1);
	    //hSumVETO01sel->Fill(veto01, 1);
	    //hSumVETO23sel->Fill(veto23, 1);
	    //hSumVETO45sel->Fill(veto45, 1);
	    hHC0selected->Fill(eHC_muon[0]);
	    hHC1selected->Fill(eHC_muon[1]);
	    hHC2selected->Fill(eHC_muon[2]);
	    hHC3selected->Fill(eHC_muon[3]);
	    std::cout << " RunNumber = " << header.RunNumber << "  EventNumber = "
		      << header.EventNumber << "  BurstNumber = " << header.BurstNumber
		      << "  NumberInBurst = " << header.NumberInBurst << std::endl;
	    hTimeSelected->Fill(trig[0]); 
	  }
	  	  
	  cuts.cutECnondiag++;
	  hHCvsEC_ECnondiagCut->Fill(SumEC36, SumHC2);
	    
	  if (SumEC36 > 0) histECtotal36afterCuts->Fill(SumEC36, 1);
	  
	  //ECAL_pos_mom_1->Fill(P_ecal.X(), P_ecal.Y());
	  ECAL_pos_mom_1->Fill(track.ecalhit.X(), track.ecalhit.Y());
		  
	  //if (!VETO2_OK) hHCvsEC_AllCutsAndV2cut->Fill(SumEC36, SumHC2);
	  
	  if (SumEC36 > 0) momVsEC->Fill(SumEC36, mom);
	  	  	  
#if OUTPUT //=====================================================================================
	  ///*if (((SumHC2 >= 0 && SumHC2 < 5) && (SumEC36 >= 0 && SumEC36 < 60)))*/ {
	  //if ((SumHC2 > 5 && SumHC2 < 20) && (SumEC36 > 0. && SumEC36 < 60)) {
	  if (SumHC2 < 20) {

            hEnergyHC3->Fill(eHC_muon[3]); 
	    
	    EcalXbad->Fill(x0EC);
	    EcalYbad->Fill(y0EC);
 	    EcalXYbad->Fill(x0EC, y0EC);
	    
	    double SRDsum = SumSRDcal; 
	    
	    std::cout << " -------------------------------------------------------------------- "
		      << std::endl;
	    std::cout << " Run = " << header.RunNumber << " Event = " << header.EventNumber
		      << "  BurstNumber = " << header.BurstNumber << " NumberInBurst = "
		      << header.NumberInBurst << std::endl << std::endl;
	    
	    printf(" Ecal =%6.2f   ECtotal =%6.2f\n", SumEC36, SumECtotal);
	    printf(" Veto = %5.2f, %5.2f, %5.2f, %5.2f, %5.2f, %5.2f\n", veto[0], veto[1],
		   veto[2], veto[3], veto[4], veto[5]);
	    printf(" Hcal(0+1) =%6.2f,  HCtotal(0+1+2) =%6.2f\n", SumHC2, SumHC012);
	    printf(" SRD1 =%6.2f,  SRD2 =%6.2f,  SRD3 =%6.2f,   SRDsum =%7.2f\n", SRDcal012[0],
		   SRDcal012[1], SRDcal012[2], SRDsum);

	    printf(" Angle12 = %5.2f, Angle12_34 = %5.2f, Momentum = %6.2f,",
		   angle12, angle12_34, mom);
	    printf("  mom_Chi2 = %5.2f\n", chi2);
	    printf(" sum_MM1 = %5d, sum_MM2 = %5d, sum_MM3 = %5d, sum_MM4 = %5d,"
		   " sum_MM5 = %5d, sum_MM6 = %5d\n", sum_mm1, sum_mm2, sum_mm3, sum_mm4,
		   sum_mm5,  sum_mm6);   
	    
	    printf(" ShowerChiSQ =%5.2f, Ratio((EC6x6-EC3x3)/EC6x6) = %5.3f, OOTratio = %5.3f\n",
		   chisq, ECratio, ECdiff);
	    
	    char outstring[100];
	    printf("\n --- MM projection: x0 =%6.2f, y0 =%6.2f, R0 =%6.2f\n", x0EC, y0EC, R0);

	    printf("  Original shower with Sum9Cal =%6.2f:", Sum9Cal);
	    std::cout << "        Predicted shower: " << std::endl;

	    for (int j = 0; j < 3; j++) {
	      for (int i = 0; i < 3; i++) {
		sprintf(outstring, "  %6.2f ", cluster[i][2-j]);
		std::cout << outstring;
	      }
	      std::cout << "                  ";
	      
	      for (int i = 0; i < 3; i++) {
		sprintf(outstring, "  %6.2f ", A[2-j][i]);
		std::cout << outstring;
	      }
	      std::cout << std::endl;
	      
	    }

#if 1
	    Cell cell1;
	    for (auto& it : e.ECcells) {
	      cell = it;
	      if (cell.plane == 1) {
		for (auto& it1 : e.ECcells) {
		  cell1 = it1;
		  if (cell1.plane == 0) {
		    if (cell.ix == cell1.ix && cell.iy == cell1.iy) {
		      //if (cell1.signalcal > 0 && cell1.signalcal < 200.) {
		      if (cell1.signalcal > 0) {
			it.signalcal += cell1.signalcal;
		      }
		    }
		  }
		}
	      }
	    }
#endif		    
	    //PlotECplane(e.ECcells, 0);
	    PlotECplane(e.ECcells, 1);
	    

	    std::cout << "  Hcal planes 0,1,2,3 (in-time energies):" << std::endl;	    
	    //HCclusters[ipl][i][j];
	    for (int j = 2; j >= 0; j--) {
	      for (int k = 0; k < 4; k++) {
		for (int i = 0; i < 3; i++) 
		  printf("%7.2f", HCclusters[k][i][j]);
		printf(" |");
	      }
	      printf("\n");
	    }
	    
	    std::cout << "  HC0: ncells1 = " << ncells1 << "  ncells2 = " << ncells2
		      << "  MaxCell = " << Amax << "(" << ixmax << "," << iymax
		      << ")  EnergyHC - E(1,1) = " << sumHC-HCclusters[0][1][1]
		      << "  pass1 = " << HC_PASS1 << "  pass2 = " << HC_PASS2 << std::endl;
	    std::cout << "  HCratio1 = " << hcratio1 << ",  HCratio2 = " << hcratio2 << std::endl;
	    
	    printf("\n");
	  }	      
	   	    
#endif // if OUTPUT    
	  
	  //==================================================================================

	  if (event_errors > 0)
	    {
	      DaqErrors &er = manager.GetEvent().GetDaqErrors();
	      // print event errors
	      printf("Event errors: total=%zu  problems(NO,WARN,ERR,CRIT)=(%zu,%zu,%zu,%zu)"\
		     "  actions(NO,GEO,SRC,EVENT)=(%zu,%zu,%zu,%zu)\n",
		     er.errors.size(),
		     er.errors_level[DaqError::OK              ].size(),
		     er.errors_level[DaqError::MINOR_PROBLEM   ].size(),
		     er.errors_level[DaqError::WARNING         ].size(),
		     er.errors_level[DaqError::SEVERE_PROBLEM  ].size(),
		     er.errors_action[DaqError::Action::NOTHING                ].size(),
		     er.errors_action[DaqError::Action::DISCARD_EVENT_ON_GEOID ].size(),
		     er.errors_action[DaqError::Action::DISCARD_EVENT_ON_SRCID ].size(),
		     er.errors_action[DaqError::Action::DISCARD_EVENT          ].size());
	      
	      if (event_errors > 1)
		{
		  // printf("Sorry, not implemented yet. 
		  // Send a mail to Zvyagin.Alexander@gmail.com\n");
		  printf("Errors sorted by LEVEL:\n");
		  printf("==> Error level:  NO ERROR: %zu errors\n",
			 er.errors_level[DaqError::OK              ].size());
		  er.Print(DaqError::OK);
		  printf("==> Error level:  MINOR PROBLEM: %zu errors\n",
			 er.errors_level[DaqError::MINOR_PROBLEM   ].size());
		  er.Print(DaqError::MINOR_PROBLEM);
		  printf("==> Error level:  WARNING: %zu errors\n",
			 er.errors_level[DaqError::WARNING         ].size());
		  er.Print(DaqError::WARNING);
		  printf("==> Error level:  SEVERE_PROBLEM: %zu errors\n",
			 er.errors_level[DaqError::SEVERE_PROBLEM  ].size());
		  er.Print(DaqError::SEVERE_PROBLEM);
		  
		  printf("\nErrors sorted by ACTION:\n");
		  printf("==> Error action:  NO ACTION: %zu errors\n",
			 er.errors_action[DaqError::Action::NOTHING].size());
		  er.Print(DaqError::Action::NOTHING);
		  printf("==> Error action:  DISCARD_EVENT_ON_GEOID: %zu errors\n",
			 er.errors_action[DaqError::Action::DISCARD_EVENT_ON_GEOID].size());
		  er.Print(DaqError::Action::DISCARD_EVENT_ON_GEOID);
		  printf("==> Error action:  DISCARD_EVENT_ON_SRCID: %zu errors\n",
			 er.errors_action[DaqError::Action::DISCARD_EVENT_ON_SRCID].size());
		  er.Print(DaqError::Action::DISCARD_EVENT_ON_SRCID);
		  printf("==> Error action:  DISCARD_EVENT: %zu errors\n",
			 er.errors_action[DaqError::Action::DISCARD_EVENT].size());
		  er.Print(DaqError::Action::DISCARD_EVENT);
		  
		  printf("- - - - - - -    End of event errors report   - - - - - -\n\n");
		}
	    }

	} // end of ReadEvent
      
      //------------------------------------------------

      std::cout << "  Are going to write histograms !" << std::endl;
      
      //  Write output histograms into file
      for (auto& it1 : histo1F) it1->Write();
      for (auto& it2 : histo2F) it2->Write();
      for (auto& it3 : profile) it3->Write();
      
      //-----------------------------------------------------------------
      std::vector<Cell> eccells;      
      // fill vector or signals: (Mean +/- Rms) for each cell (in 2 planes)
      for (int ipl = 0; ipl < 2; ipl++) {  // loop over 2 planes of EC
	for (int ix = 0; ix < 6; ix++) { // loop over ix
	  for (int iy = 0; iy < 6; iy++) { // loop over iy
	    
	    TH1F *h11 = histECP[ipl][ix][iy]; 
	    float mean = h11->GetMean();
	    float rms = h11->GetRMS();
	    
	    cell.plane = ipl;
	    cell.ix = ix;
	    cell.iy = iy;
	    cell.signalcal = mean;
	    //cell.signalcal = rms;
	    eccells.push_back(cell);
	    
	  }
	}
      }
      
     //PlotECplane(eccells, 0);
      PlotECplane(eccells, 1);
      
      std::cout << cuts;
      std::cout << std::endl;
      
      //----------------------------------------
      
      if (errors_list)
	CS::Exception::PrintStatistics();
      
      if (chips_stat)
	CS::Chip::PrintStatistics();
      
      if (stat_TT)
	{
	  manager.GetDaqOptions().GetStatTT().Print();
	  
	  int events_max = 50;
	  printf("Limiting statics to %d events.\n",events_max);
	  manager.GetDaqOptions().GetStatTT().SetBufferSize(events_max);
	  manager.GetDaqOptions().GetStatTT().Print();
	}
      
      //tree->Write(); // Write information to RootTree
      
      printf("\n\nEnd of the decoding!\n\n");
    }

  catch(const char *e)
    {
      printf("%s\n",e);
      error = 1;
    }
  catch(const std::exception &e)
    {
      printf("%s\n",e.what());
      error = 1;
    }
  catch(...)
    {
      printf("Unknown exception.\n");
      error = 1;
    }
  
  hfile->Close();
  delete hfile;

  
  return error;
}
//========================================================================================
//////////////////////////////////////////////////////////////////////////////////////////
