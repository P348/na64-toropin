#include <iostream>
#include <iomanip>

#include <stdlib.h>
#include <string>
#include <cmath>
#include <algorithm>
#include <vector>
#include <string>

#include <cstdio>
#include <exception>
#include <string>
#include <set>
#include <fstream>

#define max_spill	200

#define ec_col		6
#define ec_row		6
#define ec_sec		2
#define ec_cells	(ec_col * ec_row * ec_sec)
//#define ec_cells	72 //(ec_col * ec_row * ec_sec)

#define hc_col		3
#define hc_row		3
#define hc_sec		4
#define hc_cells	(hc_col * hc_row * hc_sec)
//#define hc_cells	36 //(hc_col * hc_row * hc_sec)

#define wc_cells	3
#define sum_cells	2

#define ec_loc		0
#define hc_loc		(ec_loc+ec_cells)
#define wc_loc		(hc_loc+hc_cells)
#define sum_loc		(wc_loc+wc_cells)

#define led_cells	(ec_cells+hc_cells+wc_cells+sum_cells)

static int last_run = -1;
static int last_spill = -1;
static bool noLedFile = false;
static bool noLedCorr = true;

int  spillled[max_spill];

float locLed[max_spill][sum_loc];

static struct LedCell {
  int ledstat;
  float Led;
} ledcell[led_cells];

int ec_zxy2lin(int z, int x,int y) {
  return ((z*ec_col + x) * ec_row) + y;
}

int hc_zxy2lin(int z, int x,int y) {
  return ((z*hc_col + x) * hc_row) + y;
}

//===================================================================

void Init_LEDCorrection(int runNb) {
  
  FILE *spills_leds;
  char leds_filenm[150];
  char line[1000];
  char calibrsDir[] = "../2018.xml/";

  int lind, ind, stat, run, spill, date, year, lastSpill = 0;
  size_t  lline = 1000;
  long timest;
  char day[4], month[4], hms[10];
  char * linep = line;
  noLedCorr = true;
  noLedFile = false;
  //sprintf(leds_filenm, "%s%s", calibrsDir, "spills_leds");
  sprintf(leds_filenm, "%s%s", calibrsDir, "ledspill-2018-v3.txt");
  if (!(spills_leds = fopen (leds_filenm, "r"))) {
    printf("can't open leds file %s, leds correction for selected data not available\n",leds_filenm);
    noLedFile = true;
    return;
  }

  while (true) {
    linep = line;
    lind = getline(&linep, &lline, spills_leds);
    
    //printf("%i %s\n", lind, line);   

    ind = sscanf(line,"stat %d RUN %i Spill %i Time %ld %s %s %d %s %d\n",&stat,&run,&spill,
		 &timest, day, month, &date, hms, &year);
    if (ind == EOF) break;
    if (ind != 9) {
      continue;
    }
    if (run > runNb) break;
    if (run != runNb) {
      linep = line;
      ind = getline(&linep, &lline, spills_leds);
      continue;
    }
    if (spill < 1 || spill > max_spill) continue;
    spillled[spill-1] = stat;
    for (int loc = 0; loc < sum_loc; loc++) {
      ind = fscanf(spills_leds, "%f", &locLed[spill-1][loc]);
    }
    lastSpill = spill;
    if (ind == 1) noLedCorr = false;
    fscanf(spills_leds,"\n");
  }
  fclose(spills_leds);
  if (!noLedCorr) {
    std::cout << "for RUN " << runNb << "  " << lastSpill << " spills with LEDs present" << std::endl;
  }
}

#if 0
//==========================================================================
int main() {
  int runNb = 3809;
  int spillNb = 2; 

  if (runNb != last_run)
    {
      //printf("Start processing RUN %d ",runNb); 
      Init_LEDCorrection(runNb);
      last_run = runNb;
      last_spill = 0;
    }
  
  if (spillNb != last_spill)
    {
      last_spill = spillNb;
      if (noLedFile) {
	for (int loc = ec_loc; loc < led_cells; loc++) {
	  ledcell[loc].ledstat = 0;
	}
      } else if (!noLedCorr) {
	for (int loc = ec_loc; loc < sum_loc; loc++) {
	  ledcell[loc].ledstat = spillled[spillNb-1];
	  ledcell[loc].Led = locLed[spillNb-1][loc];
	}
      }	       
    }
}
#endif
