#include <iostream>
#include <iomanip>

#include <stdlib.h>
#include <string>
#include <math.h>
#include <cmath>
#include <algorithm>
#include <vector>
#include <stdio.h>
#include <string>

#include <cstdio>
#include <exception>
#include <string>
#include <set>
#include <popt.h>
#include <dlfcn.h>
#include <fstream>

#include "config.h"
#include "utils.h"
#include "DaqEventsManager.h"
#include "ChipSADC.h"
#include "ChipAPV.h"
#include "ChipNA64TDC.h"

#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TInterpreter.h"
#include "TSystem.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TProfile.h"
#include "TVector3.h"
#include "TProfile2D.h"
#include "TGraph2D.h"

#if USE_RFIO
#include <shift.h>
#endif

using namespace std;
using namespace CS;

#include "conddb.h"
#include "mm.h"

#define WCATCHER 0

//========================================================================================
struct Header {
public:
  Header(): RunNumber(0), EventNumber(0), BurstNumber(0), NumberInBurst(0) { };
  ~Header() { };
  
  bool operator == (const Header &a) { return RunNumber == a.RunNumber &&
      EventNumber == a.EventNumber && BurstNumber == a.BurstNumber &&
      NumberInBurst == a.NumberInBurst; }

public:  
  int RunNumber;
  int EventNumber;
  int BurstNumber;
  int NumberInBurst;
};


struct BadBurst {
public:
  BadBurst(): run(0), burst(0) { };
  BadBurst(int arun, int aburst) {
    run = arun;
    burst = aburst;
  };
  ~BadBurst() { };

  bool operator == (const BadBurst &a) { return run == a.run && burst == a.burst; }
  
public:
  int run;
  int burst;
};


//========================================================================================
struct Cuts {
public:
Cuts(): nTotal(0), cutStraw(0), cutT2time(0), cutEC33(0), cutSRD(0), cutVETO(0), 
    cutEcalBadEnergy(0), cutHcalBadEnergy(0), cutMMgoodTrack(0), cutChiSQ(0), cutDimuons(0), 
    cutHC12ratio(0), cutMMmomentum(0), cutHCmuon(0), cutHCpass1(0), cutSRDsum(0), cutAngle12(0),
    cutHC3(0), cutR0(0), cutOOTratio(0), cutECnondiag(0)
  { };
  ~Cuts() { };
public:
  int nTotal;
  int cutStraw;
  int cutT2time;
  int cutEC33;
  int cutSRD;
  int cutVETO;
  int cutEcalBadEnergy;
  int cutHcalBadEnergy;  
  int cutMMgoodTrack;
  int cutChiSQ;
  int cutDimuons; 
  int cutHC12ratio;
  int cutMMmomentum;
  int cutHCmuon;
  int cutHCpass1;
  int cutSRDsum;
  int cutAngle12;
  int cutHC3;
  int cutR0;
  int cutOOTratio;
  int cutECnondiag;
};

ostream &operator << (ostream &os, const Cuts &cuts)
{
  os << "  Ntotal = " << cuts.nTotal
     << "  cutStraw = " << cuts.cutStraw
     << "  T2time = " << cuts.cutT2time
     << "  cutEC33 = " << cuts.cutEC33
     << "  cutSRD = " << cuts.cutSRD
     << "  cutVETO = " << cuts.cutVETO
     << "  cutEcalBadEnergy = " << cuts.cutEcalBadEnergy
     << "  cutHcalBadEnergy = " << cuts.cutHcalBadEnergy
     << "  cutMMgoodTrack = " << cuts.cutMMgoodTrack
     << "  cutChiSQ = " << cuts.cutChiSQ
     << "  cutDimuons = " << cuts.cutDimuons 
     << "  cutHC12ratio = " << cuts.cutHC12ratio
     << "  cutMMmomentum = " << cuts.cutMMmomentum
     << "  cutHCmuon = " << cuts.cutHCmuon
     << "  cutHCpass1 = " << cuts.cutHCpass1
     << "  cutSRDsum = " << cuts.cutSRDsum
     << "  cutAngle12 = " << cuts.cutAngle12
     << "  cutHC3 = " << cuts.cutHC3
     << "  cutR0 = " << cuts.cutR0
     << "  cutOOTratio = " << cuts.cutOOTratio
     << "  cutECnondiag = " << cuts.cutECnondiag;
  return os;
}

//========================================================================================
struct meanPed {
public:

  bool operator == (const meanPed &a) { return pedstat == a.pedstat && Ped0 == a.Ped0
      && Ped1 == a.Ped1; }
  bool operator != (const meanPed &a) { return pedstat != a.pedstat || Ped0 != a.Ped0
      || Ped1 != a.Ped1; }
    
public:
  int pedstat;       // number of pedestals to calculate <ped>  
  float Ped0;        // <ped> even
  float Ped1;        // <ped> odd
};


//========================================================================================
struct Cell {
public:
  Cell (): plane(-1), ix(-1), iy(-1), intime(false), ped0(0), ped1(0), signal(0),
    signalcal(0), time(0), deltaT(0), timeCal(0), used(0) { };
  ~Cell () { };

public:
  int plane;       // 
  int ix;          //
  int iy;          //
  bool intime;     //
  float ped0;      // pedestal for even pulse
  float ped1;      // pedestal for odd pulse
  float signal;    // signal;
  float signalcal; // signal calibrated;
  float time;      // time calculated using pulse rise time
  float deltaT;    // time between T2 and given hit
  float timeCal;   // position of signal during calibration
  meanPed ped;     // mean values of pedestals
  int used;        // is 1 if cell was used 
};


//========================================================================================
struct CellReco {
public:
CellReco(): plane(-1), ixMax(-1), iyMax(-1), secondhit(0), Amax(0), time(0), sum9(0),
    sum9cal(0), sum36(0), sum36cal(0), badEnergy(0), totalEnergy(0) {};
public:
  int plane;
  int ixMax;
  int iyMax;
  int secondhit;
  float Amax;
  float time;
  float sum9;
  float sum9cal;
  float sum36;
  float sum36cal;
  float badEnergy;
  float totalEnergy;
  float cluster[3][3];
  bool intime[3][3];
  float ampl36[6][6];
};

#include "calibs.cc"
#include "ledcal.cc"

//--------------------------------------------------
struct RecoEvent {

public:
  void MMproc(const CS::ChipAPV::Digit* apv);
  void MMhits();
  void MMinit(int run);

  void DETinit();
  //void DETclear();
  void DETproc(const ChipSADC::Digit* sadc);
  void DETcollectHits();
  void Trigger();
  float htime(std::vector<float> sample, float &max_signal, float ped[], float detTime);
  void findped(std::vector<float> sample, meanPed &meanped, float ped[]);
  void FindDetCells(int ix, int iy, unsigned int i, int plane, int ixmax, int iymax,
		    std::vector<Cell> &cells, meanPed &meanped, float detTime, float detTimeW);
  void Calibration(Header header);
  
public:  
  Micromega MM1;
  Micromega MM2;
  Micromega MM3;
  Micromega MM4;
  Micromega MM5;
  Micromega MM6;
  Micromega MM7;
  Micromega MM8;

  int run;

  std::vector<short> *ixMSADC = new std::vector<short>;
  std::vector<short> *iyMSADC = new std::vector<short>;
  std::vector<short> *channelMSADC = new std::vector<short>;
  std::vector<short> *srcidMSADC = new std::vector<short>;
  std::vector<short> *chipMSADC = new std::vector<short>;
  std::vector<string> *detectorMSADC = new std::vector<string>;
  std::vector<vector<short> > *samplesMSADC = new std::vector<vector<short> >;

  std::set<string> DetNames;

  meanPed TRGped[2];
  meanPed VETOped[6];
  meanPed ECped[2][6][6];
  meanPed HCped[4][3][3];
  meanPed SRDped[3];
  //meanPed Vped[3];
#if WCATCHER      
  meanPed WCped[3];
#endif
  
  float corrtime;

  //std::vector<Cell> TRGcells(2);
  std::vector<Cell> TRGcells;
  std::vector<Cell> ECcells;
  std::vector<Cell> HCcells;
  std::vector<Cell> HCcellsCut;
  std::vector<Cell> VETOcells;
  std::vector<Cell> SRDcells;
  //std::vector<Cell> Vcells;
#if WCATCHER      
  std::vector<Cell> WCcells;
#endif

  float calEC[2][6][6];
  float calHC[4][3][3];
  float calSRD[3];
  float calVETO[6];
  //float calV[3];
#if WCATCHER       
  float calWC[3];
#endif
  
  Calibs calibs;

  int last_run;
  int last_spill;
  bool masterTimeLoc;
  float masterTime;
  int ECcorrRunNumber;
  bool debug;
  
};

void RecoEvent::DETinit() {      
  DetNames.insert("ECAL0"); // ElectroMagnetic Testbeam calorimeter
  DetNames.insert("ECAL1");
  DetNames.insert("HCAL0"); // Hadron Testbeam calorimeter
  DetNames.insert("HCAL1");
  DetNames.insert("HCAL2");
  DetNames.insert("HCAL3");
  DetNames.insert("HOD0X"); 
  DetNames.insert("HOD0Y");
  DetNames.insert("HOD1X");
  DetNames.insert("HOD1Y");
  DetNames.insert("HOD2X");
  DetNames.insert("HOD2Y");
  DetNames.insert("MUON");
  DetNames.insert("VETO");  // Test beam veto counters
  DetNames.insert("SRD");   // SRD detector to use for Synchrotron radiation detection
  DetNames.insert("S2");    // main beam counter for October 2017 
  DetNames.insert("S3");
  DetNames.insert("S4");
  DetNames.insert("T2");
  //DetNames.insert("V1");
  //DetNames.insert("V2");
  //DetNames.insert("ST04X");
#if WCATCHER       
  DetNames.insert("WCAL"); // catcher   
  //
#endif
  corrtime = 0;
  debug = false;
}


void RecoEvent::MMinit(int Run) {
  // stuff from conddb.h
  run = Run;
  if (3855 <= run && run <= 4207) Init_Geometry_2018_invis100(run);
  if (3855 <= run && run <= 4310) Init_Calib_2018_MM();
  Init_MM_mapping(); // initialize map for MM
}

void RecoEvent::MMproc(const CS::ChipAPV::Digit* apv) {
  // get MM digis
  if (apv) {
    //apv->Print();
    const string& name = apv->GetDetID().GetName();
    const CS::uint16 wire = apv->GetChannel();
    const CS::uint32* raw_q = apv->GetAmplitude();

         if (name == "MM01X") DoHitAccumulation(wire,raw_q,MM1.xplane,MM1_calib.xcalib);
    else if (name == "MM02X") DoHitAccumulation(wire,raw_q,MM2.xplane,MM2_calib.xcalib);
    else if (name == "MM03X") DoHitAccumulation(wire,raw_q,MM3.xplane,MM3_calib.xcalib);
    else if (name == "MM04X") DoHitAccumulation(wire,raw_q,MM4.xplane,MM4_calib.xcalib);
    else if (name == "MM01Y") DoHitAccumulation(wire,raw_q,MM1.yplane,MM1_calib.ycalib);
    else if (name == "MM02Y") DoHitAccumulation(wire,raw_q,MM2.yplane,MM2_calib.ycalib);
    else if (name == "MM03Y") DoHitAccumulation(wire,raw_q,MM3.yplane,MM3_calib.ycalib);
    else if (name == "MM04Y") DoHitAccumulation(wire,raw_q,MM4.yplane,MM4_calib.ycalib);
    else if (name == "MM05X") DoHitAccumulation(wire,raw_q,MM5.xplane,MM5_calib.xcalib);
    else if (name == "MM06X") DoHitAccumulation(wire,raw_q,MM6.xplane,MM6_calib.xcalib);
    else if (name == "MM05Y") DoHitAccumulation(wire,raw_q,MM5.yplane,MM5_calib.ycalib);
    else if (name == "MM06Y") DoHitAccumulation(wire,raw_q,MM6.yplane,MM6_calib.ycalib);
    else if (name.substr(0, 3) == "GM0") {
    }

  }
}

void RecoEvent::MMhits() {
  DoMMReco(MM1, MM1_calib, MM1_pos);
  DoMMReco(MM2, MM2_calib, MM2_pos);
  DoMMReco(MM3, MM3_calib, MM3_pos);
  DoMMReco(MM4, MM4_calib, MM4_pos);
  DoMMReco(MM5, MM5_calib, MM5_pos);
  DoMMReco(MM6, MM6_calib, MM6_pos);
}

void RecoEvent::DETproc(const ChipSADC::Digit* sadc) {
  for (auto& p : DetNames) {
    // Does the detector name match the pattern?
    if (string_match(sadc->GetDetID().GetName(), p))
      {
	const string& detector = sadc->GetDetID().GetName();
	short chip = sadc->GetChip(); // Chip
	short channel = sadc->GetChannel(); // Channel of MSADC block
	short ix = sadc->GetX(); // x of MSADC block
	short iy = sadc->GetY(); // y of MSADC block
	detectorMSADC->push_back(detector); // Detector names(BGO,ECAL0,ECAL1,...)
	chipMSADC->push_back(chip); // x of MSADC block
	channelMSADC->push_back(channel); // x of MSADC block
	ixMSADC->push_back(ix); // x of MSADC block
	iyMSADC->push_back(iy); // y of MSADC block
	
	vector<short> slices;
	const std::vector<uint16>& samples = sadc->GetSamples();
	for (auto& s_it : samples) slices.push_back((s_it));
	
	samplesMSADC->push_back(slices);
      }
  }
}


// new variant May 2019
//========================================================================================
float RecoEvent::htime(std::vector<float> sample, float &max_signal, float ped[], float detTime) {
  
  // taken from from S.V.Donskov on the 1-May-2019
  
  const int f_sample_m = 10;
  const int n_samples = 32;     /* number of samples for msadc */
  
  const float signal_cut = 5.;
  const int f_sample_1 = 6;
  const int l_sample_1 = 31;
  
  float time = -300.;
  float sign[n_samples];
  float signk, signl;
  int  max_sample = 0, k, bpeak = 0, npeak, n_max = 0;
  int locmax[8];
  float sgtime[8];
  float ph[8];
  float maxdev = 1000.;
  float dtime;
  float signmax = 0.;
  int kmax = -1;
  max_signal = 0.;
  
  if (debug) std::cout << "  HTIME: ";
  
  float pedmn = ped[0] < ped[1] ? ped[0] : ped[1];
  for (k = 0; k < n_samples; k++) {
    if (sample[k] != 4095) signk = sample[k]-ped[k&1];
    // in the case of signal overflow use minimum pedestal for even and  odd samlpe
    else signk = sample[k]-pedmn;  
    if (signk < 0.) signk = 0.;
    
    if (debug) std::cout << "  k = " << k << "  sample[k] = " << sample[k]
			 << "  ped[k&1] = " << ped[k&1] << " signk = " << signk << std::endl;   
    //signsum += signk;
    sign[k] = signk;
    if (k < f_sample_m) continue;
    if (signk > signmax + 0.2) {
      signmax = signk;
      kmax = k;
    }
  }
  
  //word[hit].smpl = signsum;
  max_signal = signmax;
  
  if (debug) std::cout << "  max_signal = " << max_signal << "  kmax = " << kmax;// << std::endl;
  
  if (kmax >= 0) time = kmax*12.5;
  
  if (signmax <= signal_cut) return time;  
  
  locmax[0] = kmax;
  ph[0] = sign[kmax];
  n_max++;
  signl = sign[f_sample_m];
  for (k = f_sample_m+1; k < n_samples-2; k++) {
    signk = sign[k];
    if (signk < signl || signk < signal_cut || signk < sign[k+1]-1.0) {
      signl = sign[k];
      continue;
    }
    else if (k == kmax) {
      while (sign[++k] > sign[kmax]-2.0) {
        if (k == n_samples-3) break;
      }
      signl = sign[k];
      k++;
      continue;
    }      
    locmax[n_max] = k;
    ph[n_max++] = signk;
    if (n_max == 8) break;
    k++;
    signl = sign[k];      
  }
  if (n_max > 1 && locmax[0] > locmax[1]) {
    int locmax0 = locmax[0];
    float ph0 = ph[0];
    for (npeak = 1; npeak < n_max; npeak++) {
      if (locmax[npeak] > locmax0) break;
    }
    for (int n = 0; n < npeak-1; n++) {
      locmax[n] = locmax[n+1];
      ph[n] = ph[n+1];
    }
    locmax[npeak-1] = locmax0;
    ph[npeak-1] = ph0;
  }
  
  if (debug) std::cout << "  n_max = " << n_max;
  
  bool hexist;
  for (npeak = 0; npeak < n_max; npeak++) {
    bool lovr = hexist = false;
    int kmax = locmax[npeak];
    if (debug) std::cout << "  kmax = " << kmax << "  npeak = " << npeak << "  ph[npeak] = "
			 << ph[npeak] << std::endl;
    int kmin = kmax-5;
    if (npeak && kmin < locmax[npeak-1]+2) {
      kmin = locmax[npeak-1]+1;
      lovr = true;
    } 
    float signkr = 0.0;
    if (!lovr) {
      for (k = kmax-1; k >=  kmin; k--) {
	if (debug) std::cout << "  k = " << k << "  sign[k] = " << sign[k]
			     << "  0.5*ph[npeak] = " << 0.5*ph[npeak] << std::endl;
	if (sign[k] < 0.5*ph[npeak]) {
	  hexist = true;
	  signkr= sign[k];
	  break;
	}
      }
    }
    else {
      for (k = kmax-1; k >=  kmin; k--) {
	signkr = sign[k] * ph[npeak] * (1+k-locmax[npeak-1])/
	  (ph[npeak] * (1+k-locmax[npeak-1]) + ph[npeak-1] * (1+kmax-k));
	if (signkr < 0.5*ph[npeak]) {
	  hexist = true;
	  break;
	}
      }
    }
    if (debug) std::cout << "  hexist = " << hexist;
    if (!hexist) continue;
    
    time = k + ((ph[npeak]*0.5)-signkr)/(sign[k+1]-signkr);
    sgtime[npeak] =  time * 12.5;
    
    if (debug) std::cout << "  time (insede loop) = " << time; 
    
    if (masterTimeLoc) dtime = fabsf(sgtime[npeak] - detTime);
    else { 
      dtime = fabsf(sgtime[npeak] - masterTime - (detTime + corrtime));
    }
    if (dtime > maxdev) continue;
    maxdev = dtime;
    bpeak = npeak;
  }
  
  float bph = max_signal = ph[bpeak];
  time = sgtime[bpeak];
  
  // make ph time correction for small signals
  if (bph < 40.) time -= ((86./bph) - 2.15);
  
  if (debug) std::cout << "  bpeak = " << bpeak << "  time = " << time << "  max_signal = "
		       << max_signal << std::endl;  
  return (time-corrtime);
}


//=========================================================================================
void RecoEvent::findped(std::vector<float> sample, meanPed &meanped, float ped[])
{
  int f_sample_0 = 0;
  int l_sample_0 = 5;
  
  float sum_even = 0;
  float sum_odd = 0;
  float sum_evenq = 0;
  float sum_oddq = 0;
  float maxamp_even = 0;
  float maxamp_odd = 0;
  
  for (int k = f_sample_0; k <= l_sample_0; k += 2)
    {
      sum_even += sample[k];
      sum_odd += sample[k+1];
      sum_evenq += sample[k]*sample[k];
      sum_oddq += sample[k+1]*sample[k+1];
    }
  
  float ped_even = float(sum_even)*2./(l_sample_0-f_sample_0+1);
  float ped_odd  = float(sum_odd)*2./(l_sample_0-f_sample_0+1);
  float ped_evenq = float(sum_evenq)*2./(l_sample_0-f_sample_0+1);
  float ped_oddq  = float(sum_oddq)*2./(l_sample_0-f_sample_0+1);
  
  float rms = (sqrtf(ped_evenq-(ped_even*ped_even)) + sqrtf(ped_oddq-(ped_odd*ped_odd)))*0.5;
  
  int ch_stat = meanped.pedstat;
  if (rms < 10.) {
    ped[0] = ped_even;
    ped[1] = ped_odd;
    if (ch_stat < 100000) {
      meanped.Ped0 = ((meanped.Ped0*ch_stat) + ped_even)/(ch_stat+1);
      meanped.Ped1 = ((meanped.Ped1*ch_stat) + ped_odd)/(ch_stat+1);
      meanped.pedstat++;
    }
  }
  else {
    ped[0] = meanped.Ped0;
    ped[1] = meanped.Ped1;
  } 
}

 
//====================================================================================
void RecoEvent::FindDetCells(int ix, int iy, unsigned int i, int plane, int ixmax, int iymax, 
			     std::vector<Cell> &cells, meanPed &meanped, float detTime,
			     float detTimeW)
{
  Cell cell; 

  cell.plane = plane;
  cell.ix = ix;
  cell.iy = iy;

  cells.push_back(cell);
  
  if (debug) std::cout << "  i = " << i << " ix = " << ix << "  iy = " << iy << "  ixmax = "
		       << ixmax << "  iymax = " << iymax << "  plane = " << plane << std::endl;
  
  if (ix < 0 || ix >= ixmax) return;
  if (iymax && (iy < 0 || iy >= iymax)) return;

  std::vector<float> y;  
  for (unsigned int j = 0; j < (samplesMSADC->at(i)).size(); j++) {
    float x = float((*samplesMSADC)[i][j]);
    y.push_back(x);
  }
    
  // define the real value of signal
  float ped[2] = {0, 0};  

  findped(y, meanped, ped);
  //-----------------------
    
  float Amax(0), Time(0);
  Time = htime(y, Amax, ped, detTime);
  //if (LED_TRIGGER) Time = ptime(y, Amax, ped, detTime);

  //-------------------------------------------------------------------

  if (debug) {
    std::cout << "  Detector:  plane = " << plane << "  ix = " << ix << "  iy = "
	      << iy << "  Amax = " << Amax << std::endl;
    std::cout << "  meanped.pedstat = " << meanped.pedstat << "  meanped.Ped0 = "
	      << meanped.Ped0 << "  meanped.Ped1 = " << meanped.Ped1 << std::endl;
    //std::cout << "  masterTime = " << masterTime << "  Time = " << Time << std::endl;
  }

  // check if signal time is inside time window
  bool intime = false;
  float time = 0;
  if (masterTimeLoc)  { // time of head counter
    masterTime = Time;
    time = Time;// - detTime; // !!!!! new !!!!
  }

  if (debug) {
    std::cout << "  masterTime = " << masterTime << "  Time = " << Time << std::endl;
  }
  
  float deltaT(0);
  if (!masterTimeLoc) { // it is not head counter 
    time = Time - masterTime;
    float TimeHw = detTimeW;
    deltaT = time - detTime /*+ corrtime */;
    
    //-- test correction -------------------------------------
    if (Amax < 50.) TimeHw *= 2.;
    TimeHw += 6.;             //  + masterTime width
    //-- test correction -------------------------------------

    intime = fabsf(deltaT) < TimeHw;
    
    if (debug) std::cout << " time = " << time << "  detTime = " << detTime
			  << "  corrtime = " << corrtime << "  TimeHw = "
			  << TimeHw << "  deltaT = " << deltaT << "  intime = "
			  << intime << std::endl;
  }

  cell.intime = intime;
  cell.ped0 = ped[0];
  cell.ped1 = ped[1];
  cell.signal = Amax;
  cell.time = time;  // rise time
  cell.deltaT = deltaT;  
  cell.timeCal = detTime;
  cell.ped = meanped;
  if (cells.size()) cells.at(cells.size()-1) = cell;
  
  return;
}

void RecoEvent::DETcollectHits()
{  
  ECcells.clear();
  HCcells.clear();
  HCcellsCut.clear();
  VETOcells.clear();
  SRDcells.clear();
  //Vcells.clear();
#if WCATCHER	  
  WCcells.clear();
#endif	  
  
  // sort out MSADC signals
  for (unsigned int i = 0; i < samplesMSADC->size(); i++) {
    int plane(0);
    int iXY(0);
    int ix = ixMSADC->at(i);
    int iy = iyMSADC->at(i);
    
    // start processing of detectors: collect hits for each detector
    //--------------------------------------------------------------
    
    const std::string name = detectorMSADC->at(i); 
    plane = 0;
    
    if (name == "VETO") {
      if (debug) std::cout << " ----- Processing of VETO plane " << ix << std::endl;
      FindDetCells(ix, iy, i, plane, 6, 0, VETOcells, VETOped[ix], calibs.timeVETO[ix], calibs.rmsVETO[ix]);
    }
    else if (name == "ECAL0" || name == "ECAL1") {
      plane = std::atoi(&name[4]);
      if (debug) std::cout << "\n =============== Processing of Ecal plane " << plane 
	      		   << "  ix = " << ix << "  iy = " << iy << " =================="
	      		   << std::endl;
      FindDetCells(ix, iy, i, plane, 6, 6, ECcells, ECped[plane][ix][iy], calibs.timeECAL[plane][ix][iy],
		   calibs.rmsECAL[plane][ix][iy]);
      if (debug) {
	std::cout << " ECAL  masterTime = " << masterTime << "   masterTimeLoc = "
		  << masterTimeLoc << std::endl;
      }

    }
    else if (name == "HCAL0" || name == "HCAL1" || name == "HCAL2" || name == "HCAL3") {
      plane = std::atoi(&name[4]);
      if (debug) std::cout << "\n ----- Processing of Hcal plane " << plane << std::endl;
      FindDetCells(ix, iy, i, plane, 3, 3, HCcells, HCped[plane][ix][iy], calibs.timeHCAL[plane][ix][iy],
		   calibs.rmsHCAL[plane][ix][iy]);
    }  
    else if (name == "SRD") {
      if (debug) std::cout << " ------- Processing of SRD plane " << ix << std::endl;
      FindDetCells(ix, iy, i, plane, 3, 0, SRDcells, SRDped[ix], calibs.timeSRD[ix], calibs.rmsSRD[ix]);
    }
    else if (name == "HOD0X" || name == "HOD0Y" || name == "HOD1X" || name == "HOD1Y" || 
	     name == "HOD2X" || name == "HOD2Y") {
      plane = std::atoi(&name[3]);
      if (name.compare(4, 4, "X") == 0) iXY = 0;
      else if (name.compare(4, 4, "Y") == 0) iXY = 1;
    }
#if WCATCHER	    
    else if (name == "WCAL") {
      if (ix == 2) {
	if (debug) std::cout << " ----- Processing of WCAL plane " << ix << std::endl;
	FindDetCells(ix, iy, i, 0, 3, 0, WCcells, WCped[ix], calibs.timeWCAL[ix], calibs.rmsWCAL[ix]);
	if (debug) {
	  std::cout << "  WCcells.size() = " << WCcells.size() << std::endl;
	  for (int k = 0; k < (int)WCcells.size(); k++) {
	    std::cout << "  ix = " << ix << "  plane = " << WCcells[k].plane
		      << "  signal = " << WCcells[k].signal << std::endl;
	  }
	}
      }
    }
#endif
  }
}

//=================================================================================
void RecoEvent::Trigger() {
  // ------------ Check trigger --------------------------
  
  for (unsigned int i = 0; i < samplesMSADC->size(); i++)
    {
      int plane(0);
      int ix = ixMSADC->at(i);
      int iy = iyMSADC->at(i);
      
      //std::cout << "  Detector = " << detectorMSADC->at(i) << std::endl;
      
      if (detectorMSADC->at(i) == "T2") {
	masterTimeLoc = (plane == 0) ? true : false;
	
	debug = false;//true;
	if (debug && plane == 0) std::cout << " --- Processing of TRIG counters "
					   << plane << std::endl;
	FindDetCells(ix, iy, i, plane, 1, 0, TRGcells, TRGped[plane], calibs.timeT[2], calibs.rmsT[2]);
	if (debug) {
	  std::cout << " 1  masterTime = " << masterTime << "   masterTimeLoc = "
		    << masterTimeLoc << std::endl;
	}
	
	debug = false;
      }
    }   
}

void RecoEvent::Calibration(Header header) {
  //=============================================================================
  // read led corrections
  //---------------------

  //std::cout << "  Calibration: header.RunNumber = " << header.RunNumber << "  last_run = "
  //          << last_run << std::endl;
  
  if (header.RunNumber != last_run)
    {
      Init_LEDCorrection(header.RunNumber);
      last_run = header.RunNumber;
      last_spill = 0;
    }
  
  int spillNb = header.BurstNumber;
  if (spillNb != last_spill)
    {
      last_spill = spillNb;
      if (noLedFile) {
	for (int loc = ec_loc; loc < led_cells; loc++) {
	  ledcell[loc].ledstat = 0;
	}
      } else if (!noLedCorr) {
	for (int loc = ec_loc; loc < sum_loc; loc++) {
	  ledcell[loc].ledstat = spillled[spillNb-1];
	  ledcell[loc].Led = locLed[spillNb-1][loc];
	}
      }	       
    }
  
  if (header.RunNumber != ECcorrRunNumber) {
    // read calibrations from .xml files
    //----------------------------------
    readCalibs(calibs, header.RunNumber);
    
    ECcorrRunNumber = header.RunNumber;
    
    // ECAL calibration
    float c0maxEC[2] = {5., 95.};
    //float c0maxEC[2] = {5., 78.};
    
    float EcalEnergyCorr = 100./134.4 * 1.01365;
    
    for (int ipl = 0; ipl < 2; ipl ++) 
      for (int i = 0; i < 6; i++) 
	for (int j = 0; j < 6; j++) 
	  calEC[ipl][i][j] = c0maxEC[ipl]*calibs.calibECAL[ipl][i][j]*EcalEnergyCorr;
    // in GeV
    
    // HCAL calibration
    for (int ipl = 0; ipl < 4; ipl ++) 
      for (int i = 0; i < 3; i++) 
	for (int j = 0; j < 3; j++) 
	  calHC[ipl][i][j] = 100.*calibs.calibHCAL[ipl][i][j]; // in GeV
    
    for (int i = 0; i < 3; i++) calSRD[i] = calibs.calibSRD[i]; // in MeV
    
    for (int i = 0; i < 6; i++) calVETO[i] = calibs.calibVETO[i]; // in MIPs
#if WCATCHER
    for (int i = 0; i < 3; i++)
      calWC[i] = calibs.calibWCAL[i];//*1.e3; // in MeV
#endif
    //calibs.timeT[2] += 3.11; 
    
    // make corrections of electron energy
    double arg = (double)header.RunNumber;
    double corrFunction = -2.69226e+05 + 2.01921e+02*arg - 5.04470e-02*arg*arg +
      4.19990e-06*arg*arg*arg;
    double coeff = corrFunction > 0 ? 100./corrFunction : 1;
    
    std::cout << "  run = " << arg << "  corrFunction = " << corrFunction << "  coeff = "
	      << coeff << std::endl;
    
    for (int ipl = 0; ipl < 2; ipl ++)
      for (int i = 0; i < 6; i++)
	for (int j = 0; j < 6; j++)
	  calEC[ipl][i][j] *= coeff;
    
    // corrections for Hcal planes
    if (header.RunNumber > 3809) {
      
      double HCcorr = 100./(1.40086e+02 - 1.02715e-02*header.RunNumber);
      
      for (int ipl = 0; ipl < 4; ipl ++) 
	for (int i = 0; i < 3; i++) 
	  for (int j = 0; j < 3; j++) 
	    calHC[ipl][i][j] *= 100./97.54 * HCcorr; // in GeV corr from run 3924
    }
  }
}

#include "tracking.h"
